##################################
# Makefile for procgame projects #
##################################

CC := gcc
INCLUDES := -Isrc -Ibuild
TARGET := procgame


# Linux
LIBS_LINUX := -Lsrc/libs/linux/GL -lGLEW -lSDL2 -lGL -lm


# Windows
LIBS_WIN64 := -Lsrc/libs/win64/GL -Lsrc/libs/win64/SDL2 \
 -lglew32 -lmingw32 -lSDL2main -lSDL2 -lgdi32 -lwinmm -limm32 \
 -lole32 -loleaut32 -lversion -lopengl32 -lws2_32 -limagehlp \


# Compile flags for the different build types
CFLAGS_DEBUG := $(CFLAGS_EXTRA) -Wall -Wno-unused-variable -Wno-unused-function -g -O0 -std=gnu11
CFLAGS_RELEASE := $(CFLAGS_EXTRA) -Wall -Wno-unused-variable -Wno-unused-function -g -O3 -flto -std=gnu11


# Build targets
debug_linux: INCLUDES += -Isrc/libs/linux
debug_linux: LIBS := $(LIBS_LINUX)
debug_linux: CFLAGS := $(CFLAGS_DEBUG) -rdynamic \
 -D_POSIX_C_SOURCE=200809L -D_GNU_SOURCE \
 -DGLEW_STATIC -DSHADER_BASE_DIR='"${CURDIR}/src/procgl/shaders/"'
debug_linux: procgame

release_linux: INCLUDES += -Isrc/libs/linux
release_linux: LIBS := $(LIBS_LINUX)
release_linux: CFLAGS := $(CFLAGS_RELEASE) -rdynamic \
 -DPOSIX_C_SOURCE=200809L -D_GNU_SOURCE \
 -DGLEW_STATIC -DPROCGL_STATIC_SHADERS
release_linux: clean pack_shaders procgame stripdebug

debug_windows: INCLUDES += -Isrc/libs/win64
debug_windows: LIBS := $(LIBS_WIN64)
debug_windows: CFLAGS := $(CFLAGS_DEBUG) -Wl,--export-all-symbols -mwindows \
 -DGLEW_STATIC -DSHADER_BASE_DIR='"${CURDIR}/src/procgl/shaders/"'
debug_windows: TARGET := $(TARGET).exe
debug_windows: procgame

release_windows: INCLUDES += -Isrc/libs/win64
release_windows: LIBS := $(LIBS_WIN64)
release_windows: CFLAGS := $(CFLAGS_RELEASE) -Wl,--export-all-symbols -mwindows \
 -DPROCGL_STATIC_SHADERS -DGLEW_STATIC
release_windows: TARGET := $(TARGET).exe
release_windows: clean pack_shaders procgame stripdebug


# Build rules
GAME_OBJS = $(patsubst src%,build/obj%,$(patsubst %.c,%.o,$(wildcard src/game/*.c)))
GAME_UI_OBJS = $(patsubst src%,build/obj%,$(patsubst %.c,%.o,$(wildcard src/game/ui/*.c)))
PROCGAME_OBJS = $(patsubst src%,build/obj%,$(patsubst %.c,%.o,$(wildcard src/procgl/*.c)))
EXT_OBJS = $(patsubst src%,build/obj%,$(patsubst %.c,%.o,$(wildcard src/procgl/ext/*.c)))

GAME_HEADERS = $(wildcard src/game/*.h)
GAME_UI_HEADERS = $(wildcard src/game/ui/*.h)
PROCGAME_HEADERS = $(wildcard src/procgl/*.h)
EXT_HEADERS = $(wildcard src/procgl/ext/*.h)

procgame: build/obj/main.o $(GAME_OBJS) $(GAME_UI_OBJS) $(PROCGAME_OBJS) $(EXT_OBJS)
	$(CC) $(CFLAGS) -o $(TARGET) build/obj/main.o $(GAME_OBJS) $(GAME_UI_OBJS) $(PROCGAME_OBJS) $(EXT_OBJS) $(LIBS)

build/obj/main.o: src/main.c $(GAME_HEADERS) $(PROCGAME_HEADERS) $(EXT_HEADERS)
	$(CC) $(CFLAGS) $(INCLUDES) -c -o $@ src/main.c
build/obj/game/%.o: src/game/%.c $(GAME_HEADERS) $(PROCGAME_HEADERS) $(EXT_HEADERS)
	$(CC) $(CFLAGS) $(INCLUDES) -c -o $@ $<
build/obj/game/ui/%.o: src/game/ui/%.c $(GAME_HEADERS) $(GAME_UI_HEADERS) $(PROCGAME_HEADERS) $(EXT_HEADERS)
	$(CC) $(CFLAGS) $(INCLUDES) -c -o $@ $<
build/obj/procgl/%.o: src/procgl/%.c $(PROCGAME_HEADERS) $(EXT_HEADERS)
	$(CC) $(CFLAGS) $(INCLUDES) -c -o $@ $<
build/obj/procgl/ext/%.o: src/procgl/ext/%.c $(EXT_HEADERS)
	$(CC) $(CFLAGS) $(INCLUDES) -c -o $@ $<



# Separate release binary into two files: one with the binary and the other
# with debug symbols
stripdebug: procgame
	cp $(TARGET) $(TARGET).dbg
	strip --strip-debug $(TARGET) -o $(TARGET)


# Pack all the glsl sources into a C header file for release
shaders_preamble:
	echo "#ifndef PG_STATIC_SHADERS_INCLUDED" > build/shader_sources.glsl.h
	echo "#define PG_STATIC_SHADERS_INCLUDED" >> build/shader_sources.glsl.h

SHADER_SOURCES = $(wildcard src/procgl/shaders/*.glsl)
$(SHADER_SOURCES):
	xxd -i $@ | sed 's/\([0-9a-f]\)$$/\0, 0x00/' >> build/shader_sources.glsl.h
shaders: $(SHADER_SOURCES)

shaders_end:
	sed -i -e 's/unsigned char/static char/g' build/shader_sources.glsl.h
	sed -i -e 's/unsigned int/static unsigned/g' build/shader_sources.glsl.h
	sed -i -e 's/\(procgl_\|src_\|shaders_\|_glsl\)//g' build/shader_sources.glsl.h
	echo "#endif" >> build/shader_sources.glsl.h

pack_shaders: shaders_preamble shaders shaders_end


# Create build directories
BUILD_DIRS = build build/obj build/obj/game build/obj/game/ui build/obj/procgl build/obj/procgl/ext
build_dirs: $(BUILD_DIRS)
$(BUILD_DIRS):
	mkdir -p $@

# Delete all build files
clean:
	rm -f build/obj/main.o $(GAME_OBJS) $(GAME_UI_OBJS) $(PROCGAME_OBJS) $(EXT_OBJS)
	rm -f build/shader_sources.glsl.h

.PHONY: $(SHADER_SOURCES) shaders pack_shaders

procgame (source code) is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

procgame is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
procgame.  If not, see <http://www.gnu.org/licenses/>.

---

The font "Caveat-Bold.ttf" is distributed under the SIL Open Font License.

The font "Orbitron Bold.ttf" by Matt McInerney is distributed under the SIL
Open Font License.

The font "NASDAQER.ttf" by Gustavo Paz is distributed under the Creative
Commons BY-SA license.

Other assets (in the 'res/' directory) are, unless noted otherwise,
distributed under the Creative Commons BY-NC-SA license, copyright Joshua Giles

---

CC BY-SA:
    https://creativecommons.org/licenses/by-sa/4.0/deed.en_US

CC BY-NC-SA:
    https://creativecommons.org/licenses/nc-by-sa/4.0/deed.en_US

SIL Open Font License:
    http://scripts.sil.org/OFL

GNU GPLv3:
    http://www.gnu.org/licenses/

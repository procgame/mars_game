#ifndef PG_MEMPOOL_INCLUDED
#define PG_MEMPOOL_INCLUDED

/************************************************/
/*  REGULAR MEMORY POOL                         */
/************************************************/

#define PG_MEMPOOL_DECLARE(TYPE, PREFIX) \
    typedef ARR_T(TYPE) PREFIX##_pool_t; \
    typedef int64_t PREFIX##_id_t; \
    void            PREFIX##_pool_init(PREFIX##_pool_t* pool); \
    void            PREFIX##_pool_deinit(PREFIX##_pool_t* pool); \
    void            PREFIX##_pool_clear(PREFIX##_pool_t* pool); \
    PREFIX##_id_t   PREFIX##_alloc(PREFIX##_pool_t* pool, int n, TYPE** ptr); \
    void            PREFIX##_free(PREFIX##_pool_t* pool, PREFIX##_id_t id); \
    TYPE*           PREFIX##_get(PREFIX##_pool_t* pool, PREFIX##_id_t id); \
    PREFIX##_id_t   PREFIX##_id(PREFIX##_pool_t* pool, TYPE* ptr); \
    size_t          PREFIX##_pool_write_to_file(PREFIX##_pool_t* pool, FILE* f); \
    size_t          PREFIX##_pool_read_from_file(PREFIX##_pool_t* pool, FILE* f);

#define PG_MEMPOOL_DEFINE(TYPE, PREFIX, ALLOC_FIELD) \
void            PREFIX##_pool_init(PREFIX##_pool_t* pool) \
{ \
    ARR_INIT(*pool); \
} \
void            PREFIX##_pool_deinit(PREFIX##_pool_t* pool) \
{ \
    ARR_DEINIT(*pool); \
} \
\
void            PREFIX##_pool_clear(PREFIX##_pool_t* pool) \
{ \
    int i; \
    for(i = 0; i < pool->cap; ++i) { \
        pool->data[i] = (TYPE){}; \
    } \
} \
\
PREFIX##_id_t   PREFIX##_alloc(PREFIX##_pool_t* pool, int n, TYPE** ptr) \
{ \
    int run = 1, alloc = -1, i = 0; \
    for(i = 0; i < pool->cap; ++i) { \
        if(!pool->data[i].ALLOC_FIELD) { run = 1; } else continue; \
        while(run < n && run + i < pool->cap \
        && !pool->data[run + i].ALLOC_FIELD) ++run; \
        if(run < n) { i += run - 1; } else break; \
    } \
    alloc = i; \
    if(alloc + n >= pool->cap) ARR_RESERVE(*pool, alloc + n); \
    for(i = 0; i < n; ++i) pool->data[alloc + i] = (TYPE){ .ALLOC_FIELD = 1 }; \
    if(ptr) *ptr = pool->data + alloc; \
    return alloc; \
} \
\
void            PREFIX##_free(PREFIX##_pool_t* pool, PREFIX##_id_t id) \
{ \
    if(id >= 0 && id < pool->cap) pool->data[id] = (TYPE){}; \
} \
\
TYPE*           PREFIX##_get(PREFIX##_pool_t* pool, PREFIX##_id_t id) \
{ \
    if(id >= 0 && id < pool->cap) return &(pool->data[id]); \
    else return NULL; \
} \
\
PREFIX##_id_t   PREFIX##_id(PREFIX##_pool_t* pool, TYPE* ptr) \
{ \
    return ptr - pool->data; \
} \
\
size_t          PREFIX##_pool_write_to_file(PREFIX##_pool_t* pool, FILE* f) \
{ \
    size_t r = 0; \
    uint32_t len = pool->cap; \
    r += fwrite(&len, sizeof(len), 1, f); \
    r += fwrite(pool->data, sizeof(TYPE), len, f); \
    return r; \
} \
\
size_t          PREFIX##_pool_read_from_file(PREFIX##_pool_t* pool, FILE* f) \
{ \
    uint32_t len; \
    size_t r; \
    r = fread(&len, sizeof(len), 1, f); \
    ARR_TRUNCATE_CLEAR(*pool, 0); \
    ARR_RESERVE_CLEAR(*pool, len); \
    r += fread(pool->data, sizeof(TYPE), len, f); \
    pool->cap = len; \
    return r; \
} \

/************************************************/
/*  GLOBAL MEMORY POOL                          */
/************************************************/

#define PG_MEMPOOL_DECLARE_GLOBAL(TYPE, PREFIX) \
    typedef int64_t PREFIX##_id_t; \
    void            PREFIX##_deinit(void); \
    void            PREFIX##_clear(void); \
    PREFIX##_id_t   PREFIX##_alloc(int n, TYPE** ptr); \
    void            PREFIX##_free(PREFIX##_id_t id); \
    TYPE*           PREFIX##_get(PREFIX##_id_t id); \
    PREFIX##_id_t   PREFIX##_id(TYPE* ptr); \
    size_t          PREFIX##_write_to_file(FILE* f); \
    size_t          PREFIX##_read_from_file(FILE* f);

#define PG_MEMPOOL_DEFINE_GLOBAL(TYPE, PREFIX, ALLOC_FIELD) \
static ARR_T(TYPE) PREFIX = {}; \
\
void            PREFIX##_deinit(void) \
{ \
    ARR_DEINIT(PREFIX); \
} \
\
void            PREFIX##_clear(void) \
{ \
    int i; \
    for(i = 0; i < PREFIX.cap; ++i) { \
        PREFIX.data[i] = (TYPE){}; \
    } \
} \
\
PREFIX##_id_t   PREFIX##_alloc(int n, TYPE** ptr) \
{ \
    int run = 1, alloc = -1, i = 0; \
    for(i = 0; i < PREFIX.cap; ++i) { \
        if(!PREFIX.data[i].ALLOC_FIELD) { run = 1; } else continue; \
        while(run < n && run + i < PREFIX.cap \
        && !PREFIX.data[run + i].ALLOC_FIELD) ++run; \
        if(run < n) { i += run - 1; } else break; \
    } \
    alloc = i; \
    if(alloc + n >= PREFIX.cap) ARR_RESERVE(PREFIX, alloc + n); \
    for(i = 0; i < n; ++i) PREFIX.data[alloc + i] = (TYPE){ .ALLOC_FIELD = 1 }; \
    if(ptr) *ptr = PREFIX.data + alloc; \
    return alloc; \
} \
\
void            PREFIX##_free(PREFIX##_id_t id) \
{ \
    if(id >= 0 && id < PREFIX.cap) PREFIX.data[id] = (TYPE){}; \
} \
\
TYPE*           PREFIX##_get(PREFIX##_id_t id) \
{ \
    if(id >= 0 && id < PREFIX.cap) return &(PREFIX.data[id]); \
    else return NULL; \
} \
\
PREFIX##_id_t   PREFIX##_id(TYPE* ptr) \
{ \
    return ptr - PREFIX.data; \
} \
\
size_t          PREFIX##_write_to_file(FILE* f) \
{ \
    size_t r = 0; \
    uint32_t len = PREFIX.cap; \
    r += fwrite(&len, sizeof(len), 1, f); \
    r += fwrite(PREFIX.data, sizeof(TYPE), len, f); \
    return r; \
} \
\
size_t          PREFIX##_read_from_file(FILE* f) \
{ \
    uint32_t len; \
    size_t r; \
    r = fread(&len, sizeof(len), 1, f); \
    ARR_TRUNCATE_CLEAR(PREFIX, 0); \
    ARR_RESERVE_CLEAR(PREFIX, len); \
    r += fread(PREFIX.data, sizeof(TYPE), len, f); \
    PREFIX.cap = len; \
    return r; \
} \

#endif


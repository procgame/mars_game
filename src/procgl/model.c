#include "procgl.h"

struct model_vertex { vec3 pos; vec2 tex; vec3 rig; };

static const struct pg_vertex_attrib model_attribs[3] = {
    { "v_position", .type = GL_FLOAT, .elements = 3, .size = sizeof(vec3) },
    { "v_tex_coord", .type = GL_FLOAT, .elements = 2, .size = sizeof(vec2) },
    { "v_rigging", .type = GL_FLOAT, .elements = 3, .size = sizeof(vec3) },
};

void pg_model_init_vertex_buffer(struct pg_vertex_buffer* verts)
{
    pg_vertex_buffer_init(verts, 3, model_attribs);
}

void pg_model_load(struct pg_model* model, struct pg_vertex_buffer* verts,
                   const char* filename)
{
    model->vbuf = verts;
    model->vertex_range[0] = verts->vertex_len;
    uint32_t first_vert = verts->vertex_len;
    
    FILE* file = fopen(filename, "r");
    if(!file) {
        printf("procgame ERROR: Failed to load model file: %s\n", filename);
        return;
    }
    
    /*  Read vertex count   */
    int n_verts;
    fscanf(file, " V %d\n", &n_verts);
    pg_vertex_buffer_reserve_verts(verts, verts->vertex_len + n_verts);
    pg_vertex_buffer_reserve_faces(verts, verts->faces.len + (n_verts / 3));
    model->vertex_range[1] = n_verts;

    /*  Read vertex data    */
    struct model_vertex vert;
    int i, j;
    for(i = 0; i < n_verts; ++i) {
        fscanf(file, " ( %f , %f , %f ) ( %f , %f ) ( %f , %f , %f )",
            &vert.pos.x, &vert.pos.y, &vert.pos.z,
            &vert.tex.x, &vert.tex.y,
            &vert.rig.x, &vert.rig.y, &vert.rig.z );
        vert.tex.y = 1.0f - vert.tex.y;
        pg_vertex_buffer_push_vertex(verts, &vert);
    }
    for(i = 0; i < n_verts; i += 3) {
        pg_vertex_buffer_add_face(verts,
            (i + first_vert), (i + first_vert + 1), (i + first_vert + 2));
    }

    /*  Read rig data   */
    int n_bones, n_poses;
    fscanf(file, " RIG: bones: %d , poses: %d", &n_bones, &n_poses);
    model->rig.n_bones = n_bones;
    model->n_poses = n_poses;
    for(i = 0; i < n_bones; ++i) {
        fscanf(file, " bone %*d: %32s", model->rig.bone_name[i]);
        fscanf(file, " pos ( %f , %f , %f )",
            &model->rig.bone_origin[i].x, &model->rig.bone_origin[i].y,
            &model->rig.bone_origin[i].z );
        fscanf(file, " end ( %*f , %*f , %*f )");
    }
    for(i = 0; i < n_poses; ++i) {
        fscanf(file, " pose %*d: %32s", model->pose_names[i]);
        for(j = 0; j < n_bones; ++j) {
            fscanf(file, " bone %*d: %*32s");
            fscanf(file, " pos ( %f , %f , %f )",
                &model->poses[i].move[j].x, &model->poses[i].move[j].y,
                &model->poses[i].move[j].z );
            fscanf(file, " quat ( %f , %f , %f , %f )",
                &model->poses[i].rot[j].x, &model->poses[i].rot[j].y,
                &model->poses[i].rot[j].z, &model->poses[i].rot[j].w );
        }
    }

    printf("Model now has %u verts\n", verts->vertex_len);

    fclose(file);
}

void pg_model_pose_interp(struct pg_model* model, struct pg_model_pose* out,
                          int pose_0, int pose_1, float pose_lerp)
{
    struct pg_model_pose* p0 = &model->poses[pose_0];
    struct pg_model_pose* p1 = &model->poses[pose_1];
    int i;
    for(i = 0; i < model->rig.n_bones; ++i) {
        out->move[i] = vec3_lerp(p0->move[i], p1->move[i], pose_lerp);
        out->rot[i] = quat_lerp(p0->rot[i], p1->rot[i], pose_lerp);
    }
}

static size_t drawfunc_static_model(const void* draw, const struct pg_shader* shader,
                                    const mat4* mats, const GLint* idx)
{
    const mat4* tx = draw;
    uint32_t* vertex_range = (uint32_t*)((mat4*)draw + 1);
    mat4 mvp = mat4_mul(mats[PG_PROJECTIONVIEW_MATRIX], *tx);
    glUniformMatrix4fv(idx[0], 1, GL_FALSE, mvp.v);
    glDrawElements(GL_TRIANGLES, vertex_range[1], GL_UNSIGNED_INT, (GLvoid*)vertex_range[0]);
    return sizeof(*tx) + sizeof(uint32_t) * 2;
}


void pg_model_draw_static(struct pg_renderpass* pass, struct pg_model* model, mat4 tx)
{
    pg_renderpass_add_draw_data(pass, sizeof(tx), &tx);
    pg_renderpass_add_draw_data(pass, sizeof(model->vertex_range), model->vertex_range);
}

void pg_ezpass_static_model(struct pg_renderpass* pass, struct pg_rendertarget* target,
                            struct pg_vertex_buffer* vbuf, struct pg_texture* tex)
{
    pg_renderpass_init(pass, "pgm", PG_RENDERPASS_OPTS(
        PG_RENDERPASS_DEPTH_TEST | PG_RENDERPASS_DEPTH_WRITE,
        .depth_func = GL_LEQUAL ));
    pg_renderpass_target(pass, target);
    pg_renderpass_vertices(pass, vbuf);
    pg_renderpass_drawformat(pass, drawfunc_static_model, 1, "pg_matrix_mvp");
    pg_renderpass_texture(pass, 0, tex, NULL);
}

static size_t drawfunc_rigged_model(const void* draw, const struct pg_shader* shader,
                                    const mat4* mats, const GLint* idx)
{
    const mat4* tx = draw;
    const uint32_t* vertex_range = (uint32_t*)((mat4*)draw + 1);
    const int n_bones = *(int*)(vertex_range + 2);
    const mat4* bone_mats = (mat4*)(vertex_range + 3);
    mat4 mvp = mat4_mul(mats[PG_PROJECTIONVIEW_MATRIX], *tx);
    glUniformMatrix4fv(idx[0], 1, GL_FALSE, mvp.v);
    glUniformMatrix4fv(idx[1], n_bones, GL_FALSE, bone_mats);
    glDrawElements(GL_TRIANGLES, vertex_range[1], GL_UNSIGNED_INT, (GLvoid*)vertex_range[0]);
    return sizeof(*tx)
        + (sizeof(uint32_t) * 2)
        + (sizeof(int))
        + (sizeof(mat4) * n_bones);
}

void pg_model_draw_rigged(struct pg_renderpass* pass, struct pg_model* model,
                          struct pg_model_pose* pose, mat4 tx)
{
    pg_renderpass_add_draw_data(pass, sizeof(tx), &tx);
    pg_renderpass_add_draw_data(pass, sizeof(model->vertex_range), model->vertex_range);
    pg_renderpass_add_draw_data(pass, sizeof(int), &model->rig.n_bones);
    int i;
    for(i = 0; i < model->rig.n_bones; ++i) {
        mat4 bone_tx = mat4_translation(vec3_add(model->rig.bone_origin[i], pose->move[i]));
        mat4 rot = mat4_from_quat(pose->rot[i]);
        bone_tx = mat4_mul(bone_tx, rot);
        mat4 unbind = mat4_translation(vec3_negative(model->rig.bone_origin[i]));
        bone_tx = mat4_mul(bone_tx, unbind);
        pg_renderpass_add_draw_data(pass, sizeof(bone_tx), &bone_tx);
    }
}

void pg_ezpass_rigged_model(struct pg_renderpass* pass, struct pg_rendertarget* target,
                            struct pg_vertex_buffer* vbuf, struct pg_texture* tex)
{
    pg_renderpass_init(pass, "pgm_rigged", PG_RENDERPASS_OPTS(
        PG_RENDERPASS_DEPTH_TEST | PG_RENDERPASS_DEPTH_WRITE,
        .depth_func = GL_LEQUAL ));
    pg_renderpass_target(pass, target);
    pg_renderpass_vertices(pass, vbuf);
    pg_renderpass_drawformat(pass, drawfunc_rigged_model, 2, "pg_matrix_mvp", "bone_mats");
    pg_renderpass_texture(pass, 0, tex, NULL);
}

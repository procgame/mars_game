#include "htable.h"

struct pg_vertex_buffer;

/*  The basic structure of a render:
    pg_renderer
        pg_renderpass
            pg_rendertarget                 The destination for all draw calls
            pg_vertex_buffer                        The model and texture to be used
            pg_texture                      for all draws in this bucket.
            array of pg_type data
            draw_func                       Advances through uniform array
*/

typedef size_t (*pg_render_drawfunc_t)(
    const void*, const struct pg_shader*, const mat4*, const GLint*);

#define PG_RENDERPASS_SWAP_BEFORE   (1 << 0)
#define PG_RENDERPASS_SWAP_PER_DRAW (1 << 1)
#define PG_RENDERPASS_SWAP_AFTER    (1 << 2)
#define PG_RENDERPASS_BUFFER_SWAP \
    ( PG_RENDERPASS_SWAP_BEFORE | PG_RENDERPASS_SWAP_PER_DRAW \
    | PG_RENDERPASS_SWAP_AFTER)

#define PG_RENDERPASS_BLENDING      (1 << 3)
#define PG_RENDERPASS_DEPTH_WRITE   (1 << 4)
#define PG_RENDERPASS_DEPTH_TEST    (1 << 5)
#define PG_RENDERPASS_CULL_FACES    (1 << 6)
#define PG_RENDERPASS_VIEWPORT      (1 << 7)
#define PG_RENDERPASS_SCISSOR       (1 << 8)

struct pg_renderpass_opts {
    uint32_t flags;
    GLenum blend_func[2];
    GLenum depth_func;
    GLbitfield clear_buffers;
    vec4 viewport;
    vec4 scissor;
};

#define PG_RENDERPASS_OPTS(...) \
    (&(struct pg_renderpass_opts){ __VA_ARGS__ })

struct pg_renderpass {
    /*  Pass data/options   */
    int enabled;
    char shader[32];
    struct pg_renderpass_opts opts;
    /*  Output structures   */
    struct pg_rendertarget* target;
    struct pg_viewer* viewer;
    /*  Vertex buffer to draw with  */
    struct pg_vertex_buffer* verts;
    /*  Shader uniforms and matrices    */
    vec2 resolution;
    float aspect_ratio;
    mat4 mats[PG_COMMON_MATRICES];
    struct {
        enum {
            PG_PASS_TEX_IMAGE,
            PG_PASS_TEX_RENDERTARGET,
            PG_PASS_TEX_BUFFER } type;
        union {
            struct {
                struct pg_texture* tex;
                struct pg_texture_opts tex_opts;
                int use_opts;
            };
            struct {
                struct pg_rendertarget* fb_tex;
                int idx;
            };
            struct pg_buffertex* buf;
        };
    } tex[8];
    /*  Table of user-defined uniforms set by this renderpass   */
    pg_shader_uniform_table_t uniforms;
    /*  Raw array of uniform data, processed by drawfunc    */
    char draw_uniform[8][32];
    GLint draw_uniform_idx[8];
    ARR_T(char) draw_data;
    pg_render_drawfunc_t per_draw;
};

struct pg_renderer {
    GLuint dummy_vao;
    pg_shader_table_t shaders;
    pg_shader_uniform_table_t common_uniforms;
    ARR_T(struct pg_renderpass*) passes;
};

/*  Renderer    */
void pg_renderer_init(struct pg_renderer* rend);
void pg_renderer_deinit(struct pg_renderer* rend);
void pg_renderer_reset(struct pg_renderer* rend);
void pg_renderer_draw_frame(struct pg_renderer* rend);
void pg_renderer_vertex_spec(struct pg_renderer* rend, char* shader,
                             struct pg_vertex_buffer* verts);

#ifdef PROCGL_STATIC_SHADERS
#define PG_RENDERER_LOAD_SHADER(RENDERER, SHADER, VERT, FRAG) \
do { \
    struct pg_shader shader; \
    int load = pg_shader_load_static(&shader, SHADER, \
        VERT, VERT##_len, FRAG, FRAG##_len); \
    if(!load) printf("procgame ERROR: Failed to load shader: " SHADER "\n"); \
    else HTABLE_SET((RENDERER).shaders, SHADER, shader); \
} while(0)
#else
#define PG_RENDERER_LOAD_SHADER(RENDERER, SHADER, VERT, FRAG) \
do { \
    struct pg_shader shader; \
    int load = pg_shader_load(&shader, SHADER, \
        SHADER_DIR #VERT ".glsl", SHADER_DIR #FRAG ".glsl"); \
    if(!load) printf("procgame ERROR: Failed to load shader: " SHADER "\n"); \
    else HTABLE_SET((RENDERER).shaders, SHADER, shader); \
} while(0)
#endif

/****************/
/*  Renderpass  */
/****************/
void pg_renderpass_init(struct pg_renderpass* pass, char* shader_name,
                        struct pg_renderpass_opts* opts);
void pg_renderpass_deinit(struct pg_renderpass* pass);

/*  Set vertices, textures, and uniforms   */
void pg_renderpass_uniform(struct pg_renderpass* pass, char* name,
                           enum pg_data_type type, struct pg_type* data);
void pg_renderpass_vertices(struct pg_renderpass* pass, struct pg_vertex_buffer* verts);
void pg_renderpass_texture(struct pg_renderpass* pass, int idx,
                           struct pg_texture* tex, struct pg_texture_opts* opts);
void pg_renderpass_fbtexture(struct pg_renderpass* pass, int idx,
                             struct pg_rendertarget* src, int src_idx);
void pg_renderpass_buftexture(struct pg_renderpass* pass, int idx,
                              struct pg_buffertex* tex);

/*  Set how output is handled  */
void pg_renderpass_viewer(struct pg_renderpass* pass, struct pg_viewer* view);
void pg_renderpass_target(struct pg_renderpass* pass,
                          struct pg_rendertarget* target);

/*  Set miscellaneous options like blending, depth testing, etc.    */
void pg_renderpass_enable(struct pg_renderpass* pass, int enabled);
void pg_renderpass_blending(struct pg_renderpass* pass, int use,
                            GLenum left, GLenum right);
void pg_renderpass_depth_test(struct pg_renderpass* pass, int use, GLenum func);
void pg_renderpass_depth_write(struct pg_renderpass* pass, int use);
void pg_renderpass_clear_bits(struct pg_renderpass* pass, GLbitfield buffers);
void pg_renderpass_viewport(struct pg_renderpass* pass, int use, struct pg_viewport* vp);
void pg_renderpass_scissor(struct pg_renderpass* pass, int use, struct pg_viewport* vp);
void pg_renderpass_swap(struct pg_renderpass* pass, uint32_t flags);

/*  Drawing */
void pg_renderpass_drawformat(struct pg_renderpass* pass,
                               pg_render_drawfunc_t per_draw, int n, ...);
void pg_renderpass_add_draw_data(struct pg_renderpass* pass, size_t size, void* data);
void pg_renderpass_clear(struct pg_renderpass* pass);







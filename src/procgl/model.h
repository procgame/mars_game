
#define PG_MODEL_MAX_BONES   32
#define PG_MODEL_MAX_POSES   32

struct pg_model_pose {
    quat rot[PG_MODEL_MAX_BONES];
    vec3 move[PG_MODEL_MAX_BONES];
};

struct pg_model_rig {
    char bone_name[PG_MODEL_MAX_BONES][32];
    vec3 bone_origin[PG_MODEL_MAX_BONES];
    int n_bones;
};

struct pg_model {
    struct pg_vertex_buffer* vbuf;
    uint32_t vertex_range[2];
    struct pg_model_rig rig;
    char pose_names[PG_MODEL_MAX_POSES][32];
    struct pg_model_pose poses[PG_MODEL_MAX_POSES];
    int n_poses;
};

void pg_model_init_vertex_buffer(struct pg_vertex_buffer* verts);

void pg_model_load(struct pg_model* model, struct pg_vertex_buffer* verts,
                   const char* filename);

void pg_model_pose_interp(struct pg_model* model, struct pg_model_pose* out,
                          int pose_0, int pose_1, float pose_lerp);

void pg_model_draw_static(struct pg_renderpass* pass, struct pg_model* model, mat4 tx);
void pg_model_draw_rigged(struct pg_renderpass* pass, struct pg_model* model,
                          struct pg_model_pose* pose, mat4 tx);

void pg_ezpass_static_model(struct pg_renderpass* pass, struct pg_rendertarget* target,
                            struct pg_vertex_buffer* vbuf, struct pg_texture* tex);
void pg_ezpass_rigged_model(struct pg_renderpass* pass, struct pg_rendertarget* target,
                            struct pg_vertex_buffer* vbuf, struct pg_texture* tex);

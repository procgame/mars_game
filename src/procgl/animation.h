/************************************/
/*  Keyframe animations             */
/************************************/

enum pg_anim_loop {
    PG_ANIM_LOOP_NONE,
    PG_ANIM_LOOP_CIRCULAR,
    PG_ANIM_LOOP_REPEAT
};

struct pg_anim_keyframe {
    int absolute;
    vec4 value;
    pg_easing_func_t ease_forward, ease_back;
    float dur_forward, dur_back;
};

#define PG_ANIM_MAX_KEYFRAMES     32
struct pg_animation {
    struct pg_anim_keyframe keyframes[PG_ANIM_MAX_KEYFRAMES];
    int num_frames;
};

struct pg_anim_property {
    vec4 base_value;
    float frame_time;
    int current_motion;
    int start_frame, end_frame;
    int current_frame;
    int loop_count;
    enum pg_anim_loop loop_mode;
    const struct pg_animation* anim;
};

void pg_anim_property_init(struct pg_anim_property* prop, vec4 base);

vec4 pg_anim_prop_value(struct pg_anim_property* prop, float t);
void pg_anim_prop_update(struct pg_anim_property* prop, float t);
int pg_anim_status(struct pg_anim_property* prop);

void pg_animate(struct pg_anim_property* prop, const struct pg_animation* anim,
                int start_frame, int loop_start, int loop_end,
                int loop_count, int loop_mode);

/************************************/
/*  Simple animations               */
/************************************/

struct pg_simple_anim {
    int absolute;
    vec4 start, end;
    float start_time, duration;
    pg_easing_func_t ease;
};

#define PG_SIMPLE_ANIM(...) (struct pg_simple_anim) { \
    .ease = pg_ease_inout_lerp, \
    .start_time = -1, .duration = 1.0f, \
    __VA_ARGS__ }

vec4 pg_simple_anim_value(struct pg_simple_anim* anim, float t);
int pg_simple_anim_finished(struct pg_simple_anim* anim, float t);

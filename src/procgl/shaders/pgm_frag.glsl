#version 330

uniform sampler2DArray pg_texture_0;
uniform float alpha_cutoff;

in vec3 f_tex_coord;

layout(location = 0) out vec4 frag_color;

void main()
{
    vec4 tex_color = texture(pg_texture_0, f_tex_coord);
    if(tex_color.a <= alpha_cutoff) discard;
    frag_color = tex_color.rgba;
}




#version 330

uniform mat4 pg_matrix_mvp;
uniform float tex_layer;

in vec3 v_position;
in vec2 v_tex_coord;

out vec3 f_tex_coord;

void main()
{
    f_tex_coord = vec3(v_tex_coord.xy, tex_layer);
    gl_Position = pg_matrix_mvp * vec4(v_position.xyz, 1);
}

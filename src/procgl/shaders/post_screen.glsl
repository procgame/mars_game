#version 330

uniform sampler2DArray pg_texture_0;

in vec2 f_tex_coord;

out vec4 frag_color;

void main()
{
    frag_color = texture(pg_texture_0, vec3(f_tex_coord, 0));
}

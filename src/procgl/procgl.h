/*  stdlib and external dependencies    */
#include <stdarg.h>
#include <stdio.h>
#include <string.h>
#include <GL/glew.h>
#include <SDL2/SDL.h>

/*  Code from other people which is now integrated into procgame
    possibly with some modifications    */
#include "ext/stb_rect_pack.h"
#include "ext/stb_truetype.h"
#include "ext/linmath.h"
#include "ext/ksort.h"
#include "easing.h"

/*  My own macro headers for dynamic arrays and hash tables */
#include "arr.h"
#include "mempool.h"
#include "htable.h"

/*  Procgame headers    */
#include "data_type.h"
#include "procgl_base.h"
#include "wave.h"
#include "heightmap.h"
#include "texture.h"
#include "font.h"
#include "viewer.h"
#include "shader.h"
#include "renderer.h"
#include "ezdraw.h"
#include "ezbox.h"
#include "animation.h"
#include "ui.h"
#include "vertex_buffer.h"
#include "audio.h"
#include "game_state.h"
#include "model.h"

/*  Some random macros I don't have a good place for    */
#define RANDI(r)    (rand() % (r))
#define RANDF(r)    ((float)rand() / RAND_MAX * (r))
#define RANDF2(r)   (((float)rand() / RAND_MAX * 2 - 1) * (r))

#define VEC4_TO_UINT(v) (uint32_t)( \
            (((uint32_t)(LM_SATURATE((v).x) * 255) & 0xFF) << 24) | \
            (((uint32_t)(LM_SATURATE((v).y) * 255) & 0xFF) << 16) | \
            (((uint32_t)(LM_SATURATE((v).z) * 255) & 0xFF) << 8) | \
            (((uint32_t)(LM_SATURATE((v).w) * 255) & 0xFF) << 0) )

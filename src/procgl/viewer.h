struct pg_viewer {
    vec2 size;
    vec2 scale;
    vec2 near_far;
    vec3 pos;
    quat orientation;
    mat4 view_matrix;
    mat4 proj_matrix;
    mat4 projview_matrix;
    mat4 projview_inverse;
};

void pg_viewer_perspective(struct pg_viewer* view, vec3 pos, quat orientation,
                           vec2 size, vec2 near_far);
void pg_viewer_ortho(struct pg_viewer* view, vec2 pos, vec2 scale, float rotation, vec2 size);
vec2 pg_viewer_project(struct pg_viewer* view, vec3 pos);
vec3 pg_viewer_unproject(struct pg_viewer* view, vec3 pos);

struct pg_viewport {
    vec2 pos, size;
};

vec2 pg_viewport_coord(struct pg_viewport* vp, vec2 in);
vec2 pg_viewport_res(struct pg_viewport* vp, vec2 screen);

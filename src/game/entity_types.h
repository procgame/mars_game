/************************************/
/*  Defining entity subtype data    */
/************************************/

#define MG_ENTITY_TICK(F) void F(struct mg_battle_state* b, struct mg_entity* ent)
#define MG_ENTITY_DRAW(F) void F(struct marsgame* mg, struct mg_entity* ent, float dt)
#define MG_ENTITY_RAYCAST(F) float F(struct mg_entity* ent, vec3 p, vec3 d)
#define MG_ENTITY_INTERACT(F) \
    void F(struct mg_battle_state* b, struct mg_entity* ent, struct mg_entity* user)
#define MG_ENTITY_USE(F) \
    void F(struct mg_battle_state* b, struct mg_entity* ent, struct mg_entity* user)

#define MG_ENTITY_SUBTYPE_DATA(NAME, ...) \
struct NAME { __VA_ARGS__ }; \
_Static_assert(sizeof(struct NAME) <= 128, \
    "Entity subtype (" #NAME ") data must be <128 bytes"); \
static inline struct NAME* NAME(struct mg_entity* ent) \
    { return (struct NAME*)(ent->subtype_data); }

/********************************************/
/*  COMMON BEHAVIORS (entity_behavior.c)    */
/********************************************/

MG_ENTITY_RAYCAST(mg_entity_raycast_sphere);

/****************************/
/*  PLAYER  (player.c)      */
/****************************/

mg_entpool_id_t mg_new_PC(struct mg_entity** ent_ptr);

MG_ENTITY_SUBTYPE_DATA(mg_player_data,
    mg_entpool_id_t held_item;
);


/****************************/
/*  ITEMS (item_behavior.c) */
/****************************/

enum mg_item_type {
    MG_ITEM_WEAPON_PISTOL,
    MG_ITEM_WEAPON_MACHINEGUN,
    MG_ITEM_WEAPON_PLAZGUN,
    MG_ITEM_UTILITY_MEDKIT,
    MG_ITEM_PICKUP_MATERIAL,
    MG_ITEM_PICKUP_SCIENCE,
    MG_NUM_ITEM_TYPES,
};

mg_entpool_id_t mg_new_item(enum mg_item_type type, struct mg_entity** ent_ptr);

MG_ENTITY_SUBTYPE_DATA(mg_weapon_data,
    int attack_timer;
);

/********************************/
/*  ENEMIES (enemy_behavior.c)  */
/********************************/

enum mg_enemy_type {
    MG_ENEMY_SPAWNER,
    MG_ENEMY_ALIEN,
    MG_NUM_ENEMY_TYPES,
};

mg_entpool_id_t mg_new_enemy(enum mg_enemy_type type, struct mg_entity** ent_ptr);

/************************************/
/*  ENV OBJECTS (envobj_behavior.c) */
/************************************/

enum mg_envobj_type {
    MG_ENVOBJ_GROUND,
    MG_ENVOBJ_BOULDER,
    MG_ENVOBJ_DOME_SEGMENT,
    MG_NUM_ENVOBJ_TYPES,
};

mg_entpool_id_t mg_new_envobj(enum mg_envobj_type type, struct mg_entity** ent_ptr);

MG_ENTITY_SUBTYPE_DATA(mg_envobj_dome_segment,
    vec2 size;
    float tip_scale;
    ubvec4 color;
)

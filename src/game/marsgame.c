#include <stdlib.h>
#include <limits.h>
#include "procgl/procgl.h"

#include "marsgame.h"

static void marsgame_update(struct pg_game_state*);
static void marsgame_tick(struct pg_game_state*);
static void marsgame_draw(struct pg_game_state*);
static void marsgame_deinit(void*);

static void generate_tex2(struct pg_texture* tex)
{
    uint32_t* gen = malloc(sizeof(uint32_t) * 64 * 64);

    struct pg_wave red_wave[16] = {
        PG_WAVE_MOD_SEAMLESS_2D(),
        PG_WAVE_MOD_OCTAVES(.octaves = 3, .ratio = 3, .decay = 0.5),
        PG_WAVE_FUNC_PERLIN(.frequency = { 4, 4, 12, 12 }, .scale = 0.5, .add = 0.5),
    };

    struct pg_wave rocks_wave[16] = {
        PG_WAVE_MOD_SEAMLESS_2D(),
        PG_WAVE_MOD_OCTAVES(.octaves = 3, .ratio = 4, .decay = 0.5),
        PG_WAVE_FUNC_PERLIN(.frequency = { 8, 8, 8, 8 }, .scale = 2, .add = -0.375),
    };

    int x, y;
    for(x = 0; x < 64; ++x) for(y = 0; y < 64; ++y) {
        vec4 p = vec4((float)x / 64, (float)y / 64);
        vec4 color = vec4();
        float red = pg_wave_sample(&PG_WAVE_ARRAY(red_wave, 16), 2, p);
        float gray = pg_wave_sample(&PG_WAVE_ARRAY(rocks_wave, 16), 2, p);
        if(red > gray * 2) {
            color = vec4(red * 0.5, red * 0.2, red * 0.15, 1);
        } else {
            color = vec4(gray, gray * 0.75, gray * 0.75, 1);
        }
        gen[x + y * 64] = VEC4_TO_UINT(color);
    }
    pg_texture_write_pixels(tex, gen, 1, 0, 0, 64,64);
    free(gen);
}

void marsgame_start(struct pg_game_state* state)
{
    pg_game_state_init(state, pg_time(), 60, 3);
    struct marsgame* mg = malloc(sizeof(*mg));
    *mg = (struct marsgame){};
    pg_ezbox_rig_read_from_file(&mg->human_rig, "res/human_bones.rig");
    mg_build_human_model(&mg->human_model, &mg->human_rig);
    /*  Setup a render target   */
    int w, h;
    pg_screen_size(&w, &h);
    w /= 3;
    h /= 3;
    mg->res = vec2(w, h);
    mg->halfres = vec2_scale(mg->res, 0.5);
    pg_viewer_ortho(&mg->worldview, vec2(), vec2(1, 1), 0, mg->res);
    pg_viewer_ortho(&mg->screenview, vec2(), vec2(1, 1), 0, mg->halfres);
    float ar = (float)w / h;
    mg->ar = ar;
    /*  Load a font texture */
    char font_tex_size[16];
    snprintf(font_tex_size, 16, ":%d,%d", h, h);
    const char* tex_files[5] = { "res/font_8x8.png", ":64,64",
        font_tex_size, font_tex_size, font_tex_size };
    pg_texture_from_files(&mg->sgtex, 5, tex_files,
        PG_TEXTURE_OPTS(.filter_min = GL_NEAREST, .filter_mag = GL_NEAREST,
                        .wrap_x = GL_CLAMP_TO_EDGE, .wrap_y = GL_CLAMP_TO_EDGE));
    pg_texture_set_atlas(&mg->sgtex, 0, 8, 8);
    generate_tex2(&mg->sgtex);
    int font_pixels = h;
    if(h < 400) {
        pg_font_init(&mg->sgfont[0], "res/Orbitron-Bold.ttf", &mg->sgtex, 2, h*0.0625, 8);
    } else {
        pg_font_init(&mg->sgfont[0], "res/Orbitron-Regular.ttf", &mg->sgtex, 2, h*0.0625, 8);
    }
    pg_font_init(&mg->sgfont[1], "res/NASDAQER.ttf", &mg->sgtex, 3, h*0.125, 4);
    pg_font_init(&mg->sgfont[2], "res/Caveat-Bold.ttf", &mg->sgtex, 4, h*0.125, 4);
    mg->sgfont[1].line_move *= 0.55;
    pg_texture_buffer(&mg->sgtex);
    /*  Get 64x64 in UV coords for the texture  */
    mg->sgtex_64 = vec2(64.0f / mg->sgtex.w, 64.0f / mg->sgtex.h);
    /*  Load model textures */
    const char* modeltex_files[] = { "res/FUCKBLENDERFUCK.png" };
    pg_texture_from_files(&mg->modeltex, 1, modeltex_files,
        PG_TEXTURE_OPTS(.filter_min = GL_NEAREST, .filter_mag = GL_NEAREST,
                        .wrap_x = GL_CLAMP_TO_EDGE, .wrap_y = GL_CLAMP_TO_EDGE));
    pg_texture_buffer(&mg->modeltex);
    /*  Make the render textures    */
    pg_texture_init(&mg->pptex[0], PG_UBVEC4, w, h, 1, PG_TEXTURE_OPTS(
        .filter_mag = GL_NEAREST,
        .wrap_x = GL_CLAMP_TO_EDGE, .wrap_y = GL_CLAMP_TO_EDGE));
    pg_texture_init(&mg->pptex[1], PG_UBVEC4, w, h, 1, PG_TEXTURE_OPTS(
        .filter_mag = GL_NEAREST,
        .wrap_x = GL_CLAMP_TO_EDGE, .wrap_y = GL_CLAMP_TO_EDGE));
    pg_texture_init_depth_stencil(&mg->ppdepth, w, h, 1, PG_TEXTURE_OPTS());
    pg_renderbuffer_init(&mg->ppbuf[0]);
    pg_renderbuffer_init(&mg->ppbuf[1]);
    pg_renderbuffer_attach(&mg->ppbuf[0], &mg->pptex[0], 0, 0, GL_COLOR_ATTACHMENT0);
    pg_renderbuffer_attach(&mg->ppbuf[1], &mg->pptex[1], 0, 0, GL_COLOR_ATTACHMENT0);
    pg_renderbuffer_attach(&mg->ppbuf[0], &mg->ppdepth, 0, 1, GL_DEPTH_STENCIL_ATTACHMENT);
    pg_renderbuffer_attach(&mg->ppbuf[1], &mg->ppdepth, 0, 1, GL_DEPTH_STENCIL_ATTACHMENT);
    pg_renderbuffer_build(&mg->ppbuf[0]);
    pg_renderbuffer_build(&mg->ppbuf[1]);
    pg_rendertarget_init(&mg->target, &mg->ppbuf[0], &mg->ppbuf[1]);

    /*  Initialize procgl renderer  */
    pg_renderer_init(&mg->rend);
    
    /*  Load models */
    pg_model_init_vertex_buffer(&mg->model_verts);
    pg_model_load(&mg->test_model, &mg->model_verts, "res/exported.pg");
    pg_vertex_buffer_buffer(&mg->model_verts);
    pg_renderer_vertex_spec(&mg->rend, "pgm_rigged", &mg->model_verts);


    /*  Initialize render passes    */
    pg_boxbatch_init(&mg->boxbatch, 16384, &mg->sgtex);
    pg_quadbatch_init(&mg->quadbatch, 16384, &mg->sgtex);

    mg_particles_init(mg);

    pg_boxbatch_init_pass(&mg->boxbatch, &mg->box_pass, &mg->target, vec2(mg->sgtex.w, mg->sgtex.h));
    pg_renderpass_viewer(&mg->box_pass, &mg->worldview);

    pg_quadbatch_init_pass(&mg->quadbatch, &mg->world_pass, &mg->target);
    pg_renderpass_viewer(&mg->world_pass, &mg->worldview);
    pg_renderpass_depth_write(&mg->world_pass, 1);
    pg_renderpass_depth_test(&mg->world_pass, 1, GL_LEQUAL);
    pg_renderpass_uniform(&mg->world_pass, "alpha_cutoff", PG_FLOAT,
        &PG_TYPE_FLOAT(0));

    pg_quadbatch_init_pass(&mg->quadbatch, &mg->overlay_pass, &mg->target);
    pg_renderpass_viewer(&mg->overlay_pass, &mg->screenview);
    pg_renderpass_texture(&mg->overlay_pass, 1, &mg->sgtex, PG_TEXTURE_OPTS(
        .filter_min = GL_LINEAR, .filter_mag = GL_LINEAR,
        .wrap_x = GL_CLAMP_TO_EDGE, .wrap_y = GL_CLAMP_TO_EDGE ));
    pg_renderpass_uniform(&mg->overlay_pass, "alpha_cutoff", PG_FLOAT,
        &PG_TYPE_FLOAT(-1));

    pg_ezpass_fog(&mg->fog_pass, &mg->target);
    pg_ezdraw_fog(&mg->fog_pass, &(struct pg_draw_post_fog){
        .color = vec3(0.8, 0.6, 0.6), .near = -10, .far = 60 });

    pg_ezpass_screen(&mg->copy_pass, &mg->target);

    pg_ezpass_rigged_model(&mg->model_pass, &mg->target, &mg->model_verts, &mg->modeltex);
    pg_renderpass_viewer(&mg->model_pass, &mg->worldview);
    pg_renderpass_uniform(&mg->model_pass, "alpha_cutoff", PG_FLOAT,
        &PG_TYPE_FLOAT(0));

    /*  Make a UI context   */
    pg_ui_init(&mg->ui, &mg->target, &mg->sgtex, &PG_TEXT_FORMATTER(
            .fonts = PG_FONTS(3, &mg->sgfont[0], &mg->sgfont[1], &mg->sgfont[2]),
            .size = vec2(1,1), .htab = 1 ));
    *pg_ui_variable(&mg->ui, mg->ui.root, "marsgame") = PG_TYPE_POINTER(mg);
    pg_ui_enabled(&mg->ui, mg->ui.root, 1);
    mg->ui.debug_action_areas = 0;

    /*  Add the rendergroup to the first pass   */
    /*  Add the two passes to the renderer  */
    ARR_PUSH(mg->rend.passes, &mg->world_pass);
    ARR_PUSH(mg->rend.passes, &mg->box_pass);
    ARR_PUSH(mg->rend.passes, &mg->model_pass);
    ARR_PUSH(mg->rend.passes, &mg->fog_pass);
    ARR_PUSH(mg->rend.passes, &mg->particle_system.particles_pass);
    ARR_PUSH(mg->rend.passes, &mg->overlay_pass);
    ARR_PUSH(mg->rend.passes, &mg->ui.rendpass);
    ARR_PUSH(mg->rend.passes, &mg->copy_pass);

    mg_init_battle(mg);
    mg_init_homebase(mg);
    mg_init_missions(mg);
    //mg_start_homebase(mg);
    //mg->state = MG_HOMEBASE;
    //mg_start_missions(mg);
    //mg->state = MG_MISSIONS;
    mg_start_battle(mg);
    mg->state = MG_BATTLE;

    //pg_mouse_mode(0);
    /*  Fill in state structure */
    state->data = mg;
    state->update = marsgame_update;
    state->tick = marsgame_tick;
    state->draw = marsgame_draw;
    state->deinit = marsgame_deinit;
}

static void marsgame_tick(struct pg_game_state* state)
{
    struct marsgame* mg = state->data;
    if(pg_user_exit()) state->running = 0;
    if(mg->state == MG_BATTLE) mg_battle_tick(mg);
    else if(mg->state == MG_HOMEBASE) mg_homebase_tick(mg);
    else if(mg->state == MG_MISSIONS) mg_missions_tick(mg);
    pg_ui_update(&mg->ui, (float)state->ticks / 60.0f);
    pg_flush_input();
}

static void marsgame_update(struct pg_game_state* state)
{
    struct marsgame* mg = state->data;
    pg_poll_input();
    //mg_battle_update(mg);
    if(mg->state == MG_BATTLE) mg_battle_update(mg);
    else if(mg->state == MG_HOMEBASE) mg_homebase_update(mg);
    else if(mg->state == MG_MISSIONS) mg_missions_update(mg);
}

static void marsgame_draw(struct pg_game_state* state)
{
    struct marsgame* mg = state->data;
    float dt = state->tick_over;
    pg_rendertarget_clear(&mg->target);
    pg_ui_draw(&mg->ui, dt / 60.0f);
    pg_renderpass_clear(&mg->box_pass);
    pg_renderpass_clear(&mg->world_pass);
    pg_renderpass_clear(&mg->overlay_pass);
    pg_renderpass_clear(&mg->model_pass);
    if(mg->state == MG_BATTLE) mg_battle_draw(mg, dt);
    else if(mg->state == MG_HOMEBASE) mg_homebase_draw(mg, dt);
    else if(mg->state == MG_MISSIONS) mg_missions_draw(mg, dt);
    pg_renderer_draw_frame(&mg->rend);
}

void marsgame_deinit(void* data)
{
    struct marsgame* mg = data;
    pg_texture_deinit(&mg->sgtex);
    pg_texture_deinit(&mg->pptex[0]);
    pg_texture_deinit(&mg->pptex[1]);
    pg_texture_deinit(&mg->ppdepth);
    pg_font_deinit(&mg->sgfont[0]);
    pg_font_deinit(&mg->sgfont[1]);
    pg_font_deinit(&mg->sgfont[2]);
    pg_renderbuffer_deinit(&mg->ppbuf[0]);
    pg_renderbuffer_deinit(&mg->ppbuf[1]);
    pg_renderer_deinit(&mg->rend);
    pg_renderpass_deinit(&mg->world_pass);
    pg_renderpass_deinit(&mg->overlay_pass);
    pg_renderpass_deinit(&mg->copy_pass);
    pg_renderpass_deinit(&mg->box_pass);
    pg_renderpass_deinit(&mg->model_pass);
    pg_renderpass_deinit(&mg->fog_pass);
    pg_quadbatch_deinit(&mg->quadbatch);
    pg_boxbatch_deinit(&mg->boxbatch);
    pg_ui_deinit(&mg->ui);
    mg_deinit_battle(&mg->battle);
    mg_entpool_deinit();
    free(mg);
}

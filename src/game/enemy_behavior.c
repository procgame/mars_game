#include "procgl/procgl.h"
#include "marsgame.h"

/************************/
/*  ENEMY DATA          */
/************************/

/*  Forward declarations (defined below)    */
MG_ENTITY_TICK(mg_enemy_tick_spawner);
MG_ENTITY_DRAW(mg_enemy_draw_spawner);
MG_ENTITY_TICK(mg_enemy_tick_alien);
MG_ENTITY_DRAW(mg_enemy_draw_alien);

/*  Base entity behavior    */
const struct mg_entity_base MG_ENEMY_BASE[MG_NUM_ENEMY_TYPES] = {
    [MG_ENEMY_SPAWNER] = { .name = "spawner",
        .max_HP = 100, .bound = 1.5,
        .tick = mg_enemy_tick_spawner,
        .draw = mg_enemy_draw_spawner,
        .raycast = mg_entity_raycast_sphere, },
    [MG_ENEMY_ALIEN] = { .name = "spawner",
        .max_HP = 20, .bound = 1,
        .tick = mg_enemy_tick_alien,
        .draw = mg_enemy_draw_alien,
        .raycast = mg_entity_raycast_sphere, },
};

/*  Enemy allocation    */
mg_entpool_id_t mg_new_enemy(enum mg_enemy_type type, struct mg_entity** ent_ptr)
{
    struct mg_entity* ent;
    mg_entpool_id_t id = mg_entpool_alloc(1, &ent);
    mg_entity_init_from_base(ent, &MG_ENEMY_BASE[type]);
    ent->type = MG_ENTITY_ENEMY;
    ent->subtype = type;
    if(ent_ptr) *ent_ptr = ent;
    return id;
}

/********************************/
/*  Behavior functions          */
/********************************/

MG_ENTITY_TICK(mg_enemy_tick_spawner)
{
    if(ent->HP <= 0) {
        vec3 pos = ent->pos;
        struct mg_entity* sci_ent;
        mg_entpool_id_t sci_id = mg_new_item(MG_ITEM_PICKUP_SCIENCE, &sci_ent);
        sci_ent->pos = vec3(pos.x, pos.y, 0);
        ARR_PUSH(b->space.all_entities, sci_id);
    } else if(b->sim_tick % 240 == 0) {
        vec3 pos = ent->pos;
        struct mg_entity* new_ent;
        mg_entpool_id_t new_id = mg_new_enemy(MG_ENEMY_ALIEN, &new_ent);
        new_ent->pos = pos;
        new_ent->vel = vec3(RANDF2(1), RANDF2(1), RANDF(1));
        ARR_PUSH(b->space.all_entities, new_id);
    }
}

MG_ENTITY_DRAW(mg_enemy_draw_spawner)
{
    pg_boxbatch_add_box(&mg->boxbatch, &PG_EZBOX(
        .pos = vec3(VEC_XY(ent->pos), ent->pos.z), .iso_scale = 3,
        .cap_scale_top = vec2(0.25, 0.25),
        .color_mod = ubvec4(0), .color_add = ubvec4(255, 64, 64, 255) ));
}

MG_ENTITY_TICK(mg_enemy_tick_alien)
{
    struct mg_entity* plr = mg_entpool_get(b->plr);
    vec3 plr_pos = vec3(plr->pos.x, plr->pos.y, plr->pos.z + 1);
    vec3 ent_to_plr = vec3_sub(plr_pos, ent->pos);
    float plr_dist2 = vec3_len2(ent_to_plr);
    /*
    if(ent->attack_timer) --ent->attack_timer;
    if(plr_dist2 < 2) {
        if(ent->attack_timer <= 0) {
            plr->HP -= 5;
            ent->attack_timer = 60;
        }
    } else {
        ent_to_plr = vec3_tolen(ent_to_plr, 0.02);
        ent->vel = vec3_add(ent->vel, ent_to_plr);
    }*/
    ent->pos = vec3_add(ent->pos, ent->vel);
    ent->vel = vec3_scale(ent->vel, 0.8);
}

MG_ENTITY_DRAW(mg_enemy_draw_alien)
{
    vec3 drawpos = vec3(POS_LERP3(ent,dt));
    pg_boxbatch_add_box(&mg->boxbatch, &PG_EZBOX(
        .pos = drawpos, .iso_scale = 1,
        .color_mod = ubvec4(0), .color_add = ubvec4(255, 128, 128, 255) ));
}



struct marsgame;
struct mg_battle_state;
struct mg_entity;

/****************************/
/*  BEHAVIOR / BASE TYPES   */
/****************************/

enum mg_entity_base_type {
    MG_ENTITY_PLAYER =      (1 << 0),
    MG_ENTITY_ENEMY =       (1 << 1),
    MG_ENTITY_ITEM =        (1 << 2),
    MG_ENTITY_ENVOBJ =      (1 << 3),
};

/*  Entity behavior is encapsulated in a standard set of function pointers  */
typedef void (*mg_entity_tick_fn_t)(struct mg_battle_state*, struct mg_entity*);
typedef void (*mg_entity_draw_fn_t)(struct marsgame*, struct mg_entity*, float);
typedef float (*mg_entity_raycast_fn_t)(struct mg_entity*, vec3, vec3);
typedef void (*mg_entity_interact_fn_t)(struct mg_battle_state*, struct mg_entity*, struct mg_entity*);
typedef void (*mg_entity_use_fn_t)(struct mg_battle_state*, struct mg_entity*, struct mg_entity*);

/*  Those along with a few auxiliary bits of data form a "base" for any
    entity type */
struct mg_entity_base {
    /*  Base stats  */
    char name[32];
    vec3 bound[2];
    int max_HP;
    /*  Collisions  */
    vec3 collider_offset;
    struct mg_collider collider;
    /*  Behavior    */
    mg_entity_tick_fn_t tick;
    mg_entity_draw_fn_t draw;
    mg_entity_raycast_fn_t raycast;
    mg_entity_interact_fn_t interact;
    mg_entity_use_fn_t use;
};

/****************************/
/*  ACTIVE DATA             */
/****************************/

/*  Entities are stored in a global allocation pool */
PG_MEMPOOL_DECLARE_GLOBAL(struct mg_entity, mg_entpool);
typedef ARR_T(mg_entpool_id_t) mg_entity_arr_t;

/*  Running data for each entity instance   */
struct mg_entity {
    /*  Memory pool */
    int alloc;
    /*  Book-keeping    */
    int dead;
    int last_tick;
    /********************************/
    /*  Query data                  */
    uint64_t query;
    vec3 bound[2], cur_bound[2];
    struct mg_entity_ray_query {
        vec3 enter, exit;
        float dist;
    } ray_hit;
    /****************/
    /*  Type data   */
    enum mg_entity_base_type type;
    int subtype;
    char subtype_data[128];
    /********************/
    /*  Physics data    */
    // Game-logic-driven entity pushes: last_push is the accumulated
    // pushes from the last frame
    vec3 move[2];
    vec3 push[2];
    vec3 pos, vel;
    vec3 collider_offset;
    struct mg_collider collider;
    int n_phys_pairs;
    /************************/
    /*  Actual game stuff   */
    char name[32];
    int HP, max_HP;
    quat rot;
    vec2 rot_euler;
    int ground;
    /****************/
    /*  Behavior    */
    mg_entity_tick_fn_t tick;
    mg_entity_draw_fn_t draw;
    mg_entity_raycast_fn_t raycast;
    mg_entity_interact_fn_t interact;
    mg_entity_use_fn_t use;
    /************************************/
    /*  Debugging                       */
    #ifdef MG_DEBUG_COLLISIONS
    int n_debugged_pairs;
    #endif
};

void mg_entities_tick(struct mg_battle_state* b);
void mg_entities_draw(struct marsgame* mg, float dt);

void mg_entity_init_from_base(struct mg_entity* ent, const struct mg_entity_base* base);
void mg_entity_set_bound_centered(struct mg_entity* ent, float size);
void mg_entity_set_pos(struct mg_entity* ent, vec3 pos);
void mg_entity_push(struct mg_entity* ent, vec3 vel);

void mg_entity_use_held_item(struct mg_battle_state* b, struct mg_entity* ent);
void mg_entity_interact(struct mg_battle_state* b, struct mg_entity* target,
                        struct mg_entity* actor);

void mg_entity_collide(struct mg_collision* out, struct mg_entity* ent_a, struct mg_entity* ent_b);
void mg_entity_sync_collider(struct mg_entity* ent);

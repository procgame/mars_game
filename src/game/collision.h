
struct mg_collision {
    int did_collide;
    float sep_len;
    vec3 sep_axis;
    vec3 sep_norm;
    vec3 surf_norm;
    float impulse[2];
};

#define MG_COLLIDER_STATIC      (1 << 0)
#define MG_COLLIDER_DEBUGGED    (1 << 1)
#define MG_COLLIDER_ON_SURFACE  (1 << 2)
#define MG_COLLIDER_PROCESSED   (1 << 3)

struct mg_collider {
    uint32_t flags;
    float mass;
    vec3 pos, vel;
    vec3 push, push_norm;
    float push_len;
    int n_pushes;
    enum mg_collider_type {
        MG_NO_COLLIDER,
        MG_COLLIDER_SPHERE_FRICTION,
        MG_COLLIDER_SPHERE,
        MG_COLLIDER_PLANE,
        MG_NUM_COLLIDER_TYPES,
    } type;
    union {
        struct { vec3 sph; float radius; };
        struct { float plane; };
    };
};

const char* MG_COLLIDER_NAME[MG_NUM_COLLIDER_TYPES];

void mg_collider_resolve(struct mg_collision* out,
                         struct mg_collider* c0, struct mg_collider* c1);
void mg_collider_finalize(struct mg_collider* coll);
vec3 mg_collider_slide(struct mg_collider* coll);
vec3 mg_collider_bounce(struct mg_collider* coll);

void mg_debug_print_collision(struct mg_collision* coll,
                              struct mg_collider* c0, char* name0,
                              struct mg_collider* c1, char* name1);

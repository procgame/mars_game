#include "procgl/procgl.h"
#include "../marsgame.h"

static void scroll_update_items(struct pg_ui_context* ctx, pg_ui_t scroll,
                                struct mg_homebase_state* h);

static void recruiting_view_option(struct pg_ui_context* ctx,
                                   struct mg_homebase_state* h,
                                   mg_homebase_person_id_t ppl_id)
{
    pg_ui_t recruiting = pg_ui_child_path(ctx, h->ui, "dome_base.dome_menu.recruiting");
    pg_ui_t preview = pg_ui_child(ctx, recruiting, "preview");
    pg_ui_t confirm_btn = pg_ui_child(ctx, preview, "confirm_btn");
    if(ppl_id == -1) {
        pg_ui_enabled(ctx, preview, 0);
        return;
    }
    pg_ui_enabled(ctx, preview, 1);
    struct mg_homebase_person* person = mg_homebase_person_get(&h->ppl_pool, ppl_id);
    struct pg_type* btn_info = pg_ui_variable(ctx, confirm_btn, "confirm_btn");
    btn_info->i[1] = ppl_id;
    if(h->personnel.len == 1) {
        pg_ui_set_text(ctx, confirm_btn, L"%S(0.75)CANNOT DISMISS\nONLY FIGHTER", 48);
        btn_info->i[0] = 0;
    } else {
        pg_ui_set_text(ctx, confirm_btn, L"DISMISS", 32);
        btn_info->i[0] = 1;
    }
    pg_ui_enabled(ctx, confirm_btn, 1);
}

static int recruiting_click_confirm(struct pg_ui_context* ctx, pg_ui_t confirm_btn,
                                      struct pg_ui_event* event)
{
    struct marsgame* mg = pg_ui_variable(ctx, ctx->root, "marsgame")->ptr[0];
    struct mg_homebase_state* h = &mg->homebase;
    struct pg_type* btn_info = pg_ui_variable(ctx, confirm_btn, "confirm_btn");
    struct mg_homebase_person* person =
        mg_homebase_person_get(&h->ppl_pool, btn_info->i[1]);
    if(btn_info->i[0]) {
        int i;
        for(i = 0; i < h->personnel.len; ++i) {
            if(h->personnel.data[i] == btn_info->i[1]) {
                ARR_SPLICE(h->personnel, i, 1);
                break;
            }
        }
        pg_ui_t scroll = pg_ui_child_path(ctx, h->ui,
            "dome_base.dome_menu.recruiting.person_list.scroll");
        scroll_update_items(ctx, scroll, h);
        recruiting_view_option(ctx, h, -1);
    }
    return 1;
}

/*  List item:
        item_info:  i
            0   scroll list reference
            1   index in list
            2   person id
            3   is currently selected
    Scroll list:
        list_info: i
            0   selection index
            1   selected person id
            2   current length
        scroll_info: f
            0   current scroll
            1   max scroll
        item_arr: ptr
            0   pg_ui_arr_t (all list items)
*/

static int list_click(struct pg_ui_context* ctx, pg_ui_t elem, struct pg_ui_event* event)
{
    struct marsgame* mg = pg_ui_variable(ctx, ctx->root, "marsgame")->ptr[0];
    struct mg_homebase_state* h = &mg->homebase;
    struct mg_homebase_dome* dome =
        mg_homebase_dome_get(&h->dome_pool, h->domes.data[h->selected_dome]);
    struct pg_type* item_info = pg_ui_variable(ctx, elem, "item_info");
    pg_ui_t list = item_info->i[0];
    int idx = item_info->i[1];
    mg_homebase_person_id_t ppl_id = item_info->i[2];
    struct pg_type* list_info = pg_ui_variable(ctx, list, "list_info");
    /*  Set list's "selected person" to item's person index   */
    if(ppl_id == -1) {
        /*  Recruit a new person    */
        struct mg_homebase_person* recruit;
        ppl_id = mg_homebase_person_alloc(&h->ppl_pool, 1, &recruit);
        const wchar_t* firstnames[8] = {
            L"Alice", L"Bob", L"Clara", L"Daniel",
            L"Ellen", L"Filip", L"Glenna", L"Henry" };
        const wchar_t* lastnames[8] = {
            L"Andrews", L"Blake", L"Cohen", L"Dorey",
            L"Engels", L"Ford", L"Grey", L"Hobbes" };
        int first = rand() % 8;
        int last = rand() % 8;
        swprintf(recruit->name, 32, L"%ls %ls", firstnames[first], lastnames[last]);
        recruit->assigned_mission = -1;
        ARR_PUSH(h->personnel, ppl_id);
    } else {
        list_info->i[0] = item_info->i[2];
        list_info->i[1] = ppl_id;
        recruiting_view_option(ctx, h, ppl_id);
    }
    /*  Update list */
    scroll_update_items(ctx, list, h);
    return 1;
}

static pg_ui_t scroll_make_item(struct pg_ui_context* ctx, pg_ui_t scroll, int idx)
{
    char item_name[32];
    snprintf(item_name, 32, "scroll_item_%d", idx);
    pg_ui_t item = UI_ELEM(scroll, item_name,
        .pos = vec2(-0.45, idx*-0.1-0.2), .draw = PG_UI_TEXT_ONLY,
        .action_area = vec4(0,0.025,1,0.05),
        .text_size = vec2(0.05,0.05),
        .cb_click = list_click );
    *pg_ui_variable(ctx, item, "item_info") = PG_TYPE_INT(scroll, idx, -1, 0);
    pg_ui_arr_t* item_arr = pg_ui_variable(ctx, scroll, "item_arr")->ptr[0];
    ARR_PUSH(*item_arr, item);
    return item;
}

static void scroll_update_items(struct pg_ui_context* ctx, pg_ui_t scroll,
                                struct mg_homebase_state* h)
{
    struct mg_homebase_dome* dome =
        mg_homebase_dome_get(&h->dome_pool, h->domes.data[h->selected_dome]);
    struct pg_type* list_info = pg_ui_variable(ctx, scroll, "list_info");
    struct pg_type* scroll_info = pg_ui_variable(ctx, scroll, "scroll_info");
    pg_ui_arr_t* item_arr = pg_ui_variable(ctx, scroll, "item_arr")->ptr[0];
    pg_ui_t item;
    mg_homebase_person_id_t ppl_id;
    int i;
    int list_len = h->personnel.len + 1;
    scroll_info->f[1] = LM_MAX(0, list_len * 0.1 - 0.75);
    list_info->i[2] = list_len;
    int num_items = LM_MAX(item_arr->len, list_len);
    for(i = 0; i < num_items; ++i) {
        int ppl_idx = i - 1;
        if(ppl_idx >= 0 && ppl_idx >= h->personnel.len) {
            pg_ui_enabled(ctx, item_arr->data[i], 0);
            continue;
        } else if(i >= item_arr->len) {
            item = scroll_make_item(ctx, scroll, i);
        } else {
            item = item_arr->data[i];
        }
        pg_ui_enabled(ctx, item, 1);
        struct pg_type* item_info = pg_ui_variable(ctx, item, "item_info");
        if(i == 0) {
            pg_ui_set_text(ctx, item, L"NEW RECRUIT", 16);
            *pg_ui_get_property(ctx, item, PG_UI_TEXT_COLOR) = vec4(0.75,1,0.75,1);
            item_info->i[2] = -1;
        } else {
            ppl_id = h->personnel.data[ppl_idx];
            struct mg_homebase_person* ppl = mg_homebase_person_get(&h->ppl_pool, ppl_id);
            pg_ui_set_text(ctx, item, ppl->name, 32);
            item_info->i[2] = ppl_id;
        }
        if(list_info->i[1] == -1) {
            item_info->i[3] = 0;
            pg_ui_cancel_anim(ctx, item, PG_UI_POS);
        } else if(item_info->i[2] != list_info->i[1] && item_info->i[3]) {
            item_info->i[3] = 0;
            pg_ui_simple_anim(ctx, item, PG_UI_POS,
                &PG_SIMPLE_ANIM(.duration = 0.25, .start = vec4(0.025,0) ));
        } else if(item_info->i[2] == list_info->i[1] && !item_info->i[3]) {
            item_info->i[3] = 1;
            pg_ui_simple_anim(ctx, item, PG_UI_POS,
                &PG_SIMPLE_ANIM(.duration = 0.25, .end = vec4(0.025,0) ));
        }
    }
}

static int list_scroll(struct pg_ui_context* ctx, pg_ui_t scroll_background,
                       struct pg_ui_event* event)
{
    pg_ui_t scroll = pg_ui_variable(ctx, scroll_background, "scroll")->i[0];
    struct pg_type* scroll_info = pg_ui_variable(ctx, scroll, "scroll_info");
    vec4* scroll_pos = pg_ui_get_property(ctx, scroll, PG_UI_POS);
    scroll_pos->y -= event->mouse.scroll_direction * 0.075;
    scroll_pos->y = LM_CLAMP(scroll_pos->y, 0, scroll_info->f[1]);
    scroll_info->f[0] = scroll_pos->y;
    return 1;
}

pg_ui_t ui_recruiting_create(struct pg_ui_context* ctx, struct mg_homebase_state* h)
{
    pg_ui_t dome_base = pg_ui_child(ctx, h->ui, "dome_base");
    pg_ui_t dome_menu = pg_ui_child(ctx, dome_base, "dome_menu");
    pg_ui_t recruiting = UI_GROUP(dome_menu, "recruiting", .enabled = 0);
    pg_ui_t person_list = UI_GROUP(recruiting, "person_list",
        .pos = vec2(-0.6, 0),
        .enable_clip = 1, .clip = vec4(0,-0.5, 1.1,0.8) );
    pg_ui_t list_background = UI_ELEM(person_list, "background", .layer = -1,
        .draw = PG_UI_IMG_ONLY, .pos = vec2(0,-0.5),
        .img_scale = vec2(1.1,0.8),
        .img_color_mod = vec4(0), .img_color_add = vec4(0,0,0,0.25),
        .action_area = vec4(0,0,1,1),
        .cb_scroll = list_scroll);
    pg_ui_t person_scroll = UI_GROUP(person_list, "scroll");
    pg_ui_arr_t* scroll_items = malloc(sizeof(*scroll_items));
    ARR_INIT(*scroll_items);
    pg_ui_variable(ctx, person_scroll, "list_info")->i[1] = -1;
    pg_ui_variable(ctx, person_scroll, "item_arr")->ptr[0] = scroll_items;
    pg_ui_variable(ctx, list_background, "scroll")->i[0] = person_scroll;
    pg_ui_t preview = UI_GROUP(recruiting, "preview", .enabled = 0,
        .pos = vec2(0.6, 0) );
    pg_ui_t confirm_btn = UI_ELEM(preview, "confirm_btn",
        .pos = vec2(0, -0.8), .draw = PG_UI_TEXT_ONLY,
        .action_item = PG_UI_ACTION_TEXT,
        .text_formatter = &PG_TEXT_FORMATTER_MOD(ctx->default_text_formatter,
            .align = PG_TEXT_CENTER, .default_font = 1, .size = vec2(0.075,0.075)),
        .cb_click = recruiting_click_confirm );
    return recruiting;
}

void ui_recruiting(struct pg_ui_context* ctx, struct mg_homebase_state* h, int dome_idx)
{
    pg_ui_t dome_base = pg_ui_child(ctx, h->ui, "dome_base");
    pg_ui_t dome_menu = pg_ui_child(ctx, dome_base, "dome_menu");
    pg_ui_t dome_menu_base = pg_ui_child(ctx, dome_menu, "base");
    pg_ui_t recruiting = pg_ui_child(ctx, dome_menu, "recruiting");
    pg_ui_t preview = pg_ui_child(ctx, recruiting, "preview");
    pg_ui_enabled(ctx, preview, 0);
    int did_open = click_dome_submenu(ctx, dome_menu, recruiting);
    if(!did_open) return;
    pg_ui_t scroll = pg_ui_child_path(ctx, recruiting, "person_list.scroll");
    struct pg_type* list_info = pg_ui_variable(ctx, scroll, "list_info");
    list_info->i[1] = -1;
    scroll_update_items(ctx, scroll, h);
    pg_ui_set_text(ctx, dome_menu_base, L"PEOPLE", 32);
}



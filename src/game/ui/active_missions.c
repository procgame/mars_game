#include "procgl/procgl.h"
#include "../marsgame.h"

static void msn_scroll_update_items(struct pg_ui_context* ctx, pg_ui_t scroll,
                                struct mg_homebase_state* h);
static void ppl_scroll_update_items(struct pg_ui_context* ctx, pg_ui_t scroll,
                                struct mg_homebase_state* h);


/*  List item:
        item_info:  i
            0   scroll list reference
            1   index in list
            2   person id
            3   is currently selected
    Scroll list:
        list_info: i
            0   selection index
            1   selected person id
            2   current length
        scroll_info: f
            0   current scroll
            1   max scroll
        item_arr: ptr
            0   pg_ui_arr_t (all list items)
*/

static int msn_click(struct pg_ui_context* ctx, pg_ui_t elem, struct pg_ui_event* event)
{
    struct marsgame* mg = pg_ui_variable(ctx, ctx->root, "marsgame")->ptr[0];
    struct mg_homebase_state* h = &mg->homebase;
    struct mg_homebase_dome* dome =
        mg_homebase_dome_get(&h->dome_pool, h->domes.data[h->selected_dome]);
    struct pg_type* item_info = pg_ui_variable(ctx, elem, "item_info");
    pg_ui_t list = item_info->i[0];
    int idx = item_info->i[1];
    mg_homebase_person_id_t ass_id = item_info->i[3];
    mg_homebase_bulletin_id_t blt_id = item_info->i[2];
    struct mg_homebase_bulletin* blt =
        mg_homebase_bulletin_get(&h->blt_pool, blt_id);
    if(h->selected_person != -1) {
        if(ass_id != -1) {
            struct mg_homebase_person* person =
                mg_homebase_person_get(&h->ppl_pool, ass_id);
            person->assigned_mission = -1;
        }
        mg_homebase_person_id_t ppl_id = h->personnel.data[h->selected_person];
        struct mg_homebase_person* person =
            mg_homebase_person_get(&h->ppl_pool, ppl_id);
        person->assigned_mission = blt_id;
    }
    /*  Update list */
    msn_scroll_update_items(ctx, list, h);
    pg_ui_t ppl_scroll = pg_ui_variable(ctx, list, "ppl_scroll")->i[0];
    ppl_scroll_update_items(ctx, ppl_scroll, h);
    return 1;
}

static pg_ui_t msn_scroll_make_item(struct pg_ui_context* ctx, pg_ui_t scroll, int idx)
{
    char item_name[32];
    snprintf(item_name, 32, "scroll_item_%d", idx);
    pg_ui_t item = UI_ELEM(scroll, item_name,
        .pos = vec2(-0.45, idx*-0.25 - 0.2), .draw = PG_UI_TEXT_ONLY,
        .action_area = vec4(0,-0.025,1,0.1),
        .text_pos = vec2(0,0), .text_size = vec2(0.05,0.05),
        .cb_click = msn_click );
    *pg_ui_variable(ctx, item, "item_info") = PG_TYPE_INT(scroll, idx, -1, 0);
    pg_ui_arr_t* item_arr = pg_ui_variable(ctx, scroll, "item_arr")->ptr[0];
    ARR_PUSH(*item_arr, item);
    return item;
}

static void msn_scroll_update_items(struct pg_ui_context* ctx, pg_ui_t scroll,
                                    struct mg_homebase_state* h)
{
    struct mg_homebase_dome* dome =
        mg_homebase_dome_get(&h->dome_pool, h->domes.data[h->selected_dome]);
    struct pg_type* list_info = pg_ui_variable(ctx, scroll, "list_info");
    struct pg_type* scroll_info = pg_ui_variable(ctx, scroll, "scroll_info");
    pg_ui_arr_t* item_arr = pg_ui_variable(ctx, scroll, "item_arr")->ptr[0];
    pg_ui_t item;
    int mission_idx;
    int i;
    scroll_info->f[1] = LM_MAX(0, h->active_missions.len * 0.25 - 0.75);
    list_info->i[2] = h->active_missions.len;
    int num_assigned = 0;
    int num_items = LM_MAX(item_arr->len, h->active_missions.len);
    for(i = 0; i < num_items; ++i) {
        if(i >= h->active_missions.len) {
            pg_ui_enabled(ctx, item_arr->data[i], 0);
            continue;
        } else if(i >= item_arr->len) {
            item = msn_scroll_make_item(ctx, scroll, i);
        } else {
            item = item_arr->data[i];
        }
        pg_ui_enabled(ctx, item, 1);
        struct pg_type* item_info = pg_ui_variable(ctx, item, "item_info");
        mg_homebase_bulletin_id_t blt_id = h->active_missions.data[i];
        wchar_t item_text[128];
        struct mg_homebase_bulletin* blt =
            mg_homebase_bulletin_get(&h->blt_pool, blt_id);
        const struct mg_homebase_bulletin_info* blt_info =
            mg_homebase_bulletin_info(blt->info);
        mg_homebase_person_id_t ppl_id = mg_homebase_bulletin_get_assignee(h, blt_id);
        printf("Assignee %d\n", ppl_id);
        if(ppl_id != -1) {
            ++num_assigned;
            struct mg_homebase_person* person =
                mg_homebase_person_get(&h->ppl_pool, ppl_id);
            swprintf(item_text, 128, L"%ls\n   %%F2%%S(1.5)%ls", blt_info->display_name, person->name);
        } else {
            swprintf(item_text, 128, L"%ls\n   %%F2%%S(1.5)%%C(0.5,0.5,0.5,1)unassigned", blt_info->display_name);
        }
        pg_ui_set_text(ctx, item, item_text, 128);
        item_info->i[2] = blt_id;
        item_info->i[3] = ppl_id;
    }
    /*  Update the deploy button if any missions are assigned   */
    pg_ui_t deploy_btn = pg_ui_child_path(ctx, h->ui,
        "dome_base.dome_menu.active_missions.deploy_btn");
    struct pg_type* deploy_info = pg_ui_variable(ctx, deploy_btn, "deploy_info");
    vec4* deploy_text_color = pg_ui_get_property(ctx, deploy_btn, PG_UI_TEXT_COLOR);
    if(num_assigned > 0) {
        deploy_info->i[0] = 1;
        *deploy_text_color = vec4(1,1,1,1);
    } else {
        deploy_info->i[0] = 0;
        *deploy_text_color = vec4(0.5,0.5,0.5,1);
    }
}

static int ppl_click(struct pg_ui_context* ctx, pg_ui_t elem, struct pg_ui_event* event)
{
    struct marsgame* mg = pg_ui_variable(ctx, ctx->root, "marsgame")->ptr[0];
    struct mg_homebase_state* h = &mg->homebase;
    struct mg_homebase_dome* dome =
        mg_homebase_dome_get(&h->dome_pool, h->domes.data[h->selected_dome]);
    struct pg_type* item_info = pg_ui_variable(ctx, elem, "item_info");
    pg_ui_t list = item_info->i[0];
    int idx = item_info->i[1];
    h->selected_person = idx;
    /*  Update list */
    ppl_scroll_update_items(ctx, list, h);
    return 1;
}

static pg_ui_t ppl_scroll_make_item(struct pg_ui_context* ctx, pg_ui_t scroll, int idx)
{
    char item_name[32];
    snprintf(item_name, 32, "scroll_item_%d", idx);
    pg_ui_t item = UI_ELEM(scroll, item_name,
        .pos = vec2(-0.45, idx*-0.1 - 0.2), .draw = PG_UI_TEXT_ONLY,
        .action_area = vec4(0,0.025,1,0.05),
        .text_pos = vec2(0,0), .text_size = vec2(0.05,0.05),
        .cb_click = ppl_click );
    *pg_ui_variable(ctx, item, "item_info") = PG_TYPE_INT(scroll, idx, -1, 0);
    pg_ui_arr_t* item_arr = pg_ui_variable(ctx, scroll, "item_arr")->ptr[0];
    ARR_PUSH(*item_arr, item);
    return item;
}

static void ppl_scroll_update_items(struct pg_ui_context* ctx, pg_ui_t scroll,
                                    struct mg_homebase_state* h)
{
    struct mg_homebase_dome* dome =
        mg_homebase_dome_get(&h->dome_pool, h->domes.data[h->selected_dome]);
    struct pg_type* list_info = pg_ui_variable(ctx, scroll, "list_info");
    struct pg_type* scroll_info = pg_ui_variable(ctx, scroll, "scroll_info");
    pg_ui_arr_t* item_arr = pg_ui_variable(ctx, scroll, "item_arr")->ptr[0];
    pg_ui_t item;
    int i;
    scroll_info->f[1] = LM_MAX(0, h->personnel.len * 0.1 - 0.75);
    list_info->i[2] = h->personnel.len;
    int num_items = LM_MAX(item_arr->len, h->personnel.len);
    for(i = 0; i < num_items; ++i) {
        if(i >= h->personnel.len) {
            pg_ui_enabled(ctx, item_arr->data[i], 0);
            continue;
        } else if(i >= item_arr->len) {
            item = ppl_scroll_make_item(ctx, scroll, i);
        } else {
            item = item_arr->data[i];
        }
        pg_ui_enabled(ctx, item, 1);
        struct pg_type* item_info = pg_ui_variable(ctx, item, "item_info");
        mg_homebase_person_id_t ppl_id = h->personnel.data[i];
        struct mg_homebase_person* person =
            mg_homebase_person_get(&h->ppl_pool, ppl_id);
        vec4* text_color = pg_ui_get_property(ctx, item, PG_UI_TEXT_COLOR);
        if(h->selected_person == i) {
            *text_color = vec4(1, 1, 0.5, 1);
        } else if(person->assigned_mission != -1) {
            *text_color = vec4(0.75, 0.75, 1, 1);
        } else {
            *text_color = vec4(1, 1, 1, 1);
        }
        pg_ui_set_text(ctx, item, person->name, 32);
        item_info->i[2] = ppl_id;
    }
}

static int list_scroll(struct pg_ui_context* ctx, pg_ui_t scroll_background,
                       struct pg_ui_event* event)
{
    pg_ui_t scroll = pg_ui_variable(ctx, scroll_background, "scroll")->i[0];
    struct pg_type* scroll_info = pg_ui_variable(ctx, scroll, "scroll_info");
    vec4* scroll_pos = pg_ui_get_property(ctx, scroll, PG_UI_POS);
    scroll_pos->y -= event->mouse.scroll_direction * 0.075;
    scroll_pos->y = LM_CLAMP(scroll_pos->y, 0, scroll_info->f[1]);
    scroll_info->f[0] = scroll_pos->y;
    return 1;
}

pg_ui_t msn_make_list(struct pg_ui_context* ctx, pg_ui_t msn_ui, struct mg_homebase_state* h)
{
    pg_ui_t msn_scroll = UI_GROUP(msn_ui, "msn_scroll",
        .pos = vec2(-0.6, 0),
        .enable_clip = 1, .clip = vec4(0,-0.5, 1.1,0.8) );
    pg_ui_t list_background = UI_ELEM(msn_scroll, "background", .layer = -1,
        .draw = PG_UI_IMG_ONLY, .action_area = vec4(0,0,1,1),
        .pos = vec2(0, -0.5),
        .action_item = PG_UI_ACTION_IMG,
        .img_scale = vec2(1.1,0.8),
        .img_color_mod = vec4(0), .img_color_add = vec4(0,0,0,0.25),
        .cb_scroll = list_scroll);
    pg_ui_t scroll = UI_GROUP(msn_scroll, "scroll");
    pg_ui_arr_t* scroll_items = malloc(sizeof(*scroll_items));
    ARR_INIT(*scroll_items);
    pg_ui_variable(ctx, scroll, "list_info")->i[1] = -1;
    pg_ui_variable(ctx, scroll, "item_arr")->ptr[0] = scroll_items;
    pg_ui_variable(ctx, list_background, "scroll")->i[0] = scroll;
    return msn_scroll;
}

pg_ui_t ppl_make_list(struct pg_ui_context* ctx, pg_ui_t msn_ui, struct mg_homebase_state* h)
{
    pg_ui_t ppl_scroll = UI_GROUP(msn_ui, "ppl_scroll",
        .pos = vec2(0.6, 0),
        .enable_clip = 1, .clip = vec4(0,-0.5, 1.1,0.8) );
    pg_ui_t list_background = UI_ELEM(ppl_scroll, "background", .layer = -1,
        .draw = PG_UI_IMG_ONLY, .action_area = vec4(0,0,1,1),
        .pos = vec2(0, -0.5),
        .action_item = PG_UI_ACTION_IMG,
        .img_scale = vec2(1.1,0.8),
        .img_color_mod = vec4(0), .img_color_add = vec4(0,0,0,0.25),
        .cb_scroll = list_scroll);
    pg_ui_t scroll = UI_GROUP(ppl_scroll, "scroll");
    pg_ui_arr_t* scroll_items = malloc(sizeof(*scroll_items));
    ARR_INIT(*scroll_items);
    pg_ui_variable(ctx, scroll, "list_info")->i[1] = -1;
    pg_ui_variable(ctx, scroll, "item_arr")->ptr[0] = scroll_items;
    pg_ui_variable(ctx, list_background, "scroll")->i[0] = scroll;
    return ppl_scroll;
}

static int deploy_btn_click(struct pg_ui_context* ctx, pg_ui_t deploy_btn,
                            struct pg_ui_event* event)
{
    int can_deploy = pg_ui_variable(ctx, deploy_btn, "deploy_info")->i[0];
    printf("CAN DEPLOY???? %d\n", can_deploy);
    if(!can_deploy) return 1;
    struct marsgame* mg = pg_ui_variable(ctx, ctx->root, "marsgame")->ptr[0];
    struct mg_homebase_state* h = &mg->homebase;
    h->should_deploy = 1;
    return 1;
}

pg_ui_t ui_active_missions_create(struct pg_ui_context* ctx, struct mg_homebase_state* h)
{
    pg_ui_t dome_base = pg_ui_child(ctx, h->ui, "dome_base");
    pg_ui_t dome_menu = pg_ui_child(ctx, dome_base, "dome_menu");
    pg_ui_t active_missions = UI_GROUP(dome_menu, "active_missions", .enabled = 0);
    pg_ui_t msn_list = msn_make_list(ctx, active_missions, h);
    pg_ui_t ppl_list = ppl_make_list(ctx, active_missions, h);
    pg_ui_t msn_scroll = pg_ui_child(ctx, msn_list, "scroll");
    pg_ui_t ppl_scroll = pg_ui_child(ctx, ppl_list, "scroll");
    pg_ui_t deploy_btn = UI_ELEM(active_missions, "deploy_btn",
        .pos = vec2(-0.6, -1.1), .draw = PG_UI_IMG_THEN_TEXT,
        .action_item = PG_UI_ACTION_IMG,
        .img_scale = vec2(1, 0.15),
        .img_color_mod = vec4(0), .img_color_add = vec4(0,0,0,0.25),
        .text_formatter = &PG_TEXT_FORMATTER_MOD(ctx->default_text_formatter,
            .default_font = 1, .align = PG_TEXT_CENTER, .size = vec2(0.1,0.1)),
        .text = L"DEPLOY!", .text_pos = vec2(0, -0.04),
        .text_color = vec4(1,1,1,1),
        .cb_click = deploy_btn_click );
    pg_ui_variable(ctx, msn_scroll, "ppl_scroll")->i[0] = ppl_scroll;
    pg_ui_variable(ctx, ppl_scroll, "msn_scroll")->i[0] = msn_scroll;
    return active_missions;
}

static void active_missions_update(struct pg_ui_context* ctx, struct mg_homebase_state* h)
{
    pg_ui_t active_missions = pg_ui_child_path(ctx, h->ui, "dome_base.dome_menu.active_missions");
    pg_ui_t msn_scroll = pg_ui_child_path(ctx, active_missions, "msn_scroll.scroll");
    pg_ui_arr_t* item_arr = pg_ui_variable(ctx, msn_scroll, "item_arr")->ptr[0];
}

void ui_active_missions(struct pg_ui_context* ctx, struct mg_homebase_state* h, int dome_idx)
{
    pg_ui_t dome_base = pg_ui_child(ctx, h->ui, "dome_base");
    pg_ui_t dome_menu = pg_ui_child(ctx, dome_base, "dome_menu");
    pg_ui_t dome_menu_base = pg_ui_child(ctx, dome_menu, "base");
    pg_ui_t active_missions = pg_ui_child(ctx, dome_menu, "active_missions");
    int did_open = click_dome_submenu(ctx, dome_menu, active_missions);
    if(!did_open) return;
    pg_ui_t msn_scroll = pg_ui_child_path(ctx, active_missions, "msn_scroll.scroll");
    pg_ui_t ppl_scroll = pg_ui_child_path(ctx, active_missions, "ppl_scroll.scroll");
    msn_scroll_update_items(ctx, msn_scroll, h);
    ppl_scroll_update_items(ctx, ppl_scroll, h);
    pg_ui_set_text(ctx, dome_menu_base, L"MISSIONS", 32);
}

#include "procgl/procgl.h"
#include "../marsgame.h"

static void scroll_update_items(struct pg_ui_context* ctx, pg_ui_t scroll,
                                struct mg_homebase_state* h);

static void build_dome_view_option(struct pg_ui_context* ctx,
                                     struct mg_homebase_state* h,
                                     mg_homebase_dome_info_t dome_type)
{
    pg_ui_t build_dome = pg_ui_child_path(ctx, h->ui, "dome_base.dome_menu.build_dome");
    pg_ui_t preview = pg_ui_child(ctx, build_dome, "preview");
    pg_ui_t confirm_btn = pg_ui_child(ctx, preview, "confirm_btn");
    pg_ui_enabled(ctx, preview, 1);
    const struct mg_homebase_dome_info* dome_info = mg_homebase_dome_info(dome_type);
    struct pg_type* btn_info = pg_ui_variable(ctx, confirm_btn, "confirm_btn");
    if(mg_homebase_have_dome_type(h, dome_type)) {
        pg_ui_set_text(ctx, confirm_btn, L"ALREADY\nBUILT", 32);
        btn_info->i[0] = -1;
    } else if(dome_info->materials_cost <= h->materials) {
        pg_ui_set_text(ctx, confirm_btn, L"BUILD", 32);
        btn_info->i[0] = dome_type;
    } else {
        pg_ui_set_text(ctx, confirm_btn, L"NEED MORE\nMATERIALS", 32);
        btn_info->i[0] = -1;
    }
}

static int build_dome_click_confirm(struct pg_ui_context* ctx, pg_ui_t confirm_btn,
                                      struct pg_ui_event* event)
{
    struct marsgame* mg = pg_ui_variable(ctx, ctx->root, "marsgame")->ptr[0];
    struct mg_homebase_state* h = &mg->homebase;
    pg_ui_t dome_menu = pg_ui_child_path(ctx, h->ui, "dome_base.dome_menu");
    printf("build_dome: %d\n", dome_menu);
    struct pg_type* btn_info = pg_ui_variable(ctx, confirm_btn, "confirm_btn");
    if(btn_info->i[0] != -1) {
        struct mg_homebase_dome* dome =
            mg_homebase_dome_get(&h->dome_pool, h->domes.data[h->selected_dome]);
        const struct mg_homebase_dome_info* dome_info =
            mg_homebase_dome_info(btn_info->i[0]);
        h->materials -= dome_info->materials_cost;
        mg_homebase_dome_init(dome, btn_info->i[0]);
        mg_homebase_update_domes(h);
        exit_dome_submenu(ctx, dome_menu);
        dome_ui_menu(ctx, h, h->selected_dome);
    }
    return 1;
}

/*  List item:
        item_info:  i
            0   scroll list reference
            1   index in list
            2   dome type
            3   is currently selected
    Scroll list:
        list_info: i
            0   selection index
            1   selected dome type
        item_arr: ptr
            0   pg_ui_arr_t (all list items)
*/

static int list_click(struct pg_ui_context* ctx, pg_ui_t elem, struct pg_ui_event* event)
{
    struct marsgame* mg = pg_ui_variable(ctx, ctx->root, "marsgame")->ptr[0];
    struct mg_homebase_state* h = &mg->homebase;
    struct mg_homebase_dome* dome =
        mg_homebase_dome_get(&h->dome_pool, h->domes.data[h->selected_dome]);
    struct pg_type* item_info = pg_ui_variable(ctx, elem, "item_info");
    pg_ui_t list = item_info->i[0];
    int idx = item_info->i[1];
    mg_homebase_dome_info_t dome_type = item_info->i[2];
    struct pg_type* list_info = pg_ui_variable(ctx, list, "list_info");
    /*  Set list's "selected dome" to item's dome index   */
    list_info->i[0] = item_info->i[1];
    list_info->i[1] = dome_type;
    /*  Update list */
    printf("Click\n");
    build_dome_view_option(ctx, h, dome_type);
    scroll_update_items(ctx, list, h);
    return 1;
}

static void scroll_update_items(struct pg_ui_context* ctx, pg_ui_t scroll,
                                struct mg_homebase_state* h)
{
    struct pg_type* list_info = pg_ui_variable(ctx, scroll, "list_info");
    pg_ui_arr_t* item_arr = pg_ui_variable(ctx, scroll, "item_arr")->ptr[0];
    pg_ui_t item;
    int i;
    ARR_FOREACH(*item_arr, item, i) {
        struct pg_type* item_info = pg_ui_variable(ctx, item, "item_info");
        mg_homebase_dome_info_t dome_type = item_info->i[2];
        const struct mg_homebase_dome_info* dome_info = mg_homebase_dome_info(dome_type);
        vec4* text_color = pg_ui_get_property(ctx, item, PG_UI_TEXT_COLOR);
        if(mg_homebase_have_dome_type(h, dome_type)) {
            *text_color = vec4(1, 1, 0.5, 1);
        } else if(dome_info->materials_cost > h->materials) {
            *text_color = vec4(1, 0.75, 0.75, 1);
        } else {
            *text_color = vec4(1, 1, 1, 1);
        }
        if(list_info->i[1] == -1) {
            item_info->i[3] = 0;
            pg_ui_cancel_anim(ctx, item, PG_UI_POS);
        } else if(item_info->i[2] != list_info->i[1] && item_info->i[3]) {
            item_info->i[3] = 0;
            pg_ui_simple_anim(ctx, item, PG_UI_POS,
                &PG_SIMPLE_ANIM(.duration = 0.25, .start = vec4(0.025,0) ));
        } else if(item_info->i[2] == list_info->i[1] && !item_info->i[3]) {
            item_info->i[3] = 1;
            pg_ui_simple_anim(ctx, item, PG_UI_POS,
                &PG_SIMPLE_ANIM(.duration = 0.25, .end = vec4(0.025,0) ));
        }
    }
}

static int list_scroll(struct pg_ui_context* ctx, pg_ui_t scroll_background,
                       struct pg_ui_event* event)
{
    pg_ui_t scroll = pg_ui_variable(ctx, scroll_background, "scroll")->i[0];
    struct pg_type* scroll_info = pg_ui_variable(ctx, scroll, "scroll_info");
    vec4* scroll_pos = pg_ui_get_property(ctx, scroll, PG_UI_POS);
    scroll_pos->y -= event->mouse.scroll_direction * 0.075;
    scroll_pos->y = LM_CLAMP(scroll_pos->y, 0, scroll_info->f[1]);
    scroll_info->f[0] = scroll_pos->y;
    return 1;
}

static void make_list(struct pg_ui_context* ctx, pg_ui_t build_dome,
                      struct mg_homebase_state* h)
{
    /*  Build the UI    */
    float ar = 1 / pg_ui_get_aspect(ctx);
    pg_ui_t list = UI_GROUP(build_dome, "dome_list",
        .pos = vec2(-0.6, 0),
        .enable_clip = 1, .clip = vec4(0,-0.5, 1.1,0.8) );
    pg_ui_t list_background = UI_ELEM(list, "background", .layer = -1,
        .draw = PG_UI_IMG_ONLY, .action_area = vec4(0,0,1,1),
        .pos = vec2(0,-0.5),
        .action_item = PG_UI_ACTION_IMG,
        .img_scale = vec2(1.1,0.8),
        .img_color_mod = vec4(0), .img_color_add = vec4(0,0,0,0.25),
        .cb_scroll = list_scroll);
    pg_ui_t scroll = UI_GROUP(list, "scroll", .pos = vec2(0, 0));
    pg_ui_arr_t* item_arr = malloc(sizeof(*item_arr));
    ARR_INIT(*item_arr);
    pg_ui_variable(ctx, scroll, "list_info")->i[1] = -1;
    pg_ui_variable(ctx, scroll, "item_arr")->ptr[0] = item_arr;
    pg_ui_variable(ctx, list_background, "scroll")->i[0] = scroll;
    /*  Fill in the list options    */
    char item_name[32];
    int num_dome_types = mg_num_dome_types();
    int num_dome_options = 0;
    mg_homebase_dome_info_t dome_type;
    int list_idx = 0;
    int i;
    for(i = 0; i < num_dome_types; ++i) {
        const struct mg_homebase_dome_info* dome_info =
            mg_homebase_dome_info(i);
        if(!dome_info->can_build) continue;
        ++num_dome_options;
        printf("Adding dome\n");
        snprintf(item_name, 32, "scroll_item_%d", list_idx);
        pg_ui_t item = UI_ELEM(scroll, item_name,
            .pos = vec2(-0.45, list_idx*-0.1-0.2), .draw = PG_UI_TEXT_ONLY,
            .action_area = vec4(0,0.025,1,0.05),
            .text_size = vec2(0.05,0.05),
            .text = dome_info->display_name,
            .cb_click = list_click );
        *pg_ui_variable(ctx, item, "item_info") = PG_TYPE_INT(scroll, list_idx, i, 0);
        ARR_PUSH(*item_arr, item);
        ++list_idx;
    }
    pg_ui_variable(ctx, scroll, "scroll_info")->f[1] =
        LM_MAX(0, num_dome_options * 0.1 - 0.75);
}

pg_ui_t ui_build_dome_create(struct pg_ui_context* ctx, struct mg_homebase_state* h)
{
    float ar = 1 / pg_ui_get_aspect(ctx);
    pg_ui_t dome_base = pg_ui_child(ctx, h->ui, "dome_base");
    pg_ui_t dome_menu = pg_ui_child(ctx, dome_base, "dome_menu");
    pg_ui_t build_dome = UI_GROUP(dome_menu, "build_dome", .enabled = 0);
    pg_ui_t mats_counter = UI_ELEM(build_dome, "mats_counter",
        .pos = vec2(0.5,0), .draw = PG_UI_TEXT_ONLY,
        .text_size = vec2(0.05,0.05) );
    make_list(ctx, build_dome, h);
    pg_ui_t preview = UI_GROUP(build_dome, "preview", .enabled = 0,
        .pos = vec2(0.6, 0) );
    pg_ui_t confirm_btn = UI_ELEM(preview, "confirm_btn",
        .pos = vec2(0, -0.8), .draw = PG_UI_TEXT_ONLY,
        //.action_area = vec4(0,0,0.25,0.25),
        .action_item = PG_UI_ACTION_TEXT,
        .text_formatter = &PG_TEXT_FORMATTER_MOD(ctx->default_text_formatter,
            .align = PG_TEXT_LEFT, .default_font = 1, .size = vec2(0.075,0.075) ),
        .cb_click = build_dome_click_confirm );
    return build_dome;
}

void ui_build_dome(struct pg_ui_context* ctx, struct mg_homebase_state* h, int dome_idx)
{
    pg_ui_t dome_base = pg_ui_child(ctx, h->ui, "dome_base");
    pg_ui_t dome_menu = pg_ui_child(ctx, dome_base, "dome_menu");
    pg_ui_t dome_menu_base = pg_ui_child(ctx, dome_menu, "base");
    pg_ui_t build_dome = pg_ui_child(ctx, dome_menu, "build_dome");
    pg_ui_t mats_counter = pg_ui_child(ctx, build_dome, "mats_counter");
    pg_ui_t preview = pg_ui_child(ctx, build_dome, "preview");
    pg_ui_enabled(ctx, preview, 0);
    int did_open = click_dome_submenu(ctx, dome_menu, build_dome);
    if(!did_open) return;
    mg_homebase_update_domes(h);
    struct mg_homebase_dome* dome =
        mg_homebase_dome_get(&h->dome_pool, h->domes.data[h->selected_dome]);
    wchar_t mats_str[32];
    swprintf(mats_str, 32, L"Materials\t%d", h->materials);
    pg_ui_set_text(ctx, mats_counter, mats_str, 32);
    pg_ui_t scroll = pg_ui_child_path(ctx, build_dome, "dome_list.scroll");
    struct pg_type* list_info = pg_ui_variable(ctx, scroll, "list_info");
    list_info->i[1] = -1;
    scroll_update_items(ctx, scroll, h);
    pg_ui_set_text(ctx, dome_menu_base, L"BUILD DOME", 32);
}

struct ui_scroll_list_opts {
    struct pg_ui_properties* list_props;
    pg_ui_t (*make_elem)(struct pg_ui_context* ctx,
                                   pg_ui_t list, int index);
    void (*update_elem)(struct pg_ui_context* ctx, pg_ui_t item,
                                     int index, void* udata);
    void* udata;
    int num_elems;
};

#define UI_SCROLL_LIST(props, ...) &(struct ui_scroll_list_opts){ \
    .list_props = (props), __VA_ARGS__ }

pg_ui_t ui_make_scroll_list(struct pg_ui_context* ctx,
                                      pg_ui_t parent, char* name,
                                      struct ui_scroll_list_opts* opts);
void ui_scroll_list_update(struct pg_ui_context* ctx, pg_ui_t list);
void ui_scroll_list_set_max(struct pg_ui_context* ctx, pg_ui_t list, int max);
void ui_scroll_list_move(struct pg_ui_context* ctx, pg_ui_t list, int move);
void ui_scroll_list_nav_btn(struct pg_ui_context* ctx, pg_ui_t btn);

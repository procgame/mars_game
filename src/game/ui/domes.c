#include "procgl/procgl.h"
#include "../marsgame.h"

void dome_ui_menu(struct pg_ui_context* ctx, struct mg_homebase_state* h, int dome_idx);

/************************/
/*  DOME SELECTION UI   */
/************************/

static int dome_selector_click(struct pg_ui_context* ctx, pg_ui_t elem, struct pg_ui_event* event)
{
    struct marsgame* mg = pg_ui_variable(ctx, ctx->root, "marsgame")->ptr[0];
    struct mg_homebase_state* h = &mg->homebase;
    int selector = pg_ui_variable(ctx, elem, "dome_idx")->i[0];
    h->selected_dome = selector;
    pg_ui_t dome_selectors = pg_ui_child(ctx, h->ui, "dome_selectors");
    pg_ui_enabled(ctx, dome_selectors, 0);
    pg_ui_t dome_grp = pg_ui_child(ctx, h->ui, "dome_base");
    pg_ui_enabled(ctx, dome_grp, 1);
    dome_ui_menu(ctx, h, selector);
    /*  Animate camera  */
    struct mg_homebase_dome* dome = mg_homebase_dome_get(&h->dome_pool, h->domes.data[selector]);
    h->cam_lookat_anim = PG_SIMPLE_ANIM(
        .start = vec4(0,0,0,25), .end = vec4(VEC_XY(dome->pos), 0, dome->size * 1.25),
        .start_time = (float)h->tick / 60, .duration = 1,
        .ease = pg_ease_inout_sine );
    float angle = dome->angle - LM_PI;
    h->cam_angle_anim = PG_SIMPLE_ANIM(
        .start = vec4(0, LM_PI*0.25), .end = vec4(angle, LM_PI*0.1),
        .start_time = (float)h->tick / 60, .duration = 1,
        .ease = pg_ease_inout_sine );
    return 1;
}

/*
pg_ui_t ui_dome_selector_new(struct pg_ui_context* ctx, pg_ui_t dome_selectors)
{
    pg_ui_arr_t* dome_select_arr = pg_ui_variable(ctx, dome_selectors, "selector_arr")->ptr[0];
    int new_idx = dome_select_arr->len;
    char name[32];
    snprintf(name, 32, "dome_select_%d", new_idx);
    pg_ui_t new_selector = UI_ELEM(dome_selectors, name, .enabled = 0,
        .draw = PG_UI_IMG_ONLY, .img_scale = vec2(0.1,0.1),
        .img_color_mod = vec4(0), .img_color_add = vec4(1,1,1,0.25),
        .action_area = vec4(-0.1,-0.1,0.1,0.1),
        .cb_click = dome_selector_click);
    pg_ui_variable(ctx, new_selector, "dome_idx")->i[0] = new_idx;
    ARR_PUSH(*dome_select_arr, new_selector);
}*/

pg_ui_t ui_dome_selection_create(struct pg_ui_context* ctx, struct mg_homebase_state* h)
{
    pg_ui_t dome_selectors = UI_GROUP(h->ui, "dome_selectors");
    pg_ui_arr_t* dome_select_arr = malloc(sizeof(*dome_select_arr));
    pg_ui_variable(ctx, dome_selectors, "selector_arr")->ptr[0] = dome_select_arr;
    printf("\n\n\n\nDOME LEN: %d\n", h->domes.len);
    int i;
    for(i = 0; i < h->domes.len; ++i) {
        struct mg_homebase_dome* dome = mg_homebase_dome_get(&h->dome_pool, h->domes.data[i]);
        vec2 pos2 = dome->pos;
        vec3 pos = vec3(VEC_XY(pos2));
        pos2 = pg_viewer_project(&h->worldview, pos);
        char ui_name[32];
        snprintf(ui_name, 32, "dome_select_%d", i);
        pg_ui_t dome_select = UI_ELEM(dome_selectors, ui_name,
            .pos = pos2, .draw = PG_UI_IMG_ONLY, .img_scale = vec2(0.1,0.1),
            .img_color_mod = vec4(0), .img_color_add = vec4(1,1,1,0.25),
            .action_area = vec4(0,0,0.1,0.1),
            .cb_click = dome_selector_click);
        pg_ui_variable(ctx, dome_select, "dome_idx")->i[0] = i;
    }
    return dome_selectors;
}

/********************/
/*  IN-DOME MENUS   */
/********************/

void exit_dome_submenu(struct pg_ui_context* ctx, pg_ui_t dome_menu)
{
    struct pg_type* menu_info = pg_ui_variable(ctx, dome_menu, "info");
    pg_ui_t active_menu = menu_info->i[0];
    pg_ui_enabled(ctx, active_menu, 0);
    pg_ui_enabled(ctx, dome_menu, 0);
    menu_info->i[0] = -1;
}

int click_dome_submenu(struct pg_ui_context* ctx, pg_ui_t dome_menu,
                       pg_ui_t submenu)
{
    struct pg_type* info = pg_ui_variable(ctx, dome_menu, "info");
    if(info->i[0] == submenu) {
        exit_dome_submenu(ctx, dome_menu);
        info->i[0] = -1;
        return 0;
    } else if(info->i[0] != -1) {
        exit_dome_submenu(ctx, dome_menu);
    }
    info->i[0] = submenu;
    pg_ui_enabled(ctx, dome_menu, 1);
    pg_ui_enabled(ctx, submenu, 1);
    return 1;
}

/****************************/
/*  DOME FUNCTION BUTTONS   */
/****************************/

static int dome_button_back_click(struct pg_ui_context* ctx, pg_ui_t elem, struct pg_ui_event* event)
{
    struct marsgame* mg = pg_ui_variable(ctx, ctx->root, "marsgame")->ptr[0];
    struct mg_homebase_state* h = &mg->homebase;
    struct mg_homebase_dome* dome =
        mg_homebase_dome_get(&h->dome_pool, h->domes.data[h->selected_dome]);
    h->selected_dome = -1;
    pg_ui_t dome_selectors = pg_ui_child(ctx, h->ui, "dome_selectors");
    pg_ui_enabled(ctx, dome_selectors, 1);
    pg_ui_t dome_grp = pg_ui_child(ctx, h->ui, "dome_base");
    pg_ui_enabled(ctx, dome_grp, 0);
    pg_ui_t dome_menu = pg_ui_child(ctx, dome_grp, "dome_menu");
    exit_dome_submenu(ctx, dome_menu);
    /*  Animate camera  */
    h->cam_lookat_anim = PG_SIMPLE_ANIM(
        .end = vec4(0,0,0,25), .start = vec4(VEC_XY(dome->pos), 0, dome->size * 0.75),
        .start_time = (float)h->tick / 60, .duration = 1,
        .ease = pg_ease_inout_sine );
    float angle = dome->angle - LM_PI;
    h->cam_angle_anim = PG_SIMPLE_ANIM(
        .end = vec4(0, LM_PI*0.25), .start = vec4(angle, LM_PI*0.1),
        .start_time = (float)h->tick / 60, .duration = 1,
        .ease = pg_ease_inout_sine );
    return 1;
}

static int dome_button_click(struct pg_ui_context* ctx, pg_ui_t elem, struct pg_ui_event* event)
{
    struct marsgame* mg = pg_ui_variable(ctx, ctx->root, "marsgame")->ptr[0];
    struct mg_homebase_state* h = &mg->homebase;
    struct mg_homebase_dome* dome =
        mg_homebase_dome_get(&h->dome_pool, h->domes.data[h->selected_dome]);
    int button_idx = pg_ui_variable(ctx, elem, "button")->i[0];
    int func_idx = button_idx - 1;
    const struct mg_homebase_dome_info* dome_info = mg_homebase_dome_info(dome->type);
    if(dome_info->func_ui[func_idx])
        dome_info->func_ui[func_idx](ctx, h, h->selected_dome);
    return 1;
}

/********************/
/*  DOME UI         */
/********************/

pg_ui_t ui_domes_create(struct pg_ui_context* ctx, struct mg_homebase_state* h)
{
    pg_ui_t dome_base = UI_GROUP(h->ui, "dome_base",
        .enabled = 0, .fix_aspect = 1,
        .pos = vec2(0, 0), .scale = vec2(1, 1) );
    pg_ui_t dome_buttons = UI_GROUP(dome_base, "buttons",
        .pos = vec2(0, -0.75), .scale = vec2(1, 1) );
    pg_ui_t dome_bkg = UI_ELEM(dome_buttons, "background",
        .pos = vec2(0, 0), .layer = -1,
        .draw = PG_UI_IMG_ONLY, .img_scale = vec2(2.5, 0.4),
        .img_color_mod = vec4(0), .img_color_add = vec4(0,0,0,0.25) );
    pg_ui_t dome_header = UI_ELEM(dome_buttons, "header",
        .pos = vec2(-1, 0.2), .draw = PG_UI_TEXT_ONLY,
        .text_formatter = &PG_TEXT_FORMATTER_MOD(ctx->default_text_formatter,
            .default_font = 1, .size = vec2(0.1,0.1) ),
        .text = L"TEST DOME", );
    pg_ui_t dome_func[4] = {
        UI_GROUP(dome_buttons, "dome_func_0", .pos = vec2(-0.9, 0)),
        UI_GROUP(dome_buttons, "dome_func_1", .pos = vec2(-0.3, 0)),
        UI_GROUP(dome_buttons, "dome_func_2", .pos = vec2(0.3, 0)),
        UI_GROUP(dome_buttons, "dome_func_3", .pos = vec2(0.9, 0)),
    };
    pg_ui_t dome_func_base[4] = {
        UI_ELEM(dome_func[0], "base",
            .pos = vec2(0,0), .draw = PG_UI_IMG_THEN_TEXT,
            .img_pos = vec2(0, 0),
            .img_scale = vec2(0.5,0.3),
            .img_color_mod = vec4(0), .img_color_add = vec4(1,1,1,0.25),
            .text = L"BACK", .text_size = vec2(0.05,0.05),
            .text_pos = vec2(-0.24, 0.09),
            .action_item = PG_UI_ACTION_IMG,
            .cb_click = dome_button_back_click ),
        UI_ELEM(dome_func[1], "base",
            .pos = vec2(0,0), .draw = PG_UI_IMG_THEN_TEXT,
            .img_scale = vec2(0.5,0.3),
            .img_color_mod = vec4(0), .img_color_add = vec4(1,1,1,0.25),
            .text = L"FUNC 1", .text_size = vec2(0.05,0.05),
            .text_pos = vec2(-0.24, 0.09),
            .action_item = PG_UI_ACTION_IMG,
            .cb_click = dome_button_click ),
        UI_ELEM(dome_func[2], "base",
            .pos = vec2(0,0), .draw = PG_UI_IMG_THEN_TEXT,
            .img_scale = vec2(0.5,0.3),
            .img_color_mod = vec4(0), .img_color_add = vec4(1,1,1,0.25),
            .text = L"FUNC 2", .text_size = vec2(0.05,0.05),
            .text_pos = vec2(-0.24, 0.09),
            .action_item = PG_UI_ACTION_IMG,
            .cb_click = dome_button_click ),
        UI_ELEM(dome_func[3], "base",
            .pos = vec2(0,0), .draw = PG_UI_IMG_THEN_TEXT,
            .img_scale = vec2(0.5,0.3),
            .img_color_mod = vec4(0), .img_color_add = vec4(1,1,1,0.25),
            .text = L"FUNC 3", .text_size = vec2(0.05,0.05),
            .text_pos = vec2(-0.24, 0.09),
            .action_item = PG_UI_ACTION_IMG,
            .cb_click = dome_button_click ),
    };
    *pg_ui_variable(ctx, dome_func_base[0], "button") = PG_TYPE_INT(0, -1);
    *pg_ui_variable(ctx, dome_func_base[1], "button") = PG_TYPE_INT(1, -1);
    *pg_ui_variable(ctx, dome_func_base[2], "button") = PG_TYPE_INT(2, -1);
    *pg_ui_variable(ctx, dome_func_base[3], "button") = PG_TYPE_INT(3, -1);
    pg_ui_t dome_menu = UI_GROUP(dome_base, "dome_menu", .enabled = 0, .pos = vec2(0,0.8) );
    pg_ui_variable(ctx, dome_menu, "menu_active")->i[0] = -1;
    pg_ui_t menu_bkg = UI_ELEM(dome_menu, "base",
        .pos = vec2(0, -0.5), .draw = PG_UI_IMG_THEN_TEXT,
        .img_scale = vec2(2.5, 1),
        .img_color_mod = vec4(0), .img_color_add = vec4(0,0,0,0.25),
        .text_formatter = &PG_TEXT_FORMATTER_MOD(ctx->default_text_formatter,
            .default_font = 1, .size = vec2(0.1,0.1) ),
        .text = L"%F1DOME MENU",
        .text_pos = vec2(-1,0.5) );
    ui_build_dome_create(ctx, h);
    ui_upgrade_dome_create(ctx, h);
    ui_bulletin_board_create(ctx, h);
    ui_active_missions_create(ctx, h);
    ui_dome_selection_create(ctx, h);
    ui_recruiting_create(ctx, h);
    return dome_base;
}

void dome_ui_menu(struct pg_ui_context* ctx, struct mg_homebase_state* h, int dome_idx)
{
    struct mg_homebase_dome* dome = mg_homebase_dome_get(&h->dome_pool, h->domes.data[dome_idx]);
    const struct mg_homebase_dome_info* dome_info = mg_homebase_dome_info(dome->type);
    pg_ui_t dome_grp = pg_ui_child(ctx, h->ui, "dome_base");
    pg_ui_t dome_menu = pg_ui_child(ctx, dome_grp, "dome_menu");
    pg_ui_variable(ctx, dome_menu, "info")->i[0] = -1;
    pg_ui_t dome_buttons = pg_ui_child(ctx, dome_grp, "buttons");
    pg_ui_t dome_buttons_header = pg_ui_child(ctx, dome_buttons, "header");
    pg_ui_set_text(ctx, dome_buttons_header, dome_info->display_name, 32);
    pg_ui_t dome_func[4] = {
        pg_ui_child(ctx, dome_buttons, "dome_func_0"),
        pg_ui_child(ctx, dome_buttons, "dome_func_1"),
        pg_ui_child(ctx, dome_buttons, "dome_func_2"),
        pg_ui_child(ctx, dome_buttons, "dome_func_3") };
    pg_ui_t dome_func_base[4] = {
        pg_ui_child(ctx, dome_func[0], "base"),
        pg_ui_child(ctx, dome_func[1], "base"),
        pg_ui_child(ctx, dome_func[2], "base"),
        pg_ui_child(ctx, dome_func[3], "base") };
    pg_ui_enabled(ctx, dome_func[0], 1);
    pg_ui_set_text(ctx, dome_func_base[0], L"BACK", 5);
    if(dome_info->func_buttons[0][0] != L'\0') {
        pg_ui_enabled(ctx, dome_func[1], 1);
        pg_ui_set_text(ctx, dome_func_base[1], dome_info->func_buttons[0], 16);
    } else pg_ui_enabled(ctx, dome_func[1], 0);
    if(dome_info->func_buttons[1][0] != L'\0') {
        pg_ui_enabled(ctx, dome_func[2], 1);
        pg_ui_set_text(ctx, dome_func_base[2], dome_info->func_buttons[1], 16);
    } else pg_ui_enabled(ctx, dome_func[2], 0);
    if(dome_info->func_buttons[2][0] != L'\0') {
        pg_ui_enabled(ctx, dome_func[3], 1);
        pg_ui_set_text(ctx, dome_func_base[3], dome_info->func_buttons[2], 16);
    } else pg_ui_enabled(ctx, dome_func[3], 0);
}

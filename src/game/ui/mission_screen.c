#include "procgl/procgl.h"
#include "../marsgame.h"

static int start_mission_click(struct pg_ui_context* ctx, pg_ui_t btn,
                               struct pg_ui_event* event)
{
    printf("Start!\n");
    struct marsgame* mg = pg_ui_variable(ctx, ctx->root, "marsgame")->ptr[0];
    struct mg_missions_state* m = &mg->mission;
    if(m->missions[m->selected_mission].status == MG_MISSION_READY) {
        m->should_begin_mission = 1;
    }
    return 1;
}

static pg_ui_t mission_selector_add(struct pg_ui_context* ctx, pg_ui_t scroll, int idx)
{
    pg_ui_arr_t* item_arr = pg_ui_variable(ctx, scroll, "item_arr")->ptr[0];
    char new_name[32];
    snprintf(new_name, 32, "scroll_item_%d", idx);
    pg_ui_t new_item = UI_GROUP(scroll, new_name,
        .pos = vec2(idx * (2), 0) );
    pg_ui_t background = UI_ELEM(new_item, "base",
        .pos = vec2(0, -0.15), .draw = PG_UI_IMG_THEN_TEXT, .layer = -1,
        .img_scale = vec2(1.8, 0.25),
        .img_color_mod = vec4(0), .img_color_add = vec4(1,1,1,0.25),
        .text_size = vec2(0.05,0.05),
        .text_pos = vec2(-0.8, 0.025) );
    ARR_PUSH(*item_arr, new_item);
    return new_item;
}

static void mission_selector_set_info(struct pg_ui_context* ctx, pg_ui_t item, int mission_idx)
{
    struct marsgame* mg = pg_ui_variable(ctx, ctx->root, "marsgame")->ptr[0];
    struct mg_homebase_state* h = &mg->homebase;
    struct mg_missions_state* m = &mg->mission;
    mg_homebase_bulletin_id_t blt_id = m->missions[mission_idx].blt;
    mg_homebase_person_id_t ppl_id = m->missions[mission_idx].ppl;
    struct mg_homebase_bulletin* blt =
        mg_homebase_bulletin_get(&h->blt_pool, blt_id);
    const struct mg_homebase_bulletin_info* blt_info =
        mg_homebase_bulletin_info(blt->info);
    struct mg_homebase_person* ppl =
        mg_homebase_person_get(&h->ppl_pool, ppl_id);
    pg_ui_t item_base = pg_ui_child(ctx, item, "base");
    wchar_t item_text[128];
    swprintf(item_text, 128, L"%ls\n    %ls", blt_info->display_name, ppl->name);
    pg_ui_set_text(ctx, item_base, item_text, 128);
    vec4* bkg_color = pg_ui_get_property(ctx, item_base, PG_UI_IMG_COLOR_ADD);
    switch(m->missions[mission_idx].status) {
    case MG_MISSION_READY:
        *bkg_color = vec4(1,1,1,0.25);
        break;
    case MG_MISSION_ABORTED:
        *bkg_color = vec4(1,1,0.5,0.25);
        break;
    case MG_MISSION_FAILED:
        *bkg_color = vec4(1,0.5,0.5,0.25);
        break;
    case MG_MISSION_SUCCESS:
        *bkg_color = vec4(0.5,1,0.5,0.25);
        break;
    }
}

void mg_mission_selector_set(struct mg_missions_state* m, int mission_idx)
{
    m->selected_mission = mission_idx;
    struct pg_ui_context* ctx = m->ui_context;
    pg_ui_t play_btn = pg_ui_child_path(ctx, m->ui, "play_btn");
    pg_ui_t selector = pg_ui_child_path(ctx, m->ui, "mission_select.background");
    int mission_status = m->missions[mission_idx].status;
    switch(m->missions[mission_idx].status) {
    case MG_MISSION_READY:
        pg_ui_set_text(ctx, selector, L"MISSION:", 32);
        pg_ui_enabled(ctx, play_btn, 1);
        break;
    case MG_MISSION_ABORTED:
        pg_ui_set_text(ctx, selector, L"MISSION: %C(1,1,0.5,1)ABORTED", 64);
        pg_ui_enabled(ctx, play_btn, 0);
        break;
    case MG_MISSION_FAILED:
        pg_ui_set_text(ctx, selector, L"MISSION: %C(1,0.5,0.5,1)FAILED", 64);
        pg_ui_enabled(ctx, play_btn, 0);
        break;
    case MG_MISSION_SUCCESS:
        pg_ui_set_text(ctx, selector, L"MISSION: %C(0.5,1,0.5,1)SUCCESS", 64);
        pg_ui_enabled(ctx, play_btn, 0);
        break;
    }
}

static int nav_btn_click(struct pg_ui_context* ctx, pg_ui_t btn,
                         struct pg_ui_event* event)
{
    struct marsgame* mg = pg_ui_variable(ctx, ctx->root, "marsgame")->ptr[0];
    struct mg_missions_state* m = &mg->mission;
    struct pg_type* nav_info = pg_ui_variable(ctx, btn, "nav_info");
    pg_ui_t scroll = nav_info->i[0];
    int nav_scroll_dir = nav_info->i[1];
    pg_ui_arr_t* item_arr = pg_ui_variable(ctx, scroll, "item_arr")->ptr[0];
    struct pg_type* scroll_info = pg_ui_variable(ctx, scroll, "scroll_info");
    int scroll_idx = scroll_info->i[0];
    int new_idx = scroll_idx + nav_scroll_dir;
    if(new_idx < 0 || new_idx >= item_arr->len) return 1;
    mg_mission_selector_set(m, new_idx);
    /*  Animate scroll effect   */
    scroll_info->i[0] += nav_scroll_dir;
    float cur_scroll = scroll_idx * -2;
    float new_scroll = new_idx * -2;
    pg_ui_simple_anim(ctx, scroll, PG_UI_POS, &PG_SIMPLE_ANIM(
        .start = vec4(cur_scroll), .end = vec4(new_scroll),
        .duration = 0.25, .ease = pg_ease_out_cubic ));
    return 1;
}

/*  Mission UI
        "mission_select"
            "scroll"
                var "item_arr" contains pg_ui_arr_t with scroll items
        "nav_left"
        "nav_right"
        "play_btn"
*/

static pg_ui_t mission_selector_create(struct pg_ui_context* ctx, struct mg_missions_state* m)
{
    /*  Mission selector    */
    pg_ui_t selector = UI_GROUP(m->ui, "mission_select", .pos = vec2(0, 0.8),
        .enable_clip = 1, .clip = vec4(0,-0.15, 2,0.5) );
    pg_ui_t selector_scroll = UI_GROUP(selector, "scroll");
    pg_ui_t background = UI_ELEM(selector, "background",
        .draw = PG_UI_TEXT_THEN_IMG, .layer = -1,
        .text_formatter = &PG_TEXT_FORMATTER_MOD(ctx->default_text_formatter,
            .default_font = 1),
        .text = L"MISSION:", .text_pos = vec2(-0.8,0),
        .text_scale = vec2(0.1,0.1),
        .img_pos = vec2(0,-0.15), .img_scale = vec2(2,0.3),
        .img_color_mod = vec4(0), .img_color_add = vec4(0,0,0,0.25) );
    pg_ui_arr_t* selector_arr = malloc(sizeof(*selector_arr));
    ARR_INIT(*selector_arr);
    pg_ui_variable(ctx, selector_scroll, "item_arr")->ptr[0] = selector_arr;
    /*  Selection move buttons  */
    pg_ui_t nav_left_btn = UI_ELEM(m->ui, "nav_left",
        .draw = PG_UI_IMG_ONLY, .pos = vec2(-1.1, 0.65),
        .img_pos = vec2(0), .img_scale = vec2(0.15,0.3),
        .img_color_mod = vec4(0), .img_color_add = vec4(1,1,1,0.25),
        .action_item = PG_UI_ACTION_IMG,
        .cb_click = nav_btn_click );
    pg_ui_t nav_right_btn = UI_ELEM(m->ui, "nav_right",
        .draw = PG_UI_IMG_ONLY, .pos = vec2(1.1, 0.65),
        .img_pos = vec2(0), .img_scale = vec2(0.15,0.3),
        .img_color_mod = vec4(0), .img_color_add = vec4(1,1,1,0.25),
        .action_item = PG_UI_ACTION_IMG,
        .cb_click = nav_btn_click );
    *pg_ui_variable(ctx, nav_left_btn, "nav_info") = PG_TYPE_INT(selector_scroll, -1);
    *pg_ui_variable(ctx, nav_right_btn, "nav_info") = PG_TYPE_INT(selector_scroll, 1);
    /*  Play button */
    pg_ui_t play_btn = UI_ELEM(m->ui, "play_btn",
        .draw= PG_UI_IMG_THEN_TEXT, .pos = vec2(0, 0.425),
        .img_pos = vec2(0), .img_scale = vec2(1.5, 0.1),
        .img_color_mod = vec4(0), .img_color_add = vec4(0,0,0,0.25),
        .text_formatter = &PG_TEXT_FORMATTER_MOD(ctx->default_text_formatter,
            .align = PG_TEXT_CENTER, .default_font = 1),
        .text = L"PLAY MISSION", .text_scale = vec2(0.1,0.1),
        .text_pos = vec2(0,-0.03),
        .action_item = PG_UI_ACTION_IMG,
        .cb_click = start_mission_click );
    return selector;
}

void mg_missions_build_ui(struct pg_ui_context* ctx, struct mg_missions_state* m)
{
    pg_ui_t root = ctx->root;
    m->ui = UI_GROUP(root, "missions", .enabled = 0, .fix_aspect = 1,
        .pos = vec2(0, 0) );
    pg_ui_t selector = mission_selector_create(ctx, m);
}

void mg_mission_selector_fill(struct pg_ui_context* ctx, struct mg_missions_state* m)
{
    pg_ui_t selector = pg_ui_child(ctx, m->ui, "mission_select");
    pg_ui_t scroll = pg_ui_child(ctx, selector, "scroll");
    pg_ui_arr_t* item_arr = pg_ui_variable(ctx, scroll, "item_arr")->ptr[0];
    struct mg_homebase_state* h = &m->marsgame->homebase;
    int num_items = LM_MAX(m->num_missions, item_arr->len);
    int i;
    for(i = 0; i < num_items; ++i) {
        pg_ui_t item;
        if(i >= m->num_missions) {
            item = item_arr->data[i];
            pg_ui_enabled(ctx, item, 0);
            continue;
        } else if(i >= item_arr->len) {
            item = mission_selector_add(ctx, scroll, i);
        } else {
            item = item_arr->data[i];
        }
        mission_selector_set_info(ctx, item, i);
        pg_ui_enabled(ctx, item, 1);
    }
}


#include "procgl/procgl.h"
#include "scroll_list.h"

struct ui_scroll_list_data {
    void (*update_elem)(struct pg_ui_context* ctx, pg_ui_t item,
                        int index, void* udata);
    void* udata;
    int num_elems;
    int scroll_pos;
    int scroll_max;
    ARR_T(pg_ui_t) items;
};

pg_ui_t ui_make_scroll_list(struct pg_ui_context* ctx,
                                      pg_ui_t parent, char* name,
                                      struct ui_scroll_list_opts* opts)
{
    pg_ui_t list_grp = pg_ui_add_group(ctx, parent, name, opts->list_props);
    struct ui_scroll_list_data* list_data = malloc(sizeof(*list_data));
    ARR_INIT(list_data->items);
    list_data->scroll_pos = 0;
    list_data->scroll_max = 0;
    list_data->num_elems = opts->num_elems;
    list_data->udata = opts->udata;
    list_data->update_elem = opts->update_elem;
    pg_ui_variable(ctx, list_grp, "_list_data")->ptr[0] = list_data;
    int i;
    for(i = 0; i < opts->num_elems; ++i) {
        pg_ui_t list_item = opts->make_elem(ctx, list_grp, i);
        ARR_PUSH(list_data->items, list_item);
    }
    return list_grp;
}

void ui_scroll_list_update(struct pg_ui_context* ctx, pg_ui_t list)
{
    struct ui_scroll_list_data* list_data = pg_ui_variable(ctx, list, "_list_data")->ptr[0];
    int i;
    for(i = 0; i < list_data->num_elems; ++i) {
        if(i >= list_data->scroll_max) {
            pg_ui_enabled(ctx, list_data->items.data[i], 0);
        } else {
            list_data->update_elem(ctx, list_data->items.data[i],
                                   list_data->scroll_pos + i,
                                   list_data->udata);
        }
    }
}

void ui_scroll_list_set_max(struct pg_ui_context* ctx, pg_ui_t list, int max)
{
    struct ui_scroll_list_data* list_data = pg_ui_variable(ctx, list, "_list_data")->ptr[0];
    list_data->scroll_max = max;
    list_data->scroll_pos = LM_MAX(0,
        LM_MIN(list_data->scroll_pos, list_data->scroll_max - list_data->num_elems));
    ui_scroll_list_update(ctx, list);
}

void ui_scroll_list_move(struct pg_ui_context* ctx, pg_ui_t list, int move)
{
    struct ui_scroll_list_data* list_data = pg_ui_variable(ctx, list, "_list_data")->ptr[0];
    list_data->scroll_pos = LM_MAX(0,
        LM_MIN(list_data->scroll_pos, list_data->scroll_max - list_data->num_elems));
    ui_scroll_list_update(ctx, list);
}

void ui_scroll_list_nav_btn(struct pg_ui_context* ctx, pg_ui_t btn)
{
    struct pg_type* list_var = pg_ui_variable(ctx, btn, "list");
    if(!list_var) return;
    pg_ui_t list = list_var->i[0];
    int move = list_var->i[1];
    ui_scroll_list_move(ctx, list, move);
}

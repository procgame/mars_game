#include "procgl/procgl.h"
#include "marsgame.h"

/****************************************/
/*  COMMON ENTITY BEHAVIOR FUNCTIONS    */
/****************************************/

MG_ENTITY_RAYCAST(mg_entity_raycast_sphere)
{
    return raycast_sphere(p, d, ent->collider.sph, ent->collider.radius);
}

#include "procgl/procgl.h"
#include "marsgame.h"

PG_MEMPOOL_DEFINE(struct mg_homebase_upgrade, mg_homebase_upgrade, alloc);

static struct mg_homebase_upgrade_info UPGRADE_INFO[] = {
    { "hab", L"H2O Recyclers", "hab_h2o",
        .materials_cost = 25 },
    { "hab", L"Improved H2O Recyclers", "hab_h2o_2",
        .depends_name = { "hab_h2o" },
        .materials_cost = 50 },
    { "farm", L"H2O Recyclers", "grn_h2o",
        .materials_cost = 25 },
    { "farm", L"Improved H2O Recyclers", "grn_h2o_2",
        .depends_name = { "grn_h2o" },
        .materials_cost = 50 },
    { "garage", L"Pressurized Rovers", "gar_rovers",
        .materials_cost = 50 },
    { "garage", L"Rover Cargo Module", "gar_rover_storage",
        .depends_name = { "gar_rovers" },
        .materials_cost = 50 },
    { "garage", L"Rover Science Module", "gar_rover_science",
        .depends_name = { "gar_rovers" },
        .materials_cost = 50 },
    { "garage", L"Gliders", "gar_gliders",
        .depends_name = { "gar_rovers" },
        .materials_cost = 100 },
    { "garage", L"Glider Cargo Module", "gar_glider_storage",
        .depends_name = { "gar_gliders" },
        .materials_cost = 100 },
    { "garage", L"Glider Science Module", "gar_glider_science",
        .depends_name = { "gar_gliders" },
        .materials_cost = 100 },
};

static const int num_upgrade_types =
    sizeof(UPGRADE_INFO) / sizeof(UPGRADE_INFO[0]);

int mg_num_upgrade_types(void) { return num_upgrade_types; }

void mg_homebase_init_upgrades(struct mg_homebase_state* h)
{
    printf("CONSTRUCTING DOME UPGRADE TREE\n");
    int i, j;
    for(i = 0; i < num_upgrade_types; ++i) {
        struct mg_homebase_upgrade_info* up = &UPGRADE_INFO[i];
        printf("%d %s\n", i, up->id_name);
        HTABLE_SET(h->up_names, up->id_name, i);
        HTABLE_GET_V(h->dome_names, up->dome_id_name, up->dome_type);
    }
    for(i = 0; i < num_upgrade_types; ++i) {
        struct mg_homebase_upgrade_info* up = &UPGRADE_INFO[i];
        for(j = 0; j < 4; ++j) {
            up->depends_id[j] = -1;
            HTABLE_GET_V(h->up_names, up->depends_name[j], up->depends_id[j]);
        }
    }
}

const struct mg_homebase_upgrade_info* mg_homebase_upgrade_info(int up)
{
    if(up < 0 || up >= num_upgrade_types) return NULL;
    else return &UPGRADE_INFO[up];
}

int mg_homebase_have_upgrade(struct mg_homebase_state* h, mg_homebase_upgrade_info_t up)
{
    const struct mg_homebase_upgrade_info* info = &UPGRADE_INFO[up];
    int dome_idx;
    for(dome_idx = 0; dome_idx < h->domes.len; ++dome_idx) {
        struct mg_homebase_dome* dome =
            mg_homebase_dome_get(&h->dome_pool, h->domes.data[dome_idx]);
        if(dome->type != info->dome_type) continue;
        int up_idx;
        for(up_idx = 0; up_idx < dome->upgrades.len; ++up_idx) {
            struct mg_homebase_upgrade* upgrade =
                mg_homebase_upgrade_get(&h->up_pool, dome->upgrades.data[up_idx]);
            if(upgrade->info == up) {
                return upgrade->completed ? 2 : 1;
            }
        }
    }
    return 0;
}

void mg_homebase_get_upgrade(struct mg_homebase_state* h, int up)
{
    
    //ARR_PUSH(h->have_upgrades, up);
}

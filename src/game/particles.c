#include "procgl/procgl.h"
#include "marsgame.h"

void mg_particles_init(struct marsgame* mg)
{
    struct mg_particle_system* parts = &mg->particle_system;
    ARR_INIT(parts->particles);
    pg_quadbatch_init_pass(&mg->quadbatch, &parts->particles_pass, &mg->target);
    pg_renderpass_viewer(&parts->particles_pass, &mg->worldview);
    pg_renderpass_blending(&parts->particles_pass, 1, GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    pg_renderpass_depth_test(&parts->particles_pass, 1, GL_LEQUAL);
}

/*  Depth sorting   */
static int particle_comp(struct mg_particle a, struct mg_particle b, vec3* plane)
{
    float d_a = vec3_dot(vec3_sub(a.pos, plane[0]), plane[1]);
    float d_b = vec3_dot(vec3_sub(b.pos, plane[0]), plane[1]);
    return (d_a < d_b);
}
KSORT_S_DEF(particles, struct mg_particle, vec3*, particle_comp)

void mg_particles_tick(struct mg_particle_system* parts)
{
    int i;
    struct mg_particle* part;
    ARR_FOREACH_PTR_REV(parts->particles, part, i) {
        const struct mg_particle_info* info = &PARTICLE_TYPES[part->type];
        if(part->tick >= info->lifetime) {
            ARR_SWAPSPLICE(parts->particles, i, 1);
            continue;
        }
        ++part->tick;
        part->pos = vec3_add(part->pos, part->vel);
    }
    vec3 plane[2] = { parts->viewer_pos, vec3_norm(parts->viewer_dir) };
    ks_introsort_s_particles(parts->particles.len, parts->particles.data, plane);
}

static void draw_particle(struct marsgame* mg, struct mg_particle_system* sys,
                          struct mg_particle* part, float dt)
{
    vec2 scale;
    vec4 tex_frame, color_mod, color_add;
    const struct mg_particle_info* info = &PARTICLE_TYPES[part->type];
    float phase_time = ((float)part->tick + dt) / info->lifetime * info->n_phases;
    phase_time = LM_CLAMP(phase_time, 0, info->n_phases - 0.00001f);
    if(info->n_phases == 1) {
        scale = info->phases[0].scale;
        tex_frame = info->phases[0].tex_frame;
        color_mod = info->phases[0].color_mod;
        color_add = info->phases[0].color_add;
    } else {
        const struct mg_particle_phase* phase = &info->phases[(int)phase_time];
        const struct mg_particle_phase* next_phase = &info->phases[(int)phase_time + 1];
        float phase_interp = phase_time - truncf(phase_time);
        printf("%d %d %f\n", (int)phase_time, (int)phase_time + 1, phase_interp);
        scale = vec2_lerp(phase->scale, next_phase->scale, phase_interp);
        tex_frame = phase->tex_frame;
        color_mod = vec4_lerp(phase->color_mod, next_phase->color_mod, phase_interp);
        color_add = vec4_lerp(phase->color_add, next_phase->color_add, phase_interp);
    }
    float dist_fade = vec3_dist2(part->pos, sys->viewer_pos) / (40*40);
    dist_fade = 1 - LM_CLAMP(dist_fade, 0, 1);
    color_add.w *= dist_fade;
    pg_quadbatch_add_quad(&mg->quadbatch, PG_EZQUAD(
        .pos = vec3(POS_LERP3(part, dt)), .scale = scale,
        .orientation = part->rot,
        .tex_frame = tex_frame,
        .color_mod = VEC4_TO_UINT(color_mod),
        .color_add = VEC4_TO_UINT(color_add), ));
}

void mg_particles_draw(struct marsgame* mg, float dt)
{
    struct mg_particle_system* parts = &mg->particle_system;
    quat rot = parts->billboard_rot;
    pg_renderpass_clear(&parts->particles_pass);
    mat4 tx = mat4_identity();
    pg_quadbatch_next(&mg->quadbatch, &tx);
    int i;
    struct mg_particle* part;
    ARR_FOREACH_PTR_REV(parts->particles, part, i) {
        draw_particle(mg, parts, part, dt);
    }
    pg_quadbatch_draw(&mg->quadbatch, &parts->particles_pass);
}

void mg_particles_add(struct mg_particle_system* parts, struct mg_particle* new_part)
{
    ARR_PUSH(parts->particles, *new_part);
}

#include "procgl/procgl.h"
#include "marsgame.h"

#if 0
void mg_enemies_tick(struct mg_battle_state* b)
{
    int enemies_alive = 0;
    mg_entpool_id_t ent_id;
    struct mg_entity* ent;
    int i;
    ARR_FOREACH_REV(b->space.all_enemies, ent_id, i) {
        ent = mg_entpool_get(ent_id);
        if(!ent) {
            ARR_SWAPSPLICE(b->space.all_enemies, i, 1);
            continue;
        } else if(ent->dead || ent->last_tick == b->sim_tick) continue;
        ent->last_tick = b->sim_tick;
        ++enemies_alive;
        /*  Actual enemy tick   */
        if(ent->tick) ent->tick(b, ent);
        ent = mg_entpool_get(ent_id);
        if(ent->HP <= 0) ent->dead = 1;
        /*  Put it in the spatial partitioning for queries NEXT tick    */
        mg_battle_space_assign_enemy(&b->space, ent, ent_id);
    }
    b->enemies_alive = enemies_alive;
}

void mg_enemies_draw(struct marsgame* mg, quat view_inv, float dt)
{
    struct mg_battle_state* b = &mg->battle;
    mg_entpool_id_t ent_id;
    struct mg_entity* ent;
    int i;
    ARR_FOREACH_REV(b->space.all_enemies, ent_id, i) {
        ent = mg_entpool_get(ent_id);
        if(!ent || ent->dead) continue;
        if(ent->draw) ent->draw(mg, ent, dt);
    }
}

#endif

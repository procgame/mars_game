#include "procgl/procgl.h"
#include "marsgame.h"

/*  homebase_ui_domes.c */
pg_ui_t ui_domes_create(struct pg_ui_context* ctx, struct mg_homebase_state* h);

/********************************/
/*  BASE UI                     */
/********************************/

void mg_homebase_build_ui(struct pg_ui_context* ctx, struct mg_homebase_state* h)
{
    pg_ui_t root = ctx->root;
    h->ui = pg_ui_add_group(ctx, root, "homebase",
        PG_UI_PROPERTIES( .pos = vec2(0, 0), .enabled = 0));
    pg_ui_t dome_ui = ui_domes_create(ctx, h);
}


#define MG_BATTLE_PARTITIONS_X  8
#define MG_BATTLE_PARTITIONS_Y  8
#define MG_BATTLE_PARTITION_WIDTH   16.0f
#define MG_BATTLE_PARTITION_HEIGHT  16.0f
#define MG_BATTLE_SPACE_WIDTH  (MG_BATTLE_PARTITIONS_X * MG_BATTLE_PARTITION_WIDTH)
#define MG_BATTLE_SPACE_HEIGHT (MG_BATTLE_PARTITIONS_Y * MG_BATTLE_PARTITION_HEIGHT)
#define MG_BATTLE_GROUNDTILES_X     4
#define MG_BATTLE_GROUNDTILES_Y     4

/*  The "battle space" structure owns/manages all of the game entities.
    It is used to perform spatial queries like raycasts or volume tests,
    and resolves collisions in its tick function. It also handles freeing
    dead entities in its tick function; entities should not be freed anywhere
    else! Just set entity.dead=1 to mark it to be freed safely  */
struct mg_battle_space {
    /*  Convenience query numbers for single-volume queries */
    uint64_t query_no, coll_query_no;
    /*  Results of the last (or current) query  */
    mg_entity_arr_t query;
    /*  Pairs of colliding entities. [0] is each broad phase entity,
        whose 'n_phys_pairs' value contains the number of potential colliding
        entities that are stored in [1], after all those for all previous
        broad phase entities    */
    mg_entity_arr_t collision_pairs[2];
    uint64_t space_tick, game_tick;
    /*  Every single active colliding entity is here    */
    mg_entity_arr_t all_entities;
    /*  The spatial partition structure, a uniform grid. There are two
        copies of the whole structure, with each physics frame alternating
        which copies to read/write; All collisions for one frame should only
        be calculated based on physics states from the last frame   */
    mg_entity_arr_t entities[2][MG_BATTLE_PARTITIONS_X][MG_BATTLE_PARTITIONS_Y];
    int side;
    #ifdef MG_DEBUG_COLLISIONS
    int n_debugged_ents, n_debugged_pairs, n_intersection_tests;
    #endif
};


void mg_battle_space_init(struct mg_battle_space* space);
void mg_battle_space_deinit(struct mg_battle_space* space);
void mg_battle_space_reset(struct mg_battle_space* space);

/*  Perform one collision resolution pass   */
void mg_battle_space_resolve_collisions(struct mg_battle_space* space);

/*  Finish a physics frame  */
void mg_battle_space_swap(struct mg_battle_space* space);

/*  Add an entity to the spatial partition, so it will be considered for
    queries in the next frame.   */
void mg_battle_space_assign_entity(struct mg_battle_space* space, mg_entpool_id_t ent_id);

/*  A battle space query is basically a set of currently selected entities.
    Chained operations can be seen as working on the set produced by the last
    one. The selected set is stored in the space's own "query" variable.
    Only one query can be active at a time. */
void mg_battle_space_new_query(struct mg_battle_space* space);

/*  All entities whose bounding box overlaps a given aabb   */
int mg_battle_space_query_aabb(struct mg_battle_space* space,
                               vec3 Q0, vec3 Q1, uint32_t filter);
/*  All entities whose bounding box intersects a ray (directed segment) */
int mg_battle_space_query_ray(struct mg_battle_space* space,
                              vec3 r0, vec3 r1, uint32_t filter);

/*  All entities whose bounding box is hit by a ray. This does NOT
    call each entity's 'raycast' behavior, however it does still update
    each entity's 'last_ray_hit' value for its bounding box. The closest
    one will be the first entity in the selection, the rest are unsorted.   */

struct mg_space_ray_hit {
    mg_entpool_id_t hit;
    float distance;
};
struct mg_space_ray_hit mg_battle_space_raycast(
        struct mg_battle_space* space, vec3 src, vec3 dir, uint32_t filter);

/*  All entities that collide with a collider, outputs the final collision
    found   */
void mg_battle_space_query_collider(struct mg_battle_space* space,
                                    struct mg_collision* out,
                                    struct mg_collider* coll);


#include "procgl/procgl.h"
#include "marsgame.h"

void mg_init_battle(struct marsgame* mg)
{
    struct mg_battle_state* b = &mg->battle;
    ARR_INIT(b->bullets);
    ARR_INIT(b->objective.kill_enemies);
    b->mg = mg;
    b->ui_context = &mg->ui;
    b->ui = mg_battle_build_ui(&mg->ui);
    mg_battle_space_init(&b->space);
    b->science = 0;
    b->materials = 0;
}

void mg_deinit_battle(struct mg_battle_state* b)
{
    ARR_DEINIT(b->bullets);
    mg_battle_space_deinit(&b->space);
}

static void make_dome(struct mg_battle_state* b, int v_segs, int h_segs,
                      float scale, vec2 pos)
{
    float segment_height = 2.0f * (4*v_segs) * scale * sin(LM_PI / (4*v_segs)) / (4*v_segs);
    int i, j;
    for(i = 0; i < v_segs; ++i) {
        float rot = sin(((float)i+0.5) / (v_segs+1.5) * LM_PI_2) * LM_PI_2;
        float i_sin  = sin(((float)i / v_segs) * LM_PI_2);
        float i_cos = cos(((float)i / v_segs) * LM_PI_2);
        float next_sin  = sin(((float)(i+1) / v_segs) * LM_PI_2);
        float next_cos = cos(((float)(i+1) / v_segs) * LM_PI_2);
        float bottom_size = 2.0f * h_segs * (i_cos * scale) * sin(LM_PI / h_segs) / h_segs;
        float top_size = 2.0f * h_segs * (next_cos * scale) * sin(LM_PI / h_segs) / h_segs;
        float z = i_sin * scale;
        for(j = 0; j < h_segs; ++j) {
            struct mg_entity* seg;
            mg_entpool_id_t seg_id = mg_new_envobj(MG_ENVOBJ_DOME_SEGMENT, &seg);
            struct mg_envobj_dome_segment* seg_data = mg_envobj_dome_segment(seg);
            float angle = (j / (float)h_segs) * LM_TAU;
            vec2 seg_pos = vec2_scale(vec2_rotate(vec2_Y(), angle), scale * i_cos);
            seg_pos = vec2_add(seg_pos, pos);
            seg->pos = vec3(VEC_XY(seg_pos), z);
            seg->rot = quat_from_sph(angle, rot);
            seg_data->size = vec2(bottom_size * 1.05, segment_height);
            seg_data->tip_scale = (top_size * 1.05) / seg_data->size.x;
            seg_data->color = ubvec4(RANDI(64) + 150, RANDI(64) + 150, RANDI(64) + 190, 255);
            ARR_PUSH(b->space.all_entities, seg_id);
        }
    }
}

void mg_start_battle(struct marsgame* mg)
{
    struct mg_homebase_state* h = &mg->homebase;
    struct mg_missions_state* m = &mg->mission;
    struct mg_battle_state* b = &mg->battle;
    pg_ui_enabled(b->ui_context, b->ui, 1);
    ARR_TRUNCATE_CLEAR(b->bullets, 0);
    mg_battle_space_reset(&b->space);
    b->tick = 0;
    b->paused = 0;
    b->run_simulation = 1;
    b->should_end_mission = 0;
    b->vehicle_pos = vec3(72, 16, 0.25);
    b->plr = mg_new_PC(NULL);
    ARR_PUSH(b->space.all_entities, b->plr);

    int msn_idx = m->selected_mission;
    b->mission_idx = msn_idx;
    printf("Mission idx %d\n", msn_idx);
    /*  Here we should set the objective based on the mission   */
    mg_homebase_bulletin_id_t blt_id = m->missions[msn_idx].blt;
    /*  Here we should set the player entity based on the person    */
    mg_homebase_person_id_t ppl_id = m->missions[msn_idx].ppl;

    int i, j;
    ARR_TRUNCATE(b->objective.kill_enemies, 0);
    for(i = 0; i < 0; ++i) {
        struct mg_entity* enemy_ent;
        mg_entpool_id_t enemy_id = mg_new_enemy(MG_ENEMY_SPAWNER, &enemy_ent);
        enemy_ent->pos = vec3( 72 + RANDF2(40), 72 + RANDF2(40), 1.5 );
        ARR_PUSH(b->space.all_entities, enemy_id);
        ARR_PUSH(b->objective.kill_enemies, enemy_id);
        int num_mats = rand() % 5 + 5;
        vec3 pos = enemy_ent->pos;
        for(j = 0; j < num_mats; ++j) {
            struct mg_entity* mat_ent;
            mg_entpool_id_t mat_id = mg_new_item(MG_ITEM_PICKUP_MATERIAL, &mat_ent);
            mat_ent->pos = vec3(pos.x + RANDF2(10),
                                pos.y + RANDF2(10), 0);
            ARR_PUSH(b->space.all_entities, mat_id);
        }
    }

    /*  Random boulders! */
    vec2 size = vec2(0.5, 1);
    vec2 offset = vec2(0);
    for(i = 0; i < 4; ++i) {
        size = vec2_add(size, vec2(RANDF(0.5) + 0.25, RANDF(1) + 1));
        offset = vec2_add(offset, vec2(size.x, size.y * 2));
        for(j = 0; j < 2; ++j) {
            struct mg_entity* boulder;
            mg_entpool_id_t boulder_id = mg_new_envobj(MG_ENVOBJ_BOULDER, &boulder);
            mg_entity_set_bound_centered(boulder, size.v[j]);
            mg_entity_set_pos(boulder, vec3(60 + offset.v[j], 30 + j * 7.5f, 0));
            boulder->collider.radius = size.v[j];
            boulder->rot = quat_from_sph(RANDF2(LM_PI), RANDF2(0.1));
            ARR_PUSH(b->space.all_entities, boulder_id);
        }
    }
    
    struct mg_entity* ground_ent;
    mg_entpool_id_t ground_ent_id = mg_new_envobj(MG_ENVOBJ_GROUND, &ground_ent);
    ARR_PUSH(b->space.all_entities, ground_ent_id);
    mg_entity_set_pos(ground_ent, vec3(64, 64));
    //make_dome(b, 4, 20, 48, vec2(60, 20));

    /*  Reset the viewport  */
    b->view_angle = vec2(0, 0);
    pg_renderpass_viewer(&mg->world_pass, &b->worldview);
    pg_renderpass_viewer(&mg->box_pass, &b->worldview);
    pg_renderpass_viewer(&mg->model_pass, &b->worldview);

    /*  Set the input state */
    b->mouse_motion = vec2(0, 0);
    b->mouse_pos = vec2(0, 0);
    pg_reset_input();
    //pg_mouse_mode(1);
    int bx, by;
    for(bx = 0; bx < MG_BATTLE_PARTITIONS_X; ++bx) for(by = 0; by < MG_BATTLE_PARTITIONS_Y; ++by) {
        b->tiles[bx][by] = 0;
    }
}

void mg_end_battle(struct mg_battle_state* b)
{
    struct marsgame* mg = b->mg;
    struct mg_homebase_state* h = &mg->homebase;
    struct mg_missions_state* m = &mg->mission;
    int msn_idx = b->mission_idx;
    printf("Mission idx %d\n", msn_idx);
    m->missions[msn_idx].status = MG_MISSION_SUCCESS;

    int i;
    mg_entpool_id_t ent;
    ARR_FOREACH(b->space.all_entities, ent, i) mg_entpool_free(ent);
    ARR_TRUNCATE(b->bullets, 0);
    mg_entpool_free(b->plr);
    pg_ui_enabled(b->ui_context, b->ui, 0);
}

void mg_battle_update(struct marsgame* mg)
{
    struct mg_battle_state* b = &mg->battle;
    vec2 mouse_motion = vec2_scale(pg_mouse_motion(), 0.001);
    if(b->run_simulation) {
        b->view_angle = vec2_sub(b->view_angle, mouse_motion);
    }
}

void mg_battle_input(struct marsgame* mg)
{
    struct mg_battle_state* b = &mg->battle;
    struct mg_entity* plr = mg_entpool_get(b->plr);
    if(pg_check_input(SDL_SCANCODE_ESCAPE, PG_CONTROL_HIT)) {
        b->paused = 1 - b->paused;
        if(b->paused) pg_mouse_mode(0);
        else pg_mouse_mode(1);
    }
        //mg_player_input(b, plr);
    if(!b->paused && b->run_simulation) {
    }
}

void mg_battle_tick(struct marsgame* mg)
{
    struct mg_battle_state* b = &mg->battle;
    struct mg_entity* plr = mg_entpool_get(b->plr);
    /*  Calculate the viewport  */
    quat view_rot, y_plane;
    y_plane = quat_rotation(vec3_X(), M_PI * -0.5);
    view_rot = quat_mul(y_plane, quat_rotation(vec3_Z(), b->view_angle.x));
    quat view_quat = quat_mul(quat_rotation(vec3_Z(), b->view_angle.x),
                              quat_rotation(vec3_X(), b->view_angle.y));
    vec3 view_offset = quat_mul_vec3(view_quat, vec3(0.5, -1, 0));
    //b->view_dir = quat_mul_vec3(view_quat, vec3_Y());
    b->view_dir = vec3_from_sph(VEC_XY(b->view_angle));
    b->view_pos = vec3_add(vec3_add(plr->pos, vec3(0,0,0.5)), view_offset);
    //pg_viewer_perspective(&b->worldview, vec3(0, 0, 0), view_rot, mg->halfres, vec2(0.01, 100));
    /*  Do tick */
    ++b->tick;
    mg_battle_input(mg);
    if(b->run_simulation) {
        ++b->sim_tick;
        if(b->sim_tick % 5) {
            struct mg_particle new_part = {
                .pos = vec3(plr->pos.x + 32, plr->pos.y + RANDF2(32), -1 + RANDF2(1)),
                .rot = quat_mul(quat_rotation(vec3_Z(), RANDF2(0.5)), quat_rotation(vec3_Y(), LM_PI_2)),
                .vel = vec3(-1 + RANDF2(0.05), 0, 0),
                .type = MG_PARTICLE_WIND0 + (RANDI(3) != 0), };
            mg_particles_add(&mg->particle_system, &new_part);
        }
        /*  Revalidate since other tick functions may allocate new entities */
        plr = mg_entpool_get(b->plr);
        mg_particles_tick(&mg->particle_system);
        //mg_player_tick(b, plr);
        mg_bullets_update(b);
        mg_entities_tick(b);
        // Uncomment more for more precise collisions
        mg_battle_space_resolve_collisions(&b->space);
        //mg_battle_space_resolve_collisions(&b->space);
        //mg_battle_space_resolve_collisions(&b->space);
        //mg_battle_space_resolve_collisions(&b->space);
        mg_battle_space_swap(&b->space);
    } else {
        if(b->should_end_mission) {
            mg_end_battle(b);
            mg_start_missions(mg);
            mg->state = MG_MISSIONS;
        }
    }
}

void mg_battle_map_draw(struct marsgame* mg, float dt)
{
    struct mg_battle_state* b = &mg->battle;
    /*  Draw the ground */
    pg_tex_frame_t frame = pg_texture_frame(&mg->sgtex, 1, 0);
    vec2 tile_size = vec2( MG_BATTLE_PARTITION_WIDTH / MG_BATTLE_GROUNDTILES_X,
                           MG_BATTLE_PARTITION_WIDTH / MG_BATTLE_GROUNDTILES_Y );
    int i, j;
    for(i = 0; i < MG_BATTLE_PARTITIONS_X *  MG_BATTLE_GROUNDTILES_X; ++i) {
        for(j = 0; j < MG_BATTLE_PARTITIONS_Y *  MG_BATTLE_GROUNDTILES_Y; ++j) {
            vec4 subframe = vec4_mul(frame.frame, vec4(1, 1, 0.25, 0.25));
            subframe = vec4_add(subframe, vec4(subframe.z * (i % 4), subframe.w * (j % 4),
                                               subframe.z * (i % 4), subframe.w * (j % 4)));
            subframe = frame.frame;
            vec4 color = vec4( 0.25, 0, 0, 1 );
            color.x += 0.75 * b->tiles[i][j];
            pg_quadbatch_add_quad(&mg->quadbatch, PG_EZQUAD(
                .tex_frame = subframe, .tex_layer = 1,
                .pos = vec3( i * tile_size.x, j * tile_size.y, 0 ),
                .scale = vec2( tile_size.x, tile_size.y ),
                .orientation = quat_identity(),
                .color_mod = 0xFFFFFFFF ));
        }
    }
}

void mg_battle_draw_vehicle(struct marsgame* mg, float dt)
{
    struct mg_battle_state* b = &mg->battle;
    pg_boxbatch_add_box(&mg->boxbatch, &PG_EZBOX(
        .pos = b->vehicle_pos, .iso_scale = 4, .aniso_scale = vec3(1, 0.5, 0.5),
        .color_mod = ubvec4(0), .color_add = ubvec4(96, 96, 192, 255) ));
    
}

void mg_battle_draw(struct marsgame* mg, float dt)
{
    struct mg_battle_state* b = &mg->battle;
    struct mg_entity* plr = mg_entpool_get(b->plr);
    struct mg_player_data* plr_data = mg_player_data(plr);
    if(!b->run_simulation) dt = b->last_dt;
    else b->last_dt = dt;
    pg_quadbatch_reset(&mg->quadbatch);
    pg_boxbatch_reset(&mg->boxbatch);
    /*  3D stuff    */
    quat y_plane = quat_rotation(vec3_X(), LM_PI * -0.5);
    quat view_rot = quat_rotation(vec3_X(), -b->view_angle.y);
    view_rot = quat_mul(view_rot, quat_rotation(vec3_Z(), -b->view_angle.x));
    view_rot = quat_mul(y_plane, view_rot);
    quat view_inv = quat_rotation(vec3_Z(), b->view_angle.x);
    view_inv = quat_mul(view_inv, y_plane);
    mg->particle_system.billboard_rot = quat_mul(quat_from_sph(b->view_angle.x, b->view_angle.y), y_plane);
    /*  Calculate 3d viewport   */
    vec3 plr_pos = vec3( POS_LERP3(plr, dt) );
    quat view_quat = quat_mul(quat_rotation(vec3_Z(), b->view_angle.x),
                              quat_rotation(vec3_X(), b->view_angle.y));
    vec3 view_offset = quat_mul_vec3(view_quat, vec3(0.5, -1, 0));
    vec3 view_pos = vec3_add(vec3_add(plr_pos, vec3(0,0,0.5)), view_offset);
    mg->particle_system.viewer_pos = view_pos;
    vec3 view_dir = vec3_from_sph(b->view_angle.x, b->view_angle.y);
    mg->particle_system.viewer_dir = view_dir;
    pg_viewer_perspective(&b->worldview, view_pos, view_rot, mg->halfres, vec2(0.01, 100));
    mg->worldview = b->worldview;
    mat4 tx = mat4_identity();
    pg_quadbatch_next(&mg->quadbatch, &tx);
    pg_boxbatch_next(&mg->boxbatch, &tx);
    /*  Draw the things */
    pg_tex_frame_t frame;
    mg_battle_map_draw(mg, dt);
    mg_battle_draw_vehicle(mg, dt);
    mg_bullets_draw(mg, view_inv, dt);
    mg_entities_draw(mg, dt);
    //mg_player_draw(mg, plr, dt);
    pg_quadbatch_draw(&mg->quadbatch, &mg->world_pass);
    pg_boxbatch_draw(&mg->boxbatch, &mg->box_pass);
    mg_particles_draw(mg, dt);

    struct pg_model_pose pose;
    float poselerp = LM_FMOD(((float)b->tick + dt) / 60.0f, 2.0f);
    if(poselerp > 1) poselerp = (2 - poselerp);
    pg_model_pose_interp(&mg->test_model, &pose, 1, 2, poselerp);
    mat4 modeltx = mat4_translation(vec3(60, 20, 1));
    pg_model_draw_rigged(&mg->model_pass, &mg->test_model, &pose, modeltx);

    /*  Overlay */
    pg_quadbatch_next(&mg->quadbatch, &tx);
    pg_tex_frame_t O_frame = pg_texture_frame(&mg->sgtex, 0, 'O' - 32);
    /*  Reticle */
    /*
    pg_quadbatch_add_sprite(&mg->quadbatch, PG_EZDRAW_2D(
        .frame = O_frame, .pos = vec3(0,0), .scale = vec2(32, 32),
        .color_mod = vec4(0.1, 0.8, 0.1, 0.5) ));*/
    /*  Shitty HUD  */
    struct pg_text_formatter text_fmt = PG_TEXT_FORMATTER(
        .fonts = PG_FONTS(3, &mg->sgfont[0], &mg->sgfont[1], &mg->sgfont[2]),
        .htab = 6, .spacing = vec2(1,1), .size = vec2(0.1,0.1),
        .region = mg->halfres, .align = PG_TEXT_LEFT );
    struct mg_entity* weap = NULL; //mg_entpool_get(plr->held_item);
    int obj_complete = mg_battle_objective_status(&b->objective);
    int by_vehicle = (vec3_dist2(plr_pos, b->vehicle_pos) > (3*3));
    wchar_t hud_text[256];
    int len = swprintf(hud_text, 256,
        L"%%F1HEALTH: %%F2%d\n"
        L"%%F1WEAPON: %%F0%s\n"
        L"%%F1SCIENCE: %%F2%d\n"
        L"%%F1MATERIALS: %%F2%d\n"
        L"%%F1%ls",
        plr->HP,
        weap ? weap->name : "",
        b->science, b->materials,
        obj_complete ?
            (by_vehicle ? L"OBJECTIVE COMPLETE\nRETURN TO VEHICLE"
                        : L"OBJECTIVE COMPLETE\nPRESS [E] TO RETURN TO BASE")
            : L"");
    vec2 drawpos = vec2_floor(vec2_mul(mg->halfres, vec2(-0.8, 0.8)));
    /*
    pg_quadbatch_add_text(&mg->quadbatch, PG_EZDRAW_TEXT(
        .formatter = &text_fmt, .str = hud_text, .len = len,
        .color = vec4(1,1,1,1), .pos = drawpos, .scale = vec2(1,1) ));*/
    
    /*  Compass */
    vec2 plr_to_veh = vec2_norm(vec2_sub(vec2(VEC_XY(b->vehicle_pos)), vec2(VEC_XY(plr_pos))));
    plr_to_veh = vec2_rotate(plr_to_veh, -b->view_angle.x);
    float vehicle_angle = atan2(plr_to_veh.x, plr_to_veh.y);
    if(vec3_dist2(plr_pos, b->vehicle_pos) > (16*16)) {
        pg_quadbatch_add_sprite(&mg->quadbatch, PG_EZDRAW_2D(
            .frame = O_frame, .pos = vec3(vehicle_angle*mg->halfres.x, 0.9*mg->halfres.y),
            .scale = vec2(16,16), .color_mod = vec4(0.1, 0.1, 0.8, 0.5) ));
    }
    
    /*  Finish  */
    pg_quadbatch_draw(&mg->quadbatch, &mg->overlay_pass);
    pg_quadbatch_buffer(&mg->quadbatch);
    pg_boxbatch_buffer(&mg->boxbatch);
}

#include "procgl/procgl.h"
#include "marsgame.h"

PG_MEMPOOL_DEFINE(struct mg_homebase_bulletin, mg_homebase_bulletin, alloc);

static struct mg_homebase_bulletin_info BULLETIN_INFO[] = {
    { "central", L"Material gathering", "supply_run",
        .permanent = 1, .is_mission = 1, },
    { "central", L"Science expedition", "science_run",
        .permanent = 1, .is_mission = 1, },
    { "hab", L"Low water supply", "water_run",
        .permanent = 1, .is_mission = 1, },
    { "comms", L"Received distress signal", "distress",
        .permanent = 0, .is_mission = 1, },
};

static const int num_bulletin_types = sizeof(BULLETIN_INFO) / sizeof(BULLETIN_INFO[0]);

int mg_num_bulletin_types(void) { return num_bulletin_types; }

const struct mg_homebase_bulletin_info* mg_homebase_bulletin_info(int blt)
{
    return &BULLETIN_INFO[blt];
}

void mg_homebase_init_bulletins(struct mg_homebase_state* h)
{
    printf("CONSTRUCTING BULLETIN INFO\n");
    int i, j;
    for(i = 0; i < num_bulletin_types; ++i) {
        struct mg_homebase_bulletin_info* blt = &BULLETIN_INFO[i];
        HTABLE_SET(h->blt_names, blt->id_name, i);
        printf("%d %s %s\n", i, blt->id_name, blt->dome_id_name);
        HTABLE_GET_V(h->dome_names, blt->dome_id_name, blt->dome_type);
    }
}

mg_homebase_person_id_t mg_homebase_bulletin_get_assignee(
                            struct mg_homebase_state* h,
                            mg_homebase_bulletin_id_t blt_id)
{
    if(blt_id == -1) return -1;
    int i;
    for(i = 0; i < h->personnel.len; ++i) {
        struct mg_homebase_person* ppl =
            mg_homebase_person_get(&h->ppl_pool, h->personnel.data[i]);
        if(ppl->assigned_mission == blt_id) {
            return h->personnel.data[i];
        }
    }
    return -1;
}
    

int mg_homebase_have_mission(struct mg_homebase_state* h, mg_homebase_bulletin_info_t blt)
{
    const struct mg_homebase_bulletin_info* info = &BULLETIN_INFO[blt];
    int dome_idx;
    for(dome_idx = 0; dome_idx < h->domes.len; ++dome_idx) {
        struct mg_homebase_dome* dome =
            mg_homebase_dome_get(&h->dome_pool, h->domes.data[dome_idx]);
        if(dome->type != info->dome_type) continue;
        int blt_idx;
        for(blt_idx = 0; blt_idx < dome->bulletins.len; ++blt_idx) {
            struct mg_homebase_bulletin* bulletin =
                mg_homebase_bulletin_get(&h->blt_pool, dome->bulletins.data[blt_idx]);
            if(bulletin->info == blt) {
                return 1;
            }
        }
    }
    return 0;
}

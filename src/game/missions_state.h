struct mg_missions_state {
    struct marsgame* marsgame;
    /*  UI  */
    struct pg_ui_context* ui_context;
    pg_ui_t ui;
    /*  Rendering   */
    struct pg_viewer worldview;
    /*  Game state  */
    int tick;
    int should_begin_mission;
    struct {
        mg_homebase_bulletin_id_t blt;
        mg_homebase_person_id_t ppl;
        enum {
            MG_MISSION_READY,
            MG_MISSION_ABORTED,
            MG_MISSION_FAILED,
            MG_MISSION_SUCCESS,
        } status;
    } missions[8];
    int selected_mission;
    int num_missions;
};

/****************************************/
/*  Base functions  (missions_state.c)  */
/****************************************/
void mg_init_missions(struct marsgame* mg);
void mg_deinit_missions(struct mg_missions_state* h);
void mg_start_missions(struct marsgame* mg);
void mg_missions_update(struct marsgame* mg);
void mg_missions_input(struct marsgame* mg);
void mg_missions_tick(struct marsgame* mg);
void mg_missions_draw(struct marsgame* mg, float dt);

/********************************************/
/*  UI functions    (ui/mission_screen.c)   */
/********************************************/
void mg_missions_build_ui(struct pg_ui_context* ctx, struct mg_missions_state* m);
void mg_mission_selector_fill(struct pg_ui_context* ctx, struct mg_missions_state* m);
void mg_mission_selector_set(struct mg_missions_state* m, int mission_idx);

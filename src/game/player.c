#include "procgl/procgl.h"
#include "marsgame.h"

MG_ENTITY_TICK(mg_player_tick);
MG_ENTITY_DRAW(mg_player_draw);

static const struct mg_entity_base MG_PLAYER_BASE = {
    .name = "Player", .max_HP = 100, .bound = { {{ -1, -1, -1 }}, {{ 1, 1, 1 }} },
    .collider_offset = {{ 0, 0, 0 }},
    .collider = { .type = MG_COLLIDER_SPHERE_FRICTION, .radius = 1,
        .flags = MG_COLLIDER_DEBUGGED },
    .tick = mg_player_tick, .draw = mg_player_draw };
    
mg_entpool_id_t mg_new_PC(struct mg_entity** ent_ptr)
{
    mg_entpool_id_t weap = mg_new_item(MG_ITEM_WEAPON_PISTOL, NULL);
    struct mg_entity* ent;
    mg_entpool_id_t id = mg_entpool_alloc(1, &ent);
    mg_entity_init_from_base(ent, &MG_PLAYER_BASE);
    mg_entity_set_pos(ent, vec3(72, 20, 4));
    ent->type = MG_ENTITY_ENEMY;
    struct mg_player_data* plr_data = mg_player_data(ent);
    plr_data->held_item = weap;
    if(ent_ptr) *ent_ptr = ent;
    return id;

}

static void mg_player_input_pickup(struct mg_battle_state* b, struct mg_entity* plr)
{
    struct mg_space_ray_hit item_q = mg_battle_space_raycast(&b->space,
                    b->view_pos, vec3_scale(b->view_dir, 4), MG_ENTITY_ITEM);
    if(item_q.hit == -1) return;
    struct mg_entity* item = mg_entpool_get(item_q.hit);
    mg_entity_interact(b, item, plr);
}

void use_held_item(struct mg_battle_state* b, struct mg_entity* plr)
{
    struct mg_player_data* plr_data = mg_player_data(plr);
    mg_entpool_id_t held_item = plr_data->held_item;
    struct mg_entity* held_ent = mg_entpool_get(held_item);
    if(!held_ent || !held_ent->use) return;
    held_ent->use(b, held_ent, plr);
}

void mg_player_input(struct mg_battle_state* b, struct mg_entity* plr)
{
    /********************/
    /*  Player inputs   */
    /********************/
    vec3 plr_push = vec3(0);
    /*  Pick up items   */
    if(pg_check_input(SDL_SCANCODE_E, PG_CONTROL_HIT)) mg_player_input_pickup(b, plr);
    /*  Attack          */
    if(pg_check_input(PG_LEFT_MOUSE, PG_CONTROL_HIT)) {
        use_held_item(b, plr);
    }
    /*  Jump    */
    if(pg_check_input(SDL_SCANCODE_SPACE, PG_CONTROL_HIT) && plr->ground) {
        plr->ground = 0;
        plr_push.z = 0.25;
    }
    /*  Debug particles */
    if(pg_check_input(SDL_SCANCODE_F, PG_CONTROL_HIT)) {
        struct mg_particle new_part = {
            .pos = vec3(VEC_XY(plr->pos), plr->pos.z + 1.5), .vel = vec3(0),
            .type = MG_PARTICLE_TEST };
        mg_particles_add(&b->mg->particle_system, &new_part);
    }
    /*  Movement    */
    vec2 move_dir = vec2(0);
    int keys_held[4] = {
        pg_check_input(SDL_SCANCODE_A, PG_CONTROL_HELD),
        pg_check_input(SDL_SCANCODE_D, PG_CONTROL_HELD),
        pg_check_input(SDL_SCANCODE_W, PG_CONTROL_HELD),
        pg_check_input(SDL_SCANCODE_S, PG_CONTROL_HELD) };
    if(keys_held[0]) move_dir.x = -1;
    else if(keys_held[1]) move_dir.x = 1;
    if(keys_held[2]) move_dir.y = 1;
    else if(keys_held[3]) move_dir.y = -1;
    if(!vec2_is_zero(move_dir)) {
        if((plr->collider.flags & MG_COLLIDER_ON_SURFACE)) {
            move_dir = vec2_rotate(vec2_tolen(move_dir, 0.02), b->view_angle.x);
        } else {
            move_dir = vec2_rotate(vec2_tolen(move_dir, 0.001), b->view_angle.x);
        }
        plr_push = vec3_add(vec3(VEC_XY(move_dir)), plr_push);
        mg_entity_push(plr, plr_push);
    }
    /*  Finish mission  */
    if(mg_battle_can_finish(b) && pg_check_input(SDL_SCANCODE_E, PG_CONTROL_HIT)) {
        pg_ui_t results_pane = pg_ui_child(b->ui_context, b->ui, "results");
        b->run_simulation = 0;
        pg_mouse_mode(0);
        if(results_pane >= 0) pg_ui_enabled(b->ui_context, results_pane, 1);
    }
}

MG_ENTITY_TICK(mg_player_tick)
{
    /************************/
    /*  Regular player tick */
    /************************/
    ent->rot_euler = b->view_angle;
    ent->rot = quat_from_sph(VEC_XY(b->view_angle));
    mg_player_input(b, ent);
}

MG_ENTITY_DRAW(mg_player_draw)
{
    struct mg_battle_state* b = &mg->battle;
    vec3 ent_pos = vec3(POS_LERP3(ent, dt));
    //ent_pos = ent->pos;
    ent_pos.z -= 1;
    float anim_lerp = LM_FMOD((float)b->sim_tick + dt, 60);
    float anim_frame = floor(anim_lerp / 15.0f);
    float frame_lerp = LM_FMOD((float)b->sim_tick, 15) / 15;
    int frame0 = anim_frame + 1;
    int frame1 = (int)(anim_frame + 1) % 4 + 1;
    pg_boxbatch_add_model(&mg->boxbatch, PG_EZDRAW_BOX_MODEL(&mg->human_model,
        .rig = &mg->human_rig, .pose = { frame0, frame1 }, .pose_lerp = frame_lerp,
        .orientation = quat_rotation(vec3_Z(), b->view_angle.x + M_PI), .scale = 0.5,
        .pos = ent_pos ));
}

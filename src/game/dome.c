#include "procgl/procgl.h"
#include "marsgame.h"

PG_MEMPOOL_DEFINE(struct mg_homebase_dome, mg_homebase_dome, alloc);

static struct mg_homebase_dome_info DOME_INFO[] = {
    { L"BARE REGOLITH", "empty",
        .func_buttons = { L"BUILD" },
        .func_ui = { ui_build_dome },
        .size = 5 },
    { L"CENTRAL DOME", "central",
        .func_buttons = { L"UPGRADE", L"MISSIONS", L"BULLETIN" },
        .func_ui = { ui_upgrade_dome, ui_active_missions, ui_bulletin_board },
        .color = {{200,200,225,255}}, .size = 7.5, },
    { L"HABITATION DOME", "hab",
        .materials_cost = 50, .can_build = 1,
        .func_buttons = { L"UPGRADE", L"PEOPLE", L"BULLETIN" },
        .func_ui = { ui_upgrade_dome, ui_recruiting, ui_bulletin_board },
        .color = {{150,150,200,255}}, .size = 5, },
    { L"GREENHOUSE", "farm",
        .materials_cost = 100, .can_build = 1,
        .func_buttons = { L"UPGRADE", L"", L"BULLETIN" },
        .func_ui = { ui_upgrade_dome, NULL, ui_bulletin_board },
        .color = {{150,150,200,255}}, .size = 5, },
    { L"GARAGE", "garage",
        .materials_cost = 100, .can_build = 1,
        .func_buttons = { L"UPGRADE", L"MANAGE", L"BULLETIN" },
        .func_ui = { ui_upgrade_dome, NULL, ui_bulletin_board },
        .color = {{150,150,200,255}}, .size = 5, },
    { L"COMMS STATION", "comms",
        .materials_cost = 75, .can_build = 1,
        .func_buttons = { L"UPGRADE", L"", L"BULLETIN" },
        .func_ui = { ui_upgrade_dome, NULL, ui_bulletin_board },
        .color = {{150,150,200,255}}, .size = 5, },
    { L"LABORATORY", "lab",
        .materials_cost = 75, .can_build = 1,
        .func_buttons = { L"UPGRADE", L"RESEARCH", L"BULLETIN" },
        .func_ui = { ui_upgrade_dome, NULL, ui_bulletin_board },
        .color = {{150,150,200,255}}, .size = 5, },
    { L"MANUFACTORY", "mfg",
        .materials_cost = 150, .can_build = 1,
        .func_buttons = { L"UPGRADE", L"MFG.", L"BULLETIN" },
        .func_ui = { ui_upgrade_dome, NULL, ui_bulletin_board },
        .color = {{150,150,200,255}}, .size = 5, },
};

static const int num_dome_types = sizeof(DOME_INFO) / sizeof(DOME_INFO[0]);

int mg_num_dome_types(void) { return num_dome_types; }

const struct mg_homebase_dome_info* mg_homebase_dome_info(mg_homebase_dome_info_t base)
{
    return &DOME_INFO[base];
}

mg_homebase_dome_info_t mg_homebase_dome_info_by_name(struct mg_homebase_state* h, char* name)
{
    mg_homebase_dome_info_t info;
    HTABLE_GET_V(h->dome_names, name, info);
    return info;
}

void mg_homebase_init_domes(struct mg_homebase_state* h)
{
    printf("CONSTRUCTING DOME INFORMATION\n");
    int i, j;
    for(i = 0; i < num_dome_types; ++i) {
        struct mg_homebase_dome_info* dome = &DOME_INFO[i];
        HTABLE_SET(h->dome_names, dome->id_name, i);
    }
}

void mg_homebase_dome_init(struct mg_homebase_dome* dome, mg_homebase_dome_info_t type)
{
    struct mg_homebase_dome_info* info = &DOME_INFO[type];
    dome->type = type;
    dome->color = info->color;
    dome->size = info->size;
    ARR_INIT(dome->upgrades);
    ARR_INIT(dome->bulletins);
}

void mg_homebase_dome_deinit(struct mg_homebase_dome* dome)
{
    ARR_DEINIT(dome->upgrades);
    ARR_DEINIT(dome->bulletins);
}

int mg_homebase_have_dome_type(struct mg_homebase_state* h, mg_homebase_dome_info_t type)
{
    mg_homebase_dome_id_t dome_id;
    int i;
    ARR_FOREACH(h->domes, dome_id, i) {
        struct mg_homebase_dome* dome = mg_homebase_dome_get(&h->dome_pool, dome_id);
        if(dome->type == type) return 1;
    }
    return 0;
}

static int mg_homebase_dome_has_upgrade(struct mg_homebase_state* h,
                                        struct mg_homebase_dome* dome,
                                        mg_homebase_upgrade_info_t up_type)
{
    int i;
    for(i = 0; i < dome->upgrades.len; ++i) {
        struct mg_homebase_upgrade* up =
            mg_homebase_upgrade_get(&h->up_pool, dome->upgrades.data[i]);
        if(up->info != up_type) continue;
        return up->completed ? 2 : 1;
    }
    return 0;
}

static int mg_homebase_dome_has_bulletin(struct mg_homebase_state* h,
                                         struct mg_homebase_dome* dome,
                                         mg_homebase_bulletin_info_t blt_type)
{
    int i;
    for(i = 0; i < dome->upgrades.len; ++i) {
        if(mg_homebase_bulletin_get(&h->blt_pool,
                dome->bulletins.data[i])->info == blt_type) {
            return 1;
        }
    }
    return 0;
}

void mg_homebase_dome_update_upgrades(struct mg_homebase_state* h,
                                      struct mg_homebase_dome* dome)
{
    int num_upgrades = mg_num_upgrade_types();
    int i;
    for(i = 0; i < num_upgrades; ++i) {
        const struct mg_homebase_upgrade_info* up_info =
            mg_homebase_upgrade_info(i);
        if(up_info->dome_type != dome->type) continue;
        if(mg_homebase_dome_has_upgrade(h, dome, i)) continue;
        int dep;
        for(dep = 0; dep < 4; ++dep) {
            if(up_info->depends_id[dep] == -1
            || mg_homebase_have_upgrade(h, up_info->depends_id[dep]) == 2)
                continue;
            break;
        }
        if(dep < 4) continue;
        struct mg_homebase_upgrade* new_up;
        mg_homebase_upgrade_id_t new_up_id =
            mg_homebase_upgrade_alloc(&h->up_pool, 1, &new_up);
        new_up->info = i;
        new_up->completed = 0;
        ARR_PUSH(dome->upgrades, new_up_id);
    }
}

void mg_homebase_dome_update_bulletins(struct mg_homebase_state* h,
                                       struct mg_homebase_dome* dome)
{
    int num_bulletins = mg_num_bulletin_types();
    int i;
    /*  Go through all bulletins; if criteria are met, add to bulletin  */
    for(i = 0; i < num_bulletins; ++i) {
        const struct mg_homebase_bulletin_info* blt_info =
            mg_homebase_bulletin_info(i);
        if(blt_info->dome_type != dome->type) continue;
        int j;
        for(j = 0; j < dome->bulletins.len; ++j) {
            struct mg_homebase_bulletin* blt =
                mg_homebase_bulletin_get(&h->blt_pool, dome->bulletins.data[j]);
            if(blt->info == i) break;
        }
        if(j != dome->bulletins.len) continue;
        struct mg_homebase_bulletin* blt;
        mg_homebase_bulletin_id_t blt_id =
            mg_homebase_bulletin_alloc(&h->blt_pool, 1, &blt);
        blt->active = 0;
        blt->info = i;
        ARR_PUSH(dome->bulletins, blt_id);
    }
}

void mg_homebase_update_domes(struct mg_homebase_state* h)
{
    int dome_idx;
    for(dome_idx = 0; dome_idx < h->domes.len; ++dome_idx) {
        /*  Go through all upgrades, check if dome has all deps, add to avail   */
        struct mg_homebase_dome* dome =
            mg_homebase_dome_get(&h->dome_pool, h->domes.data[dome_idx]);
        if(dome->type == 0) continue;
        mg_homebase_dome_update_upgrades(h, dome);
        mg_homebase_dome_update_bulletins(h, dome);
    }
}

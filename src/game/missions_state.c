#include "procgl/procgl.h"
#include "marsgame.h"

/************************/
/*  Base functions      */
/************************/
void mg_init_missions(struct marsgame* mg)
{
    struct pg_ui_context* ctx = &mg->ui;
    struct mg_missions_state* m = &mg->mission;
    m->ui_context = ctx;
    m->marsgame = mg;
    mg_missions_build_ui(ctx, m);
    int i;
    for(i = 0; i < 8; ++i) {
        m->missions[i].blt = -1;
        m->missions[i].ppl = -1;
        m->missions[i].status = MG_MISSION_READY;
    };
    m->num_missions = 0;
}

void mg_deinit_missions(struct mg_missions_state* m)
{
}

void mg_start_missions(struct marsgame* mg)
{
    struct pg_ui_context* ctx = &mg->ui;
    struct mg_missions_state* m = &mg->mission;
    struct mg_homebase_state* h = &mg->homebase;
    if(mg->state == MG_HOMEBASE) {
        int n_msn = 0;
        int i;
        for(i = 0; i < h->active_missions.len; ++i) {
            mg_homebase_bulletin_id_t blt_id = h->active_missions.data[i];
            mg_homebase_person_id_t ppl_id =
                mg_homebase_bulletin_get_assignee(h, blt_id);
            if(ppl_id == -1) continue;
            m->missions[n_msn].blt = blt_id;
            m->missions[n_msn].ppl = ppl_id;
            m->missions[n_msn].status = MG_MISSION_READY;
            ++n_msn;
        }
        m->num_missions = n_msn;
        m->selected_mission = 0;
    }
    m->should_begin_mission = 0;
    pg_renderpass_viewer(&mg->world_pass, &m->worldview);
    pg_renderpass_viewer(&mg->box_pass, &m->worldview);
    pg_ui_enabled(ctx, m->ui, 1);
    mg_mission_selector_fill(ctx, m);
    mg_mission_selector_set(m, m->selected_mission);
}

void mg_end_missions(struct marsgame* mg)
{
    struct pg_ui_context* ctx = &mg->ui;
    struct mg_missions_state* m = &mg->mission;
    pg_ui_enabled(ctx, m->ui, 0);
}

void mg_missions_update(struct marsgame* mg)
{
    
}

void mg_missions_input(struct marsgame* mg)
{
}

void mg_missions_tick(struct marsgame* mg)
{
    struct mg_missions_state* m = &mg->mission;
    if(m->should_begin_mission) {
        mg_end_missions(mg);
        mg_start_battle(mg);
        mg->state = MG_BATTLE;
    }
}

static void draw_ground(struct marsgame* mg, float dt)
{
    /*  Draw the ground */
    pg_tex_frame_t frame = pg_texture_frame(&mg->sgtex, 1, 0);
    vec2 tile_size = vec2( 16, 16 );
    int i, j;
    for(i = 0; i < 16; ++i) {
        for(j = 0; j < 16; ++j) {
            pg_quadbatch_add_quad(&mg->quadbatch, PG_EZQUAD(
                .tex_frame = frame.frame, .tex_layer = 1,
                .pos = vec3( i * tile_size.x - tile_size.x * 8,
                             j * tile_size.y - tile_size.y * 8, 0 ),
                .scale = vec2( tile_size.x, tile_size.y ),
                .orientation = quat_identity(),
                .color_mod = 0xFFFFFFFF ));
        }
    }
}

void mg_missions_draw(struct marsgame* mg, float dt)
{
    struct mg_missions_state* m = &mg->mission;
    pg_quadbatch_reset(&mg->quadbatch);
    pg_boxbatch_reset(&mg->boxbatch);
    /*  3D stuff    */
    ++m->tick;
    /*  Calculate animation results */
    quat y_plane = quat_rotation(vec3_X(), LM_PI * -0.5);
    quat view_quat = quat_identity();
    view_quat = quat_mul(y_plane, view_quat);
    float move_pos = ((float)m->tick) * 0.25;
    vec3 view_offset = vec3(0, LM_FMOD(move_pos, 16), 3);
    pg_viewer_perspective(&m->worldview, view_offset, view_quat, mg->halfres, vec2(0.01, 100));

    mat4 tx = mat4_identity();
    pg_quadbatch_next(&mg->quadbatch, &tx);
    pg_boxbatch_next(&mg->boxbatch, &tx);

    /*  Draw the things */
    draw_ground(mg, dt);

    /*  Finish  */
    pg_quadbatch_draw(&mg->quadbatch, &mg->world_pass);
    pg_boxbatch_draw(&mg->boxbatch, &mg->box_pass);
    pg_quadbatch_buffer(&mg->quadbatch);
    pg_boxbatch_buffer(&mg->boxbatch);
}

/********************************/
/*  Game functions              */
/********************************/

void mg_missions_begin_play(struct mg_missions_state* m)
{
    int msn_idx = m->selected_mission;
    if(m->missions[msn_idx].status != MG_MISSION_READY) return;
    struct marsgame* mg = m->marsgame;
    struct mg_homebase_state* h = &mg->homebase;
    struct mg_battle_state* b = &mg->battle;
    mg_homebase_bulletin_id_t blt_id = m->missions[msn_idx].blt;
    mg_homebase_person_id_t ppl_id = m->missions[msn_idx].ppl;
    struct mg_homebase_bulletin* blt = mg_homebase_bulletin_get(&h->blt_pool, blt_id);
    struct mg_homebase_person* ppl = mg_homebase_person_get(&h->ppl_pool, ppl_id);
    
}







#include "procgl/procgl.h"
#include "marsgame.h"

void mg_battle_space_init(struct mg_battle_space* space)
{
    int bx, by;
    for(bx = 0; bx < MG_BATTLE_PARTITIONS_X; ++bx) {
        for(by = 0; by < MG_BATTLE_PARTITIONS_Y; ++by) {
            ARR_INIT(space->entities[0][bx][by]);
            ARR_INIT(space->entities[1][bx][by]);
        }
    }
    ARR_INIT(space->all_entities);
    ARR_INIT(space->query);
    ARR_INIT(space->collision_pairs[0]);
    ARR_INIT(space->collision_pairs[0]);
}

void mg_battle_space_deinit(struct mg_battle_space* space)
{
    int bx, by;
    for(bx = 0; bx < MG_BATTLE_PARTITIONS_X; ++bx) {
        for(by = 0; by < MG_BATTLE_PARTITIONS_Y; ++by) {
            ARR_DEINIT(space->entities[0][bx][by]);
            ARR_DEINIT(space->entities[1][bx][by]);
        }
    }
    ARR_DEINIT(space->all_entities);
    ARR_DEINIT(space->query);
    ARR_DEINIT(space->collision_pairs[0]);
    ARR_DEINIT(space->collision_pairs[0]);
}

void mg_battle_space_reset(struct mg_battle_space* space)
{
    int bx, by;
    for(bx = 0; bx < MG_BATTLE_PARTITIONS_X; ++bx) {
        for(by = 0; by < MG_BATTLE_PARTITIONS_Y; ++by) {
            ARR_TRUNCATE(space->entities[0][bx][by], 0);
            ARR_TRUNCATE(space->entities[1][bx][by], 0);
        }
    }
    ARR_TRUNCATE(space->all_entities, 0);
    ARR_TRUNCATE(space->query, 0);
    ARR_TRUNCATE(space->collision_pairs[0], 0);
    ARR_TRUNCATE(space->collision_pairs[0], 0);
}

void mg_battle_space_assign_entity(struct mg_battle_space* space, mg_entpool_id_t ent_id)
{
    struct mg_entity* ent = mg_entpool_get(ent_id);
    if(!ent || ent->pos.x < 0 || ent->pos.x > MG_BATTLE_SPACE_WIDTH
    || ent->pos.y < 0 || ent->pos.y > MG_BATTLE_SPACE_HEIGHT) return;
    vec2 bound[2] = {
        vec2(VEC_XY(ent->cur_bound[0])), vec2(VEC_XY(ent->cur_bound[1])) };
    int c[2] = { floor(bound[0].x / MG_BATTLE_PARTITION_WIDTH),
                 floor(bound[0].y / MG_BATTLE_PARTITION_HEIGHT) };
    int ce[2] = { floor(bound[1].x / MG_BATTLE_PARTITION_WIDTH),
                  floor(bound[1].y / MG_BATTLE_PARTITION_HEIGHT) };
    c[0] = LM_CLAMP(c[0], 0, MG_BATTLE_PARTITIONS_X - 1);
    c[1] = LM_CLAMP(c[1], 0, MG_BATTLE_PARTITIONS_Y - 1);
    ce[0] = LM_CLAMP(ce[0], 0, MG_BATTLE_PARTITIONS_X - 1);
    ce[1] = LM_CLAMP(ce[1], 0, MG_BATTLE_PARTITIONS_Y - 1);
    int bx, by, i;
    mg_entpool_id_t id;
    for(bx = c[0]; bx <= ce[0]; ++bx) for(by = c[1]; by <= ce[1]; ++by) {
        ARR_PUSH(space->entities[space->side][bx][by], ent_id);
    }
}

/*  Swap between the two spatial partition structures for next game tick    */
void mg_battle_space_swap(struct mg_battle_space* space)
{
    space->space_tick = 0;
    space->game_tick += 1;
    space->side = !space->side;
    int bx, by;
    for(bx = 0; bx < MG_BATTLE_PARTITIONS_X; ++bx) {
        for(by = 0; by < MG_BATTLE_PARTITIONS_Y; ++by) {
            ARR_TRUNCATE(space->entities[space->side][bx][by], 0);
        }
    }
}

/*  End the current query   */
void mg_battle_space_new_query(struct mg_battle_space* space)
{
    ARR_TRUNCATE(space->query, 0);
}


/********************************/
/*  QUERY CODE                  */
/********************************/

#define QUERY_BASE(Q0, Q1, ...) \
    int QUERY_START_[2] = { floor(Q0.x / MG_BATTLE_PARTITION_WIDTH), \
                            floor(Q0.y / MG_BATTLE_PARTITION_HEIGHT) }; \
    int QUERY_END_[2] = { floor(Q1.x / MG_BATTLE_PARTITION_WIDTH), \
                          floor(Q1.y / MG_BATTLE_PARTITION_HEIGHT) };\
    QUERY_START_[0] = LM_CLAMP(QUERY_START_[0], 0, MG_BATTLE_PARTITIONS_X - 1); \
    QUERY_START_[1] = LM_CLAMP(QUERY_START_[1], 0, MG_BATTLE_PARTITIONS_Y - 1); \
    QUERY_END_[0] = LM_CLAMP(QUERY_END_[0], 0, MG_BATTLE_PARTITIONS_X - 1); \
    QUERY_END_[1] = LM_CLAMP(QUERY_END_[1], 0, MG_BATTLE_PARTITIONS_Y - 1); \
    int QUERY_SIDE_ = !space->side; \
    int PART_X_, PART_Y_, PART_IDX_; \
    mg_entpool_id_t QUERY_ID; \
    struct mg_entity* QUERY_ENT; \
    for(PART_X_ = QUERY_START_[0]; PART_X_ <= QUERY_END_[0]; ++PART_X_) \
    for(PART_Y_ = QUERY_START_[1]; PART_Y_ <= QUERY_END_[1]; ++PART_Y_) { \
        ARR_FOREACH(space->entities[QUERY_SIDE_][PART_X_][PART_Y_], QUERY_ID, PART_IDX_) { \
            if(!(QUERY_ENT = mg_entpool_get(QUERY_ID))) continue; \
            __VA_ARGS__ \
        } \
    } \

int mg_battle_space_query_aabb(struct mg_battle_space* space,
                               vec3 q0, vec3 q1, uint32_t filter)
{ 
    ++space->query_no;
    int start_len = space->query.len;
    QUERY_BASE(q0, q1,
        if(QUERY_ENT->query != space->query_no
        && aabb_intersect(q0, q1, QUERY_ENT->cur_bound[0], QUERY_ENT->cur_bound[1])) {
            QUERY_ENT->query = space->query_no;
            ARR_PUSH(space->query, QUERY_ID);
        }
    );
    return space->query.len - start_len;
}

#define MAX_RAYCAST_DISTANCE    128.0f

/*  A raycast function suitable only for relatively short-range rays    */
struct mg_space_ray_hit mg_battle_space_raycast(
        struct mg_battle_space* space, vec3 src, vec3 dir, uint32_t filter)
{
    ++space->query_no;
    vec3 dir_norm = vec3_norm(dir);
    float dir_len = vec3_len(dir);
    mg_entpool_id_t nearest_id = -1;
    float nearest_dist = LM_MIN(dir_len, MAX_RAYCAST_DISTANCE);
    vec3 q_end = vec3_add(src, dir);
    vec3 q0 = vec3_min(src, q_end);
    vec3 q1 = vec3_max(src, q_end);
    QUERY_BASE(q0, q1,
        if(QUERY_ENT->query == space->query_no) continue;
        QUERY_ENT->query = space->query_no;
        float t = QUERY_ENT->raycast(QUERY_ENT, src, dir_norm);
        if(t >= 0 && t < nearest_dist) {
            nearest_dist = t;
            nearest_id = QUERY_ID;
        }
    );
    return (struct mg_space_ray_hit){ .hit = nearest_id, .distance = nearest_dist };
}


/********************************************/
/*  COLLISION HANDLING BETWEEN ENTITIES     */
/********************************************/

static int mg_battle_space_coll_query(struct mg_battle_space* space, struct mg_entity* ent);
static void update_entities(struct mg_battle_space* space);

/*  Perform one collision resolution pass   */
void mg_battle_space_resolve_collisions(struct mg_battle_space* space)
{
    int i, j;
    mg_entpool_id_t broad_ent_id;
    struct mg_entity* broad_ent;
    mg_entpool_id_t pair_ent_id;
    struct mg_entity* pair_ent;
    struct mg_collision coll;
    ARR_TRUNCATE(space->collision_pairs[0], 0);
    ARR_TRUNCATE(space->collision_pairs[1], 0);
    #ifdef MG_DEBUG_COLLISIONS
    space->n_debugged_pairs = 0;
    space->n_intersection_tests = 0;
    int n_potential_collisions = 0;
    int n_debugged_potential = 0;
    int n_debugged_confirmed = 0;
    int n_confirmed_collisions = 0;
    #endif
    /********************/
    /*  Broad phase     */
    #ifdef MG_DEBUG_COLLISIONS
    printf("#### Broad phase ##############################################\n\n");
    #endif
    ARR_FOREACH_REV(space->all_entities, broad_ent_id, i) {
        if(!(broad_ent = mg_entpool_get(broad_ent_id))) continue;
        if(broad_ent->dead) {
            mg_entpool_free(broad_ent_id);
            ARR_SWAPSPLICE(space->all_entities, i, 1);
            continue;
        }
        /*  Exclude from all collision logic for the rest of the tick,
            every potential collision with this entity is being found now   */
        broad_ent->collider.flags |= MG_COLLIDER_PROCESSED;
        /*  Query the spatial partition structure for all entities with
            bounds overlapping broad_ent's  */
        int query_len = mg_battle_space_coll_query(space, broad_ent);
        if(query_len > 0) {
            /*  Add all found overlaps to the end of collision_pairs[1],
                save length in broad_ent    */
            broad_ent->n_phys_pairs = query_len;
            ARR_PUSH(space->collision_pairs[0], broad_ent_id);
        }
    }
    #ifdef MG_DEBUG_COLLISIONS
    printf("|\n+---- Narrow phase (%d pairs, %d debugged) ---\n|\n\n",
        space->collision_pairs[1].len, space->n_debugged_pairs);
    #endif
    /*  Narrow phase: Resolve collisions of all pairs   */
    int pair_start = 0, pair_end;
    ARR_FOREACH(space->collision_pairs[0], broad_ent_id, i) {
        broad_ent = mg_entpool_get(broad_ent_id);
        pair_end = pair_start + broad_ent->n_phys_pairs;
        #ifdef MG_DEBUG_COLLISIONS
        if(broad_ent->n_debugged_pairs > 0) {
            printf(" %d/%d: %s<%d> has %d pairs, %d debugged\n",
                i, space->collision_pairs[0].len - 1, broad_ent->name, broad_ent_id,
                broad_ent->n_phys_pairs, broad_ent->n_debugged_pairs);
        }
        #endif
        for(j = pair_start; j < pair_end; ++j) {
            pair_ent_id = space->collision_pairs[1].data[j];
            pair_ent = mg_entpool_get(pair_ent_id);
            mg_entity_collide(&coll, broad_ent, pair_ent);
            #ifdef MG_DEBUG_COLLISIONS
            if(coll.did_collide) ++n_confirmed_collisions;
            if((pair_ent->collider.flags & MG_COLLIDER_DEBUGGED)) {
                ++n_debugged_potential;
                if(coll.did_collide) {
                    ++n_debugged_confirmed;
                    printf("    %d/%d: %s<%d>%s\n",
                        j - pair_start, broad_ent->n_phys_pairs,
                        pair_ent->name, pair_ent_id,
                        coll.did_collide ? " <COLLISION>" : "");
                    mg_debug_print_collision(&coll,
                        &broad_ent->collider, broad_ent->name,
                        &pair_ent->collider, pair_ent->name);
                }
            }
            #endif
        }
        pair_start = pair_end;
    }
    update_entities(space);
    #ifdef MG_DEBUG_COLLISIONS
    int nck = factorial(space->all_entities.len)
        / (2 * factorial(space->all_entities.len - 2));
    float nck_improve_percent = ((float)pair_end / (float)nck) * 100.0f;
    printf("\n|\n"
           "| ^^^  %lu.%lu  ^^^  COLLISION RESOLUTION FRAME  ^^^\n"
           "|     Entities: %d, debugged %d | nC2: %d (%d, %f%%)\n"
           "|     Broad:  Intersection tests: %d\n"
           "|             Potential collisions: %d, debugged %d\n"
           "|     Narrow: Confirmed collisions: %d, debugged %d\n"
           "###############################################################\n\n",
           space->game_tick, space->space_tick,
           space->all_entities.len, space->n_debugged_ents, nck, pair_end, nck_improve_percent,
           space->n_intersection_tests, n_potential_collisions, n_debugged_potential,
           n_confirmed_collisions, n_debugged_confirmed);
    #endif

    ++space->space_tick;
}

/*  Special query function for collision broad-phase    */
static int mg_battle_space_coll_query(struct mg_battle_space* space, struct mg_entity* ent)
{
    #ifdef MG_DEBUG_COLLISIONS
    int query_debugged = 0;
    int arg_debugged = ent->collider.flags & MG_COLLIDER_DEBUGGED;
    if(arg_debugged) ++space->n_debugged_ents;
    int n_debugged = 0;
    int n_debugged_pairs = 0;
    int n_ix_tests = 0;
    #endif
    int arg_static = 0;
    int query_static = 0;
    vec3 q0 = ent->cur_bound[0];
    vec3 q1 = ent->cur_bound[1];
    int start_len = space->collision_pairs[1].len;
    int ent_static = ent->collider.flags & MG_COLLIDER_STATIC;
    ++space->query_no;
    QUERY_BASE(q0, q1,  
        query_static = (QUERY_ENT->collider.flags & MG_COLLIDER_STATIC);
        #ifdef MG_DEBUG_COLLISIONS
        query_debugged = (QUERY_ENT->collider.flags & MG_COLLIDER_DEBUGGED);
        if((QUERY_ENT->collider.flags & MG_COLLIDER_PROCESSED)) {
            if(query_debugged || arg_debugged) {
                printf(" %s%s<%d>: Already fully tested, not re-testing.\n",
                        (query_debugged ? "!" : ""), QUERY_ENT->name, QUERY_ID);
            }
            continue;
        }
        if(QUERY_ENT->query == space->query_no) {
            if(query_debugged || arg_debugged) {
                printf(" %s%s<%d>: Re-checked by spatial partition, not re-testing.\n",
                        (query_debugged ? "!" : ""), QUERY_ENT->name, QUERY_ID);
            }
            continue;
        }
        QUERY_ENT->query = space->query_no;
        if(!QUERY_ENT->collider.type || (query_static && ent_static)) {
            if(query_debugged || arg_debugged) {
                printf(" %s%s<%d>: Useless collision check between %s%s and %s%s\n",
                    (query_debugged ? "!" : ""), QUERY_ENT->name, QUERY_ID,
                    MG_COLLIDER_NAME[ent->collider.type], (arg_static ? "(Static)" : ""),
                    MG_COLLIDER_NAME[QUERY_ENT->collider.type], (query_static ? "(Static)" : ""));
            }
            continue;
        }
        ++n_ix_tests;
        #else
        if((QUERY_ENT->collider.flags & MG_COLLIDER_PROCESSED)
        || (QUERY_ENT->query == space->query_no)
        || !QUERY_ENT->collider.type || query_static && arg_static)
            continue;
        #endif
        int ix = aabb_intersect(q0, q1, QUERY_ENT->cur_bound[0], QUERY_ENT->cur_bound[1]);
        if(ix) ARR_PUSH(space->collision_pairs[1], QUERY_ID);
        #ifdef MG_DEBUG_COLLISIONS
        if(arg_debugged || query_debugged) {
            if(ix) ++n_debugged_pairs;
            ++n_debugged;
            printf("  %s %s%s<%d>    [%f, %f, %f] [%f, %f, %f]\n",
                (!ix ? " " : "%"), (query_debugged ? "!" : ""), QUERY_ENT->name, QUERY_ID,
                VEC_XYZ(QUERY_ENT->cur_bound[0]), VEC_XYZ(QUERY_ENT->cur_bound[1]));
        }
        #endif
    )
    space->n_intersection_tests += n_ix_tests;
    ent->n_debugged_pairs = n_debugged;
    space->n_debugged_pairs += n_debugged_pairs;
    #ifdef MG_DEBUG_COLLISIONS
    if(n_debugged > 0) {
        printf("  \\==> %s%s<%d>    [%f, %f, %f] [%f, %f, %f]\n\n",
            (arg_debugged ? "!" : ""), ent->name, mg_entpool_id(ent),
            VEC_XYZ(ent->cur_bound[0]), VEC_XYZ(ent->cur_bound[1]));
    }
    #endif
    return space->collision_pairs[1].len - start_len;
}

/*  Apply all entities' collider results to their game state, reset colliders   */
static void update_entities(struct mg_battle_space* space)
{
    int i;
    mg_entpool_id_t ent_id;
    struct mg_entity* ent;
    #ifdef MG_DEBUG_COLLISIONS
    printf("\n|\n+---- Update ----\n|\n\n");
    #endif
    ARR_FOREACH(space->all_entities, ent_id, i) {
        if(!(ent = mg_entpool_get(ent_id))) continue;
        #ifdef MG_DEBUG_COLLISIONS
        vec3 old_pos = ent->pos;
        vec3 old_vel = ent->vel;
        #endif
        if(ent->collider.n_pushes != 0) {
            /*  Update the entity state */
            mg_collider_finalize(&ent->collider);
            mg_entity_set_pos(ent, vec3_add(ent->pos, ent->collider.push));
            ent->vel = vec3_add(ent->vel, mg_collider_slide(&ent->collider));
            /*  Reset the collider  */
            ent->collider.push = vec3(0);
            ent->collider.pos = vec3_add(ent->pos, ent->collider_offset);
            ent->collider.vel = ent->vel;
            ent->collider.n_pushes = 0;
            ent->collider.flags &= ~MG_COLLIDER_PROCESSED;
        }
        #ifdef MG_DEBUG_COLLISIONS
        if((ent->collider.flags & MG_COLLIDER_DEBUGGED)) {
            printf(" !%s<%d> : %d influencers\n"
                   "   Position (%f, %f, %f) -> (%f, %f, %f)\n"
                   "   Velocity (%f, %f, %f) -> Slide(%f, %f, %f)\n",
                   ent->name, ent_id, ent->collider.n_pushes,
                   VEC_XYZ(old_pos), VEC_XYZ(ent->pos),
                   VEC_XYZ(old_vel), VEC_XYZ(ent->vel));
        }
        #endif
    }
}



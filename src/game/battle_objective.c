#include "procgl/procgl.h"
#include "marsgame.h"

int mg_battle_objective_status(struct mg_battle_objective* objective)
{
    int i;
    struct mg_entity* ent;
    mg_entpool_id_t ent_id;
    if(objective->kill_enemies.len == 0) return 1;
    ARR_FOREACH(objective->kill_enemies, ent_id, i) {
        ent = mg_entpool_get(ent_id);
        /*  This shouldn't happen, so just give a success for the bug   */
        if(!ent) return 1;
        if(!ent->dead) return 0;
    }
    return 1;
}

int mg_battle_can_finish(struct mg_battle_state* b)
{
    vec3 plr_pos = mg_entpool_get(b->plr)->pos;
    if(!mg_battle_objective_status(&b->objective)) return 0;
    if(vec3_dist2(plr_pos, b->vehicle_pos) > 3 * 3) return 0;
    return 1;
}

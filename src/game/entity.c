#include "procgl/procgl.h"
#include "marsgame.h"

PG_MEMPOOL_DEFINE_GLOBAL(struct mg_entity, mg_entpool, alloc);

void mg_entity_init_from_base(struct mg_entity* ent, const struct mg_entity_base* base)
{
    strncpy(ent->name, base->name, 32);
    ent->HP = base->max_HP;
    ent->max_HP = base->max_HP;
    ent->bound[0] = base->bound[0];
    ent->bound[1] = base->bound[1];
    ent->collider_offset = base->collider_offset;
    ent->collider = base->collider;
    ent->rot = quat_identity();
    ent->tick = base->tick;
    ent->draw = base->draw;
    ent->raycast = base->raycast;
    ent->interact = base->interact;
    ent->use = base->use;
}

void mg_entity_set_pos(struct mg_entity* ent, vec3 pos)
{
    ent->pos = pos;
    ent->cur_bound[0] = vec3_add(ent->bound[0], ent->pos);
    ent->cur_bound[1] = vec3_add(ent->bound[1], ent->pos);
}

void mg_entity_set_bound_centered(struct mg_entity* ent, float size)
{
    ent->bound[0] = vec3(-size, -size, -size);
    ent->bound[1] = vec3(size, size, size);
}

void mg_entities_tick(struct mg_battle_state* b)
{
    mg_entpool_id_t ent_id;
    struct mg_entity* ent;
    int i;
    #ifdef MG_DEBUG_ENTITIES
    printf("\n\nENTITY TICK\n");
    #endif
    ARR_FOREACH_REV(b->space.all_entities, ent_id, i) {
        if(!(ent = mg_entpool_get(ent_id)) || ent->dead
        || ent->last_tick == b->sim_tick) continue;
        ent->last_tick = b->sim_tick;
        /*  Update the entity by its own logic  */
        if((ent->collider.flags & MG_COLLIDER_ON_SURFACE)) ent->ground = 6;
        if(!(ent->collider.flags & MG_COLLIDER_STATIC)) ent->vel.z -= 0.01;
        if(ent->tick) ent->tick(b, ent);
        if(ent->ground > 0) {
            if(ent->ground == 6) ent->vel = vec3_mul(ent->vel, vec3(0.8, 0.8, 1));
            --ent->ground;
        };
        /*  Update game logic driven impulses   */
        ent->vel = vec3_add(ent->vel, ent->push[ent->last_tick & 1]);
        ent->pos = vec3_add(ent->pos, ent->move[ent->last_tick & 1]);
        ent->push[ent->last_tick & 1] = vec3(0);
        ent->move[ent->last_tick & 1] = vec3(0);
        mg_entity_set_pos(ent, vec3_add(ent->pos, ent->vel));
        /*  Update the entity's collider for the collision check at the end of
            this frame  */
        ent->collider.flags &= ~(MG_COLLIDER_ON_SURFACE | MG_COLLIDER_PROCESSED);
        ent->collider.push = vec3(0);
        ent->collider.pos = vec3_add(ent->pos, ent->collider_offset);
        ent->collider.vel = ent->vel;
        ent->collider.n_pushes = 0;
        /*  Add entity to the spatial partition for this frame  */
        mg_battle_space_assign_entity(&b->space, ent_id);
    }
}

void mg_entities_draw(struct marsgame* mg, float dt)
{
    struct mg_battle_state* b = &mg->battle;
    mg_entpool_id_t ent_id;
    struct mg_entity* ent;
    int i;
    ARR_FOREACH_REV(b->space.all_entities, ent_id, i) {
        ent = mg_entpool_get(ent_id);
        if(!ent || ent->dead) continue;
        if(ent->draw) ent->draw(mg, ent, dt);
    }
}


void mg_entity_interact(struct mg_battle_state* b, struct mg_entity* target,
                        struct mg_entity* actor)
{
    if(target->interact) target->interact(b, target, actor);
}

/********************************/
/*  Physics                     */
/********************************/

void mg_entity_push(struct mg_entity* ent, vec3 vel)
{
    vec3* start = &ent->push[ent->last_tick & 1];
    *start = vec3_add(*start, vel);
}

void mg_entity_move(struct mg_entity* ent, vec3 pos)
{
    vec3* start = &ent->move[ent->last_tick & 1];
    *start = vec3_add(*start, pos);
}

/*  Resolve a collision between two entities    */
void mg_entity_collide(struct mg_collision* out, struct mg_entity* ent_a, struct mg_entity* ent_b)
{
    mg_collider_resolve(out, &ent_a->collider, &ent_b->collider);
}

#include "procgl/procgl.h"
#include "marsgame.h"

void mg_bullets_draw(struct marsgame* mg, quat view_inv, float dt)
{
    struct mg_battle_state* b = &mg->battle;
    struct mg_bullet* blt;
    int i;
    pg_tex_frame_t frame = pg_texture_frame(&mg->sgtex, 0, '+' - 32);
    ARR_FOREACH_PTR_REV(b->bullets, blt, i) {
        vec3 pos_lerp = vec3(POS_LERP3(blt, dt));
        frame = pg_texture_frame(&mg->sgtex, 0, blt->frame);
        pg_quadbatch_add_quad(&mg->quadbatch, PG_EZQUAD(
            .tex_frame = frame.frame, .tex_layer = 0,
            .pos = pos_lerp, .scale = blt->scale, .orientation = view_inv,
            .color_mod = blt->color, .color_add = 0, ));
    }
}

void mg_bullets_update(struct mg_battle_state* b)
{
    struct mg_bullet* blt;
    int i;
    ARR_FOREACH_PTR_REV(b->bullets, blt, i) {
        /*  Try to hit enemies  */
        struct mg_space_ray_hit enemy_q = mg_battle_space_raycast(&b->space,
                                    blt->pos, blt->vel, MG_ENTITY_ENEMY | MG_ENTITY_ENVOBJ);
        if(enemy_q.hit != -1) {
            struct mg_entity* hit_ent = mg_entpool_get(enemy_q.hit);
            blt->dead = 1;
            hit_ent->HP -= 10;
            hit_ent->vel = vec3_add(vec3_tolen(blt->vel, 0.75), hit_ent->vel);
        }
        /*  Handle bullets too far from player, or dead otherwise   */
        if(vec3_dist2(blt->pos, vec3(VEC_XY(b->view_pos), 0)) > (32 * 32)) blt->dead = 1;
        if(blt->dead) ARR_SWAPSPLICE(b->bullets, i, 1);
        else blt->pos = vec3_add(blt->pos, blt->vel);
    }
}


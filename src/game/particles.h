/*  Particle types  */
enum mg_particle_type {
    MG_PARTICLE_WIND0,
    MG_PARTICLE_WIND1,
    MG_PARTICLE_TEST,
    MG_NUM_PARTICLE_TYPES,
};

struct mg_particle_phase {
    vec4 tex_frame;
    vec4 color_mod, color_add;
    vec2 scale;
};

struct mg_particle_info {
    int n_phases, lifetime;
    struct mg_particle_phase phases[8];
};

struct mg_particle_info PARTICLE_TYPES[MG_NUM_PARTICLE_TYPES];

/*  Running particle data   */
struct mg_particle {
    vec3 pos, vel;
    quat rot;
    enum mg_particle_type type;
    int tick;
};

struct mg_particle_system {
    ARR_T(struct mg_particle) particles;
    struct pg_renderpass particles_pass;
    quat billboard_rot;
    vec3 viewer_pos;
    vec3 viewer_dir;
};

void mg_particles_init(struct marsgame* mg);
void mg_particles_tick(struct mg_particle_system* parts);
void mg_particles_draw(struct marsgame* mg, float dt);

void mg_particles_add(struct mg_particle_system* parts, struct mg_particle* new_part);


#define MG_BATTLE_TICKRATE  60

struct mg_battle_objective {
    mg_entity_arr_t kill_enemies;
};

struct mg_battle_state {
    struct marsgame* mg;
    /*  Memory management   */
    mg_entpool_id_t plr;
    struct mg_battle_space space;
    ARR_T(struct mg_bullet) bullets;
    float tiles[MG_BATTLE_PARTITIONS_X][MG_BATTLE_PARTITIONS_Y];
    /*  UI  */
    struct pg_ui_context* ui_context;
    pg_ui_t ui;
    struct pg_text_form hud_text;
    /*  Input stuff */
    vec2 mouse_motion;
    vec2 mouse_pos;
    /*  Squad position, view info   */
    struct pg_viewer worldview;
    vec2 view_angle;
    vec3 view_pos;
    vec3 view_dir;
    float last_dt;
    /*  Game state  */
    int mission_idx;
    int science, materials;
    int should_end_mission;
    vec3 vehicle_pos;
    struct mg_battle_objective objective;
    int enemies_alive;
    int tick, sim_tick;
    int paused;
    int run_simulation;
    struct pg_ezbox_model test_model;
    struct pg_ezbox_rig test_rig;
};

/********************************/
/*  Battle functions (battle.c) */
/********************************/
void mg_init_battle(struct marsgame* mg);
void mg_deinit_battle(struct mg_battle_state* b);
void mg_start_battle(struct marsgame* mg);
void mg_battle_update(struct marsgame* mg);
void mg_battle_input(struct marsgame* mg);
void mg_battle_tick(struct marsgame* mg);
void mg_battle_draw(struct marsgame* mg, float dt);


/********************************/
/*  Battle UI (battle_ui.c)     */
/********************************/
pg_ui_t mg_battle_build_ui(struct pg_ui_context* ui);

/************************************************/
/*  Objective functions (battle_objective.c)    */
/************************************************/
int mg_battle_objective_status(struct mg_battle_objective* objective);
int mg_battle_can_finish(struct mg_battle_state* b);

/********************************/
/*  Player functions (player.c) */
/********************************/
void mg_player_input(struct mg_battle_state* b, struct mg_entity* plr);
void mg_player_tick(struct mg_battle_state* b, struct mg_entity* plr);
void mg_player_draw(struct marsgame* mg, struct mg_entity* plr, float dt);

/********************************/
/*  Enemy functions (enemy.c)   */
/********************************/
void mg_enemies_tick(struct mg_battle_state* b);
void mg_enemies_draw(struct marsgame* mg, quat view_inv, float dt);

/********************************/
/*  Item functions (item.c)     */
/********************************/
void mg_items_draw(struct marsgame* mg, float dt);
void mg_items_tick(struct mg_battle_state* b);

void mg_envobjs_draw(struct marsgame* mg, float dt);
void mg_envobjs_tick(struct mg_battle_state* b);

/********************************/
/*  Bullet functions (bullet.c) */
/********************************/
void mg_bullets_draw(struct marsgame* mg, quat view_inv, float dt);
void mg_bullets_update(struct mg_battle_state* d);

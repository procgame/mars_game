#include "procgl/procgl.h"
#include "marsgame.h"

struct mg_particle_info PARTICLE_TYPES[MG_NUM_PARTICLE_TYPES] = {
    [MG_PARTICLE_WIND0] = { .lifetime = 120, .n_phases = 1, .phases = {
        {   .tex_frame = {{ 0, 0, 1, 1 }},
            .color_mod = {{ 0 }}, .color_add = {{ 0.6, 0.4, 0.04, 0.5 }},
            .scale = {{ 2, 8 }} }, }, },
    [MG_PARTICLE_WIND1] = { .lifetime = 120, .n_phases = 1, .phases = {
        {   .tex_frame = {{ 0, 0, 1, 1 }},
            .color_mod = {{ 0 }}, .color_add = {{ 0.9, 0.6, 0.06, 0.1 }},
            .scale = {{ 5, 16 }} }, }, },
    [MG_PARTICLE_TEST] = { .lifetime = 120, .n_phases = 3, .phases = {
        {   .color_mod = {{ 0 }}, .color_add = {{ 1,0,0,1 }},
            .scale = {{ 1,1 }} },
        {   .color_mod = {{ 0 }}, .color_add = {{ 0,1,0,1 }},
            .scale = {{ 2,2 }} },
        {   .color_mod = {{ 0 }}, .color_add = {{ 0,0,1,1 }},
            .scale = {{ 3,3 }} },
        {   .color_mod = {{ 0 }}, .color_add = {{ 0,0,1,0 }},
            .scale = {{ 0,0 }} }, }, },
};


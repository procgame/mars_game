#include "procgl/procgl.h"
#include "marsgame.h"

static int ui_objective_result_update(struct pg_ui_context* ctx, pg_ui_t elem, struct pg_ui_event* event)
{
    struct marsgame* mg = (struct marsgame*)
        pg_ui_variable(ctx, ctx->root, "marsgame")->ptr[0];
    struct mg_battle_state* b = &mg->battle;
    if(mg_battle_objective_status(&b->objective)) {
        pg_ui_set_text(ctx, elem, L"OBJECTIVE: COMPLETE", 32);
    } else {
        pg_ui_set_text(ctx, elem, L"OBJECTIVE: INCOMPLETE", 32);
    }
    return 0;
}

static int return_btn_click(struct pg_ui_context* ctx, pg_ui_t elem, struct pg_ui_event* event)
{
    struct marsgame* mg = (struct marsgame*)
        pg_ui_variable(ctx, ctx->root, "marsgame")->ptr[0];
    struct mg_battle_state* b = &mg->battle;
    b->should_end_mission = 1;
    return 1;
}

static pg_ui_t build_debrief_ui(struct pg_ui_context* ctx,
                                          pg_ui_t battle_ui)
{
    float ar = 1 / pg_ui_get_aspect(ctx);
    pg_ui_t results_pane = pg_ui_add_group(ctx, battle_ui, "results",
        PG_UI_PROPERTIES( .enabled = 0, .pos = vec2(0, 0), .scale = vec2(1, 1) ));
    pg_ui_t results_back = pg_ui_add_elem(ctx, results_pane, "background",
        PG_UI_PROPERTIES( .pos = vec2(0, 0), .layer = -1, .draw = PG_UI_IMG_ONLY,
            .img_scale = vec2(1.5 * ar, 1.5),
            .img_color_mod = vec4(0,0,0,0),
            .img_color_add = vec4(0,0,0,0.75), ));
    pg_ui_t results_header = pg_ui_add_elem(ctx, results_pane, "header",
        PG_UI_PROPERTIES( .pos = vec2(0, 0.6), .draw = PG_UI_TEXT_ONLY,
            .text_formatter = &PG_TEXT_FORMATTER_MOD(ctx->default_text_formatter,
                .align = PG_TEXT_CENTER, .size = vec2(0.1,0.1) ),
            .text = L"%F1MISSION DEBRIEF", ));
    pg_ui_t finish_btn = pg_ui_add_elem(ctx, results_pane, "finish_btn",
        PG_UI_PROPERTIES( .pos = vec2(0, -0.6), .draw = PG_UI_TEXT_ONLY,
            .text_formatter = &PG_TEXT_FORMATTER_MOD(ctx->default_text_formatter,
                .align = PG_TEXT_CENTER, .size = vec2(0.15,0.15) ),
            .text = L"RETURN TO BASE",
            .action_item = PG_UI_ACTION_TEXT,
            .cb_click = return_btn_click ));
    return results_pane;
}


pg_ui_t mg_battle_build_ui(struct pg_ui_context* ctx)
{
    pg_ui_t root = ctx->root;
    pg_ui_t battle_ui = pg_ui_add_group(ctx, root, "battle",
        PG_UI_PROPERTIES( .pos = vec2(0, 0), .scale = vec2(1, 1) ));
    pg_ui_t results_pane = build_debrief_ui(ctx, battle_ui);
    return battle_ui;
}


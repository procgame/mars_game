#include "procgl/procgl.h"
#include "marsgame.h"

/************************/
/*  ITEM DATA           */
/************************/

/*  Forward declarations (defined below)    */
MG_ENTITY_DRAW(mg_item_draw_pickup);
MG_ENTITY_INTERACT(mg_item_interact_pickup);
MG_ENTITY_USE(mg_item_use_gun_basic);

/*  Base entity behavior    */
const struct mg_entity_base MG_ITEM_BASE[MG_NUM_ITEM_TYPES] = {
    [MG_ITEM_WEAPON_PISTOL] = { .name = "pistol",
        .use = mg_item_use_gun_basic, },
    [MG_ITEM_WEAPON_MACHINEGUN] = { .name = "machine gun",
        .use = mg_item_use_gun_basic, },
    [MG_ITEM_WEAPON_PLAZGUN] = { .name = "plaz-gun",
        .use = mg_item_use_gun_basic, },
    [MG_ITEM_UTILITY_MEDKIT] = { .name = "medkit",
        .use = NULL, },
    [MG_ITEM_PICKUP_MATERIAL] = { .name = "raw material",
        .bound = 1,
        .draw = mg_item_draw_pickup,
        .interact = mg_item_interact_pickup,
        .raycast = mg_entity_raycast_sphere, },
    [MG_ITEM_PICKUP_SCIENCE] = { .name = "science data",
        .bound = 1,
        .draw = mg_item_draw_pickup,
        .interact = mg_item_interact_pickup,
        .raycast = mg_entity_raycast_sphere, },
};

/*  Item-specific data  */
const struct mg_item_type_data {
    /*  Pickup info */
    ubvec4 color;
    /*  Weapon info */
    float fire_rate;
    uint32_t bullet_color, bullet_frame, bullet_damage;
    float bullet_size, bullet_speed;
} MG_ITEM_TYPE_DATA[] = {
    [MG_ITEM_WEAPON_PISTOL] = {
        .bullet_damage = 5, .fire_rate = 2, .bullet_speed = 1.0f,
        .bullet_frame = '.' - 32, .bullet_color = 0xFFFFFFFF,
        .bullet_size = 0.5 },
    [MG_ITEM_WEAPON_MACHINEGUN] = {
        .bullet_damage = 10, .fire_rate = 5, .bullet_speed = 1.25f,
        .bullet_frame = '+' - 32, .bullet_color = 0xFFFF70FF,
        .bullet_size = 0.5 },
    [MG_ITEM_WEAPON_PLAZGUN] = {
        .bullet_damage = 20, .fire_rate = 1, .bullet_speed = 0.5f,
        .bullet_frame = 'O' - 32, .bullet_color = 0xFF8080FF,
        .bullet_size = 0.75 },
    [MG_ITEM_UTILITY_MEDKIT] = {
        .color = {{64, 192, 64, 255}} },
    [MG_ITEM_PICKUP_MATERIAL] = {
        .color = {{64, 64, 64, 255}} },
    [MG_ITEM_PICKUP_SCIENCE] = {
        .color = {{64, 64, 192, 255}} },
};

/*  Item allocation */
mg_entpool_id_t mg_new_item(enum mg_item_type type, struct mg_entity** ent_ptr)
{
    struct mg_entity* ent;
    mg_entpool_id_t id = mg_entpool_alloc(1, &ent);
    mg_entity_init_from_base(ent, &MG_ITEM_BASE[type]);
    ent->type = MG_ENTITY_ITEM;
    ent->subtype = type;
    if(ent_ptr) *ent_ptr = ent;
    return id;
}

/********************************/
/*  Behavior functions          */
/********************************/

MG_ENTITY_DRAW(mg_item_draw_pickup)
{
    const struct mg_item_type_data* info = &MG_ITEM_TYPE_DATA[ent->subtype];
    vec3 pos = vec3( POS_LERP3(ent, dt) );
    pg_boxbatch_add_box(&mg->boxbatch, &PG_EZBOX(
        .pos = pos, .iso_scale = 0.5,
        .base_at_origin = 1,
        .color_mod = ubvec4(0), .color_add = info->color ));
}

MG_ENTITY_INTERACT(mg_item_interact_pickup)
{
    if(ent->subtype == MG_ITEM_PICKUP_MATERIAL) {
        b->materials += 5;
        ent->dead = 1;
    } else if(ent->subtype == MG_ITEM_PICKUP_SCIENCE) {
        b->science += 5;
        ent->dead = 1;
    }
}

MG_ENTITY_USE(mg_item_use_gun_basic)
{
    struct mg_weapon_data* weap = mg_weapon_data(ent);
    printf("Hello\n");
    if(weap->attack_timer > b->tick) return;
    const struct mg_item_type_data* info = &MG_ITEM_TYPE_DATA[ent->subtype];
    vec3 shoot_pos = user->pos;
    shoot_pos.z += 1;
    vec3 shoot_dir = vec3_from_sph(VEC_XY(user->rot_euler));
    vec3 bullet_vel = vec3_tolen(shoot_dir, info->bullet_speed);
    struct mg_bullet new_bullet = { .hostile = 0,
        .pos = vec3_add(shoot_pos, shoot_dir), .vel = bullet_vel,
        .scale = vec2(info->bullet_size, info->bullet_size),
        .color = info->bullet_color, .frame = info->bullet_frame };
    ARR_PUSH(b->bullets, new_bullet);
    weap->attack_timer = b->tick + info->fire_rate;
}


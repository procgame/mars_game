#include "procgl/procgl.h"
#include "marsgame.h"








void mg_build_human_model(struct pg_ezbox_model* model, struct pg_ezbox_rig* rig)
{
    /*  Test model  */
    *model = (struct pg_ezbox_model){ .num_parts = 18,
        .parts = {
            /*  Torso   */
            PG_EZBOX(.tex_layer = 1, .iso_scale = 2, .aniso_scale = vec3(0.275,0.15,0.55),
                     .color_mod=ubvec4(), .color_add = ubvec4(64, 128, 96, 255),
                     .pos = vec3(0,0,1.2)),
            /*  Head    */
            PG_EZBOX(.tex_layer = 1, .iso_scale = 0.5, .pos = vec3(0, 0, 2.3),
                     .aniso_scale = vec3(0.6, 0.6, 0.9),
                     .color_mod=ubvec4(), .color_add = ubvec4(128, 128, 150, 255)),
            /*  Right upper leg   */
            PG_EZBOX(.tex_layer = 1, .iso_scale = 2,
                     .color_mod=ubvec4(), .color_add = ubvec4(64, 128, 96, 255),
                     .cap_scale_top=vec2(0.9,0.9),
                     .aniso_scale = vec3(0.14, 0.14, 0.3), .pos = vec3(0.15, 0, 1.2),
                     .orientation = quat_rotation(vec3(0,1,0), LM_PI)),
            /*  Right lower leg   */
            PG_EZBOX(.tex_layer = 1, .iso_scale = 2,
                     .color_mod=ubvec4(), .color_add = ubvec4(42, 96, 64, 255),
                     .cap_scale_top=vec2(0.7,0.7),
                     .aniso_scale = vec3(0.14, 0.14, 0.3), .pos = vec3(0.15, 0, 0.6),
                     .orientation = quat_rotation(vec3(0,1,0), LM_PI)),
            /*  Left upper leg   */
            PG_EZBOX(.tex_layer = 1, .iso_scale = 2,
                     .color_mod=ubvec4(), .color_add = ubvec4(64, 128, 96, 255),
                     .cap_scale_top=vec2(0.9,0.9),
                     .aniso_scale = vec3(0.14, 0.14, 0.3), .pos = vec3(-0.15, 0, 1.2),
                     .orientation = quat_rotation(vec3(0,1,0), -LM_PI)),
            /*  Left lower leg   */
            PG_EZBOX(.tex_layer = 1, .iso_scale = 2,
                     .color_mod=ubvec4(), .color_add = ubvec4(42, 96, 64, 255),
                     .cap_scale_top=vec2(0.7,0.7),
                     .aniso_scale = vec3(0.14, 0.14, 0.3), .pos = vec3(-0.15, 0, 0.6),
                     .orientation = quat_rotation(vec3(0,1,0), -LM_PI)),
            /*  Right upper arm  */
            PG_EZBOX(.tex_layer = 1, .iso_scale = 2,
                     .color_mod=ubvec4(), .color_add = ubvec4(64, 128, 96, 255),
                     .cap_scale_top=vec2(0.8,0.8),
                     .aniso_scale = vec3(0.1, 0.1, 0.25), .pos = vec3(0.2, 0, 2.2),
                     .orientation = quat_rotation(vec3(0,1,0), LM_PI * 0.5)),
            /*  Right forearm */
            PG_EZBOX(.tex_layer = 1, .iso_scale = 2,
                     .color_mod=ubvec4(), .color_add = ubvec4(42, 96, 64, 255),
                     .cap_scale_bottom=vec2(0.8,0.8),
                     .cap_scale_top=vec2(0.6,0.6),
                     .aniso_scale = vec3(0.1, 0.1, 0.25), .pos = vec3(0.7, 0, 2.2),
                     .orientation = quat_rotation(vec3(0,1,0), LM_PI * 0.5)),
            /*  Left upper arm  */
            PG_EZBOX(.tex_layer = 1, .iso_scale = 2,
                     .color_mod=ubvec4(), .color_add = ubvec4(64, 128, 96, 255),
                     .cap_scale_top=vec2(0.8,0.8),
                     .aniso_scale = vec3(0.1, 0.1, 0.25), .pos = vec3(-0.2, 0, 2.2),
                     .orientation = quat_rotation(vec3(0,1,0), LM_PI * -0.5)),
            /*  Left forearm */
            PG_EZBOX(.tex_layer = 1, .iso_scale = 2,
                     .color_mod=ubvec4(), .color_add = ubvec4(42, 96, 64, 255),
                     .cap_scale_bottom=vec2(0.8,0.8),
                     .cap_scale_top=vec2(0.6,0.6),
                     .aniso_scale = vec3(0.1, 0.1, 0.25), .pos = vec3(-0.7, 0, 2.2),
                     .orientation = quat_rotation(vec3(0,1,0), LM_PI * -0.5)),
            /*  O2 Tanks    */
            PG_EZBOX(.tex_layer = 1, .iso_scale = 2, .aniso_scale = vec3(0.1, 0.1, 0.3),
                     .color_mod=ubvec4(), .color_add = ubvec4(128, 128, 200, 255),
                     .orientation = quat_rotation(vec3_Z(), LM_PI * 0.25),
                     .pos = vec3(-0.15,0.3,1.5)),
            PG_EZBOX(.tex_layer = 1, .iso_scale = 2, .aniso_scale = vec3(0.1, 0.1, 0.1),
                     .color_mod=ubvec4(), .color_add = ubvec4(108, 108, 180, 255),
                     .orientation = quat_rotation(vec3_Z(), LM_PI * 0.25),
                     .cap_scale_bottom=vec2(0,0),
                     .pos = vec3(-0.15,0.3,1.3)),
            PG_EZBOX(.tex_layer = 1, .iso_scale = 2, .aniso_scale = vec3(0.1, 0.1, 0.1),
                     .color_mod=ubvec4(), .color_add = ubvec4(148, 148, 220, 255),
                     .orientation = quat_rotation(vec3_Z(), LM_PI * 0.25),
                     .cap_scale_top=vec2(0,0),
                     .pos = vec3(-0.15,0.3,2.1)),
            PG_EZBOX(.tex_layer = 1, .iso_scale = 2, .aniso_scale = vec3(0.1, 0.1, 0.3),
                     .color_mod=ubvec4(), .color_add = ubvec4(128, 128, 200, 255),
                     .orientation = quat_rotation(vec3_Z(), LM_PI * 0.25),
                     .pos = vec3(0.15,0.3,1.5)),
            PG_EZBOX(.tex_layer = 1, .iso_scale = 2, .aniso_scale = vec3(0.1, 0.1, 0.1),
                     .color_mod=ubvec4(), .color_add = ubvec4(108, 108, 180, 255),
                     .orientation = quat_rotation(vec3_Z(), LM_PI * 0.25),
                     .cap_scale_bottom=vec2(0,0),
                     .pos = vec3(0.15,0.3,1.3)),
            PG_EZBOX(.tex_layer = 1, .iso_scale = 2, .aniso_scale = vec3(0.1, 0.1, 0.1),
                     .color_mod=ubvec4(), .color_add = ubvec4(148, 148, 220, 255),
                     .orientation = quat_rotation(vec3_Z(), LM_PI * 0.25),
                     .cap_scale_top=vec2(0,0),
                     .pos = vec3(0.15,0.3,2.1)),
            /*  Comm stack  */
            PG_EZBOX(.tex_layer = 1, .iso_scale = 0.5, .aniso_scale = vec3(0.3,0.3,0.3),
                     .color_mod=ubvec4(), .color_add = ubvec4(64, 64, 80, 255),
                     .orientation = quat_rotation(vec3_Z(), LM_PI * 0.25),
                     .pos = vec3(-0.15,0,2.5)),
            PG_EZBOX(.tex_layer = 1, .iso_scale = 1, .aniso_scale = vec3(0.04,0.04,0.6),
                     .color_mod=ubvec4(), .color_add = ubvec4(64, 64, 80, 255),
                     .orientation = quat_rotation(vec3_Z(), LM_PI * 0.25),
                     .pos = vec3(-0.185,0,2.5)),

        },
        .use_bone = {
            "torso", "head", "upper_leg.R", "lower_leg.R",
            "upper_leg.L", "lower_leg.L", "upper_arm.R", "forearm.R",
            "upper_arm.L", "forearm.L",
            "torso", "torso", "torso", "torso", "torso", "torso",
            "head", "head", },
    };
    pg_ezbox_model_link_rig(model, rig);
}

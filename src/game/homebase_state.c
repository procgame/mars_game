#include "procgl/procgl.h"
#include "marsgame.h"

PG_MEMPOOL_DEFINE(struct mg_homebase_person, mg_homebase_person, alloc);

/****************************/
/*  UPGRADES                */
/****************************/

/****************************/
/*  BULLETINS               */
/****************************/

/************************/
/*  DOMES               */
/************************/

void mg_homebase_init_domes(struct mg_homebase_state* h);
void mg_homebase_init_upgrades(struct mg_homebase_state* h);
void mg_homebase_init_bulletins(struct mg_homebase_state* h);

static void homebase_make_viewport(struct marsgame* mg, struct mg_homebase_state* h, float dt);

void mg_init_homebase(struct marsgame* mg)
{
    struct mg_homebase_state* h = &mg->homebase;
    mg_homebase_dome_pool_init(&h->dome_pool);
    mg_homebase_upgrade_pool_init(&h->up_pool);
    mg_homebase_bulletin_pool_init(&h->blt_pool);
    HTABLE_INIT(h->dome_names, 8);
    HTABLE_INIT(h->up_names, 16);
    HTABLE_INIT(h->blt_names, 16);
    mg_homebase_init_domes(h);
    mg_homebase_init_upgrades(h);
    mg_homebase_init_bulletins(h);
    ARR_INIT(h->active_missions);
    ARR_INIT(h->domes);

    h->selected_dome = -1;
    h->selected_person = -1;

    mg_homebase_dome_info_t empty= mg_homebase_dome_info_by_name(h, "empty");
    float halfstep = LM_TAU / 6 * 0.5;
    int i;
    for(i = 0; i < 6; ++i) {
        /*  Allocate a dome */
        struct mg_homebase_dome* dome;
        mg_homebase_dome_id_t dome_id =
            mg_homebase_dome_alloc(&h->dome_pool, 1, &dome);
        mg_homebase_dome_init(dome, empty);
        float angle = ((float)i / 6) * LM_TAU + halfstep;
        dome->angle = angle;
        dome->pos = vec2_rotate(vec2(0,12), angle);
        ARR_PUSH(h->domes, dome_id);
    }
    mg_homebase_dome_info_t central= mg_homebase_dome_info_by_name(h, "central");
    struct mg_homebase_dome* dome;
    mg_homebase_dome_id_t dome_id =
        mg_homebase_dome_alloc(&h->dome_pool, 1, &dome);
    mg_homebase_dome_init(dome, central);
    dome->pos = vec2(0,0);
    ARR_PUSH(h->domes, dome_id);

    h->cam_lookat_anim = PG_SIMPLE_ANIM(
        .start = vec4(0,0,0,25), .end = vec4(0,0,0,25) );
    h->cam_angle_anim = PG_SIMPLE_ANIM(
        .start = vec4(0,LM_PI*0.25), .end = vec4(0,LM_PI*0.25) );
    homebase_make_viewport(mg, h, 0);
    mg_homebase_build_ui(&mg->ui, h);

    h->population = 100;
    h->water = 1000;
    h->materials = 150;
    h->science = 40;
}

void mg_deinit_homebase(struct mg_homebase_state* h)
{
    ARR_DEINIT(h->domes);
    ARR_DEINIT(h->active_missions);
    mg_homebase_bulletin_pool_deinit(&h->blt_pool);
    mg_homebase_upgrade_pool_deinit(&h->up_pool);
    mg_homebase_dome_pool_deinit(&h->dome_pool);
    pg_ui_delete(h->ui_context, h->ui);
}

static void homebase_make_viewport(struct marsgame* mg, struct mg_homebase_state* h, float dt)
{
    /*  Animate the camera  */
    float anim_time = ((float)h->tick + dt) / 60.0f;
    vec4 camera_lookat = pg_simple_anim_value(&h->cam_lookat_anim, anim_time);
    vec4 camera_rotate = pg_simple_anim_value(&h->cam_angle_anim, anim_time);
    if(h->selected_dome >= 0
    && pg_simple_anim_finished(&h->cam_angle_anim, anim_time)) {
        float since_done = h->cam_angle_anim.start_time + h->cam_angle_anim.duration;
        since_done = anim_time - since_done;
        camera_rotate.x += since_done;
    }
    /*  Calculate animation results */
    quat y_plane = quat_rotation(vec3_X(), LM_PI * -0.5);
    quat pos_quat = quat_mul(quat_rotation(vec3_Z(), camera_rotate.x),
                             quat_rotation(vec3_X(), -camera_rotate.y));
    quat view_quat = quat_mul(quat_rotation(vec3_X(), camera_rotate.y),
                              quat_rotation(vec3_Z(), -camera_rotate.x));
    view_quat = quat_mul(y_plane, view_quat);
    vec3 view_offset = quat_mul_vec3(pos_quat, vec3(0, -camera_lookat.w, 0));
    view_offset = vec3_add(view_offset, vec3(VEC_XYZ(camera_lookat)));
    pg_viewer_perspective(&h->worldview, view_offset, view_quat, mg->halfres, vec2(0.01, 100));
}

void mg_start_homebase(struct marsgame* mg)
{
    struct mg_homebase_state* h = &mg->homebase;
    h->ui_context = &mg->ui;
    pg_ui_enabled(h->ui_context, h->ui, 1);
    h->should_deploy = 0;
    h->science += mg->battle.science;
    h->materials += mg->battle.materials;
    pg_renderpass_viewer(&mg->world_pass, &h->worldview);
    pg_renderpass_viewer(&mg->box_pass, &h->worldview);
}

void mg_end_homebase(struct mg_homebase_state* h)
{
    pg_ui_enabled(h->ui_context, h->ui, 0);
}

void mg_homebase_update(struct marsgame* mg)
{
    struct mg_homebase_state* h = &mg->homebase;
}

void mg_homebase_input(struct marsgame* mg)
{
    struct mg_homebase_state* h = &mg->homebase;
}

void mg_homebase_tick(struct marsgame* mg)
{
    struct mg_homebase_state* h = &mg->homebase;
    if(h->should_deploy) {
        mg_end_homebase(h);
        mg_start_missions(mg);
        mg->state = MG_MISSIONS;
    }
}

/********************/
/*  Rendering       */
/********************/

static void draw_ground(struct marsgame* mg, float dt)
{
    /*  Draw the ground */
    pg_tex_frame_t frame = pg_texture_frame(&mg->sgtex, 1, 0);
    vec2 tile_size = vec2( 16, 16 );
    int i, j;
    for(i = 0; i < 16; ++i) {
        for(j = 0; j < 16; ++j) {
            pg_quadbatch_add_quad(&mg->quadbatch, PG_EZQUAD(
                .tex_frame = frame.frame, .tex_layer = 1,
                .pos = vec3( i * tile_size.x - tile_size.x * 8,
                             j * tile_size.y - tile_size.y * 8, 0 ),
                .scale = vec2( tile_size.x, tile_size.y ),
                .orientation = quat_identity(),
                .color_mod = 0xFFFFFFFF ));
        }
    }
}

void draw_dome(struct marsgame* mg, ubvec4 color, vec3 pos, float scale, float angle)
{
    quat rot = quat_rotation(vec3_Z(), angle);
    pg_boxbatch_add_box(&mg->boxbatch, &PG_EZBOX(
        .pos = vec3_add(pos, vec3(0,0,0)), .iso_scale = scale,
        .base_at_origin = 1,
        .aniso_scale = vec3(1, 1, 0.5),
        .cap_scale_top = vec2(0.75, 0.75),
        .orientation = rot,
        .color_mod = ubvec4(0), .color_add = color ));
    pg_boxbatch_add_box(&mg->boxbatch, &PG_EZBOX(
        .pos = vec3_add(pos, vec3(0,0,0.5*scale)), .iso_scale = scale,
        .base_at_origin = 1,
        .aniso_scale = vec3(1, 1, 0.25),
        .cap_scale_bottom = vec2(0.75, 0.75),
        .cap_scale_top = vec2(0.5, 0.5),
        .orientation = rot,
        .color_mod = ubvec4(0), .color_add = color ));
    pg_boxbatch_add_box(&mg->boxbatch, &PG_EZBOX(
        .pos = vec3_add(pos, vec3(0,0,0.75*scale)), .iso_scale = scale,
        .base_at_origin = 1,
        .aniso_scale = vec3(1, 1, 0.125),
        .cap_scale_bottom = vec2(0.5, 0.5),
        .cap_scale_top = vec2(0.25, 0.25),
        .orientation = rot,
        .color_mod = ubvec4(0), .color_add = color ));
}

void mg_homebase_draw(struct marsgame* mg, float dt)
{
    struct mg_homebase_state* h = &mg->homebase;
    pg_quadbatch_reset(&mg->quadbatch);
    pg_boxbatch_reset(&mg->boxbatch);
    /*  3D stuff    */
    ++h->tick;
    /*  Animate the camera  */
    float anim_time = ((float)h->tick) / 60.0f;
    vec4 camera_lookat = pg_simple_anim_value(&h->cam_lookat_anim, anim_time);
    vec4 camera_rotate = pg_simple_anim_value(&h->cam_angle_anim, anim_time);
    if(h->selected_dome >= 0
    && pg_simple_anim_finished(&h->cam_angle_anim, anim_time)) {
        float since_done = h->cam_angle_anim.start_time + h->cam_angle_anim.duration;
        since_done = (anim_time - since_done) * 0.5;
        camera_rotate.x += since_done;
    }
    /*  Calculate animation results */
    quat y_plane = quat_rotation(vec3_X(), LM_PI * -0.5);
    quat pos_quat = quat_mul(quat_rotation(vec3_Z(), camera_rotate.x),
                             quat_rotation(vec3_X(), -camera_rotate.y));
    quat view_quat = quat_mul(quat_rotation(vec3_X(), camera_rotate.y),
                              quat_rotation(vec3_Z(), -camera_rotate.x));
    view_quat = quat_mul(y_plane, view_quat);
    //pos_quat = quat_mul(pos_quat, y_plane);
    vec3 view_offset = quat_mul_vec3(pos_quat, vec3(0, -camera_lookat.w, 0));
    //view_offset = quat_mul_vec3(y_plane, view_offset);
    view_offset = vec3_add(view_offset, vec3(VEC_XYZ(camera_lookat)));
    pg_viewer_perspective(&h->worldview, view_offset, view_quat, mg->halfres, vec2(0.01, 100));

    mat4 tx = mat4_identity();
    pg_quadbatch_next(&mg->quadbatch, &tx);
    pg_boxbatch_next(&mg->boxbatch, &tx);
    /*  Draw the things */

    draw_ground(mg, dt);
    //draw_dome(mg, ubvec4(100,100,220,255), vec3(0,0,0), 0.75, 0);
    int i;
    for(i = 0; i < h->domes.len; ++i) {
        struct mg_homebase_dome* dome = mg_homebase_dome_get(&h->dome_pool, h->domes.data[i]);
        if(dome->type == 0) continue;
        draw_dome(mg, dome->color, vec3(VEC_XY(dome->pos),0), dome->size, dome->angle);
    }

    /*  Finish  */
    pg_quadbatch_draw(&mg->quadbatch, &mg->world_pass);
    pg_boxbatch_draw(&mg->boxbatch, &mg->box_pass);
    pg_quadbatch_buffer(&mg->quadbatch);
    pg_boxbatch_buffer(&mg->boxbatch);
}

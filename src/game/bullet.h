struct mg_bullet {
    int dead;
    int frame;
    int hostile;
    uint32_t color;
    vec3 pos;
    vec3 vel;
    vec2 scale;
    float rotation;
};

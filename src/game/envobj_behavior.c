#include "procgl/procgl.h"
#include "marsgame.h"

/************************/
/*  ENEMY DATA          */
/************************/

/*  Forward declarations (defined below)    */
MG_ENTITY_DRAW(mg_envobj_draw_boulder);
MG_ENTITY_DRAW(mg_envobj_draw_dome_segment);

/*  Base entity behavior    */
const struct mg_entity_base MG_ENVOBJ_BASE[MG_NUM_ENVOBJ_TYPES] = {
    [MG_ENVOBJ_GROUND] = { .name = "ground",
        .max_HP = 100, .bound = { {{ -128, -128, -128 }}, {{ 128, 128, 1 }} },
        .collider = { .type = MG_COLLIDER_PLANE, .plane = 0,
            .flags = MG_COLLIDER_STATIC }, },
    [MG_ENVOBJ_BOULDER] = { .name = "boulder",
        .max_HP = 100, .bound = { {{ -1, -1, -1 }}, {{ 1, 1, 1 }} },
        .collider = { .type = MG_COLLIDER_SPHERE, .radius = 1,
            .flags = MG_COLLIDER_STATIC },
        .draw = mg_envobj_draw_boulder,
        .raycast = mg_entity_raycast_sphere, },
    [MG_ENVOBJ_DOME_SEGMENT] = { .name = "dome segment",
        .max_HP = 100, .bound =  { {{ -1, -1, -1 }}, {{ 1, 1, 1 }} },
        .draw = mg_envobj_draw_dome_segment },
};

/*  Enemy allocation    */
mg_entpool_id_t mg_new_envobj(enum mg_envobj_type type, struct mg_entity** ent_ptr)
{
    struct mg_entity* ent;
    mg_entpool_id_t id = mg_entpool_alloc(1, &ent);
    mg_entity_init_from_base(ent, &MG_ENVOBJ_BASE[type]);
    ent->type = MG_ENTITY_ENVOBJ;
    ent->subtype = type;
    if(ent_ptr) *ent_ptr = ent;
    return id;
}

/********************************/
/*  Behavior functions          */
/********************************/

MG_ENTITY_DRAW(mg_envobj_draw_boulder)
{
    pg_boxbatch_add_box(&mg->boxbatch, &PG_EZBOX(
        .pos = ent->pos,
        .iso_scale = ent->bound[1].x * 1.5,
        .orientation = ent->rot,
        .tex_layer = 1 ));
}

MG_ENTITY_DRAW(mg_envobj_draw_dome_segment)
{
    struct mg_envobj_dome_segment* seg_data = mg_envobj_dome_segment(ent);
    ubvec4 tex_frame = ubvec4(0, 0, 64, 64);
    ubvec4 black_frame = ubvec4(64, 0, 64, 64);
    pg_boxbatch_add_box(&mg->boxbatch, &PG_EZBOX(
        .pos = ent->pos,
        .iso_scale = 24.0f,
        .aniso_scale = vec3(seg_data->size.x/24.0f, 0.025, seg_data->size.y/24.0f),
        .cap_scale_top = vec2(seg_data->tip_scale, 1),
        .color_mod = ubvec4(0), .color_add = seg_data->color,
        .base_at_origin = 1,
        .orientation = ent->rot,
        .tex_layer = 1,
        .tex_front = tex_frame,
        .tex_back = tex_frame,
        .tex_left = black_frame,
        .tex_right = black_frame,
        .tex_top = black_frame,
        .tex_bottom = black_frame, ));
}

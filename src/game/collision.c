#include "procgl/procgl.h"
#include "marsgame.h"

/******************************************/
/*  Basic collision functions             */
/******************************************/
/*  (collider, collider) -> collision
    The types of the colliders passed as arguments are in the same order as
    the types in the function name. */

typedef void (*collide_func_t)(struct mg_collision*, struct mg_collider*, struct mg_collider*);
#define COLLIDE_FUNC(NAME) \
    static void NAME(struct mg_collision* out, struct mg_collider* coll_a, struct mg_collider* coll_b)

//  Friction sphere -> Friction sphere
COLLIDE_FUNC(collide_fsphere_fsphere)
{
    vec3 sph_a = vec3_add(coll_a->pos, coll_a->sph);
    vec3 sph_b = vec3_add(coll_b->pos, coll_b->sph);
    float rad_a = coll_a->radius;
    float rad_b = coll_b->radius;
    vec3 sep = vec3_sub(sph_a, sph_b);
    float overlap = (rad_a + rad_b) - vec3_len(sep);
    if(overlap <= 0) {
        *out = (struct mg_collision){ .did_collide = 0 };
        return;
    }
    vec3 sep_norm = vec3_norm(sep);
    /*  If almost on top, find a new separation axis so it is pushed vertically only  */
    if(fabs(sep_norm.z) > 0.75) {
        vec3 contact_point = vec3_add(sph_a, vec3_scale(sep_norm, rad_a - overlap));
        float new_sep = -raycast_sphere_inverted(contact_point, vec3_Z(), sph_a, rad_a);
        *out = (struct mg_collision){ .did_collide = 1,
            .sep_axis = vec3(0, 0, new_sep), .sep_norm = vec3_Z(),
            .sep_len = new_sep, .surf_norm = vec3_Z() };
        return;
    }
    /*  Otherwise regular sphere collision  */
    vec3 sep_axis = vec3_scale(sep_norm, overlap);
    *out = (struct mg_collision){ .did_collide = 1,
        .sep_axis = sep_axis, .sep_norm = sep_norm,
        .sep_len = overlap, .surf_norm = sep_norm };
    return;
}

// Friction sphere -> Sphere
COLLIDE_FUNC(collide_fsphere_sphere)
{
    vec3 sph_a = vec3_add(coll_a->pos, coll_a->sph);
    vec3 sph_b = vec3_add(coll_b->pos, coll_b->sph);
    float rad_a = coll_a->radius;
    float rad_b = coll_b->radius;
    vec3 sep = vec3_sub(sph_a, sph_b);
    float overlap = -(vec3_len(sep) - (rad_a + rad_b));
    if(overlap <= 0) {
        *out = (struct mg_collision){ .did_collide = 0 };
        return;
    }
    vec3 sep_norm = vec3_norm(sep);
    /*  If almost on top, find a new separation axis so it is pushed vertically only  */
    if(sep_norm.z > 0.75) {
        vec3 contact_point = vec3_add(sph_a, vec3_scale(sep_norm, rad_a - overlap));
        float new_sep = raycast_sphere_inverted(contact_point, vec3(0, 0, -1), sph_a, rad_a);
        *out = (struct mg_collision){ .did_collide = 1,
            .sep_axis = vec3(0, 0, new_sep), .sep_norm = vec3_Z(),
            .sep_len = overlap, .surf_norm = vec3_Z() };
        return;
    }
    /*  Otherwise regular sphere collision  */
    vec3 sep_axis = vec3_scale(sep_norm, overlap);
    *out = (struct mg_collision){ .did_collide = 1,
        .sep_axis = sep_axis, .sep_norm = sep_norm,
        .sep_len = overlap, .surf_norm = sep_norm };
    return;
}

// Sphere -> Sphere
COLLIDE_FUNC(collide_sphere_sphere)
{
    vec3 sph_a = coll_a->pos;
    vec3 sph_b = coll_b->pos;
    float rad_a = coll_a->radius;
    float rad_b = coll_b->radius;
    vec3 sep = vec3_sub(sph_a, sph_b);
    float overlap = (rad_a + rad_b) - vec3_len(sep);
    if(overlap <= 0) {
        *out = (struct mg_collision){ .did_collide = 0 };
        return;
    }
    vec3 sep_norm = vec3_norm(sep);
    vec3 sep_axis = vec3_scale(sep_norm, overlap);
    *out = (struct mg_collision){ .did_collide = 1,
        .sep_axis = sep_axis, .sep_norm = sep_norm,
        .sep_len = overlap, .surf_norm = sep_norm };
    return;
}

// Sphere -> Plane
COLLIDE_FUNC(collide_sphere_plane)
{
    vec3 sphere = coll_a->pos;
    float radius = coll_a->radius;
    float z = coll_b->plane;
    if(sphere.z - radius < z) {
        float dist = (z - (sphere.z - radius));
        *out = (struct mg_collision){ .did_collide = 1,
             .sep_axis = vec3(0, 0, dist), .sep_norm = vec3_Z(),
             .sep_len = dist, .surf_norm = vec3_Z() };
        return;
    }
    *out = (struct mg_collision){ .did_collide = 0 };
    return;
}


/*  Function dispatch table     */

static const struct collide_func_dispatch {
    collide_func_t func;
    int arg0, arg1;
} COLLIDE_DISPATCH[MG_NUM_COLLIDER_TYPES][MG_NUM_COLLIDER_TYPES] = {
    [MG_COLLIDER_SPHERE_FRICTION] = {
        [MG_COLLIDER_SPHERE_FRICTION] = { collide_fsphere_fsphere, 0, 1 },
        [MG_COLLIDER_SPHERE] = { collide_fsphere_sphere, 0, 1 },
        [MG_COLLIDER_PLANE] = { collide_sphere_plane, 0, 1 }, },
    [MG_COLLIDER_SPHERE] = {
        [MG_COLLIDER_SPHERE_FRICTION] = { collide_fsphere_sphere, 1, 0 },
        [MG_COLLIDER_SPHERE] = { collide_sphere_sphere, 0, 1 },
        [MG_COLLIDER_PLANE] = { collide_sphere_plane, 0, 1 }, },
    [MG_COLLIDER_PLANE] = {
        [MG_COLLIDER_SPHERE_FRICTION] = { collide_sphere_plane, 1, 0 },
        [MG_COLLIDER_SPHERE] = { collide_sphere_plane, 1, 0 },
        [MG_COLLIDER_PLANE] = { NULL }, },
};

const char* MG_COLLIDER_NAME[MG_NUM_COLLIDER_TYPES] = {
    [MG_NO_COLLIDER] = "NullCollider",
    [MG_COLLIDER_SPHERE_FRICTION] = "FrictionSphere",
    [MG_COLLIDER_SPHERE] = "Sphere",
    [MG_COLLIDER_PLANE] = " XPlane",
};

/********************************************/
/*  Collision detection and response code   */
/********************************************/

/*  Resolve a full collision for any two colliders.
    Calculates the function and argument order for the combination of
    collider types, and applies the result to both colliders    */
void mg_collider_resolve(struct mg_collision* out,
                         struct mg_collider* c0, struct mg_collider* c1)
{
    if(c0->flags & c1->flags & MG_COLLIDER_STATIC) return;
    /*  Dispatch to the correct function with correct argument order    */
    const struct collide_func_dispatch* func =
        &COLLIDE_DISPATCH[c0->type][c1->type];
    struct mg_collider* arg_fixed_order[2] = { c0, c1 };
    struct mg_collider* coll[2] = { arg_fixed_order[func->arg0], arg_fixed_order[func->arg1] };
    int debug = (c0->flags & MG_COLLIDER_DEBUGGED) || (c1->flags & MG_COLLIDER_DEBUGGED);
    if(!func->func) {
        if(debug) printf("        No collision function.\n");
        return;
    }
    func->func(out, coll[0], coll[1]);
    if(!out->did_collide) return;
    /*  Calculate mass differential; static = infinite mass */
    if(c0->flags & MG_COLLIDER_STATIC) {
        out->impulse[0] = 0.0f;
        out->impulse[1] = 1.0f;
    } else if(c1->flags & MG_COLLIDER_STATIC) {
        out->impulse[0] = 1.0f;
        out->impulse[1] = 0.0f;
    } else {
        float mass[2] = { coll[0]->mass, coll[1]->mass };
        float total_mass = mass[0] + mass[1];
        out->impulse[0] = mass[1] / total_mass;
        out->impulse[1] = -(mass[0] / total_mass);
    }
    float impulse[2] = { out->impulse[func->arg0], -out->impulse[func->arg1] };
    /*  Apply collision to both entities    */
    if(impulse[0] != 0) {
        coll[0]->n_pushes += 1;
        coll[0]->push = vec3_add(coll[0]->push, vec3_scale(out->sep_axis, impulse[0]));
        if(out->sep_norm.z * impulse[0] > 0.75) coll[0]->flags |= MG_COLLIDER_ON_SURFACE;
    }
    if(impulse[1] != 0) {
        coll[1]->n_pushes += 1;
        coll[1]->push = vec3_add(coll[1]->push, vec3_scale(out->sep_axis, impulse[1]));
        if(out->sep_norm.z * impulse[1] > 0.75) coll[1]->flags |= MG_COLLIDER_ON_SURFACE;
    }
}


/*  Collision reaction functions    */
void mg_collider_finalize(struct mg_collider* coll)
{
    coll->push_norm = vec3_norm(coll->push);
    coll->push_len = vec3_len(coll->push);
    coll->n_pushes = 0;
}

vec3 mg_collider_slide(struct mg_collider* coll)
{
    if(coll->push_len == 0) return vec3(0);
    return vec3_scale(coll->push_norm, -vec3_dot(coll->vel, coll->push_norm));
}

vec3 mg_collider_bounce(struct mg_collider* coll)
{
    if(coll->push_len == 0) return vec3(0);
    return vec3_scale(coll->push_norm, -2*vec3_dot(coll->vel, coll->push_norm));
}

/********************************/
/*  Debug printing functions    */
/********************************/
void debug_print_collider(struct mg_collider* coll, char* name)
{
    printf("       %s | %d influencers | ", (name ? name : "Collider"), coll->n_pushes);
    if(coll->type == MG_COLLIDER_SPHERE) {
        printf("Sphere(%f)", coll->radius);
    } else if(coll->type == MG_COLLIDER_SPHERE_FRICTION) {
        printf("Friction phere(%f)", coll->radius);
    } else if(coll->type == MG_COLLIDER_PLANE) {
        printf("Plane(%f)", coll->plane);
    }
    printf(" | Flags: %s %s %s\n",
        (coll->flags & MG_COLLIDER_STATIC) ? "Static " : "",
        (coll->flags & MG_COLLIDER_DEBUGGED) ? "DEBUG " : "",
        (coll->flags & MG_COLLIDER_ON_SURFACE) ? "OnSurface " : "");
    printf("         Position: (%f, %f, %f) Velocity: (%f, %f, %f)\n",
        VEC_XYZ(coll->pos), VEC_XYZ(coll->vel));
}

void debug_print_collision(struct mg_collision* coll, vec3 vel, int side)
{
    float impulse = coll->impulse[side];
    vec3 push = vec3_scale(coll->sep_axis, impulse);
    float len = vec3_len(push);
    printf("         Push %f: (%f, %f, %f) Length %f\n"
           "         Surface normal: (%f, %f, %f)\n",
           impulse, VEC_XYZ(push), len, VEC_XYZ(coll->surf_norm));
}

void mg_debug_print_collision(struct mg_collision* coll,
                              struct mg_collider* c0, char* name0,
                              struct mg_collider* c1, char* name1)
{
    #ifdef MG_DEBUG_COLLISIONS
    debug_print_collider(c0, name0);
    if(coll->impulse[0] != 0) debug_print_collision(coll, c0->vel, 0);
    debug_print_collider(c1, name1);
    if(coll->impulse[1] != 0) debug_print_collision(coll, c1->vel, 1);
    #endif
}


struct marsgame;

#include "game_math.h"
#include "particles.h"
#include "item.h"
#include "collision.h"
#include "entity.h"
#include "entity_types.h"
#include "bullet.h"
#include "battle_space.h"
#include "battle_state.h"
#include "homebase_state.h"
#include "missions_state.h"
#include "ui/scroll_list.h"

struct marsgame {
    /*  Assets  */
    struct pg_texture sgtex;
    struct pg_font sgfont[3];
    struct pg_ezbox_model human_model;
    struct pg_ezbox_rig human_rig;
    struct pg_texture modeltex;
    struct pg_model test_model;
    struct pg_vertex_buffer model_verts;
    /*  RENDERING   */
    vec2 sgtex_64;
    struct pg_renderer rend;
    vec2 halfres;
    vec2 res;
    float ar;
    /*  Rendertargets   */
    struct pg_texture pptex[2];
    struct pg_texture ppdepth;
    struct pg_renderbuffer ppbuf[2];
    struct pg_rendertarget target;
    /*  Renderpasses    */
    struct mg_particle_system particle_system;
    struct pg_quadbatch quadbatch;
    struct pg_boxbatch boxbatch;
    struct pg_renderpass world_pass;
    struct pg_renderpass box_pass;
    struct pg_renderpass fog_pass;
    struct pg_renderpass particle_pass;
    struct pg_renderpass overlay_pass;
    struct pg_renderpass model_pass;
    struct pg_renderpass copy_pass;
    /*  Viewers */
    struct pg_viewer worldview;
    struct pg_viewer screenview;
    /*  UI  */
    struct pg_ui_context ui;
    pg_ui_t overworld_ui;
    /*  GAME DATA   */
    vec2 mouse_motion;
    vec2 mouse_pos;
    /*  Game states */
    enum {
        MG_BATTLE,
        MG_HOMEBASE,
        MG_MISSIONS,
    } state;
    struct mg_battle_state battle;
    struct mg_homebase_state homebase;
    struct mg_missions_state mission;
};

void marsgame_start(struct pg_game_state* state);

/********************************/
/*  Model functions (models.c)  */
/********************************/
void pg_model_load_mg(struct pg_model* model, const char* filename);
void mg_build_human_model(struct pg_ezbox_model* model, struct pg_ezbox_rig* rig);

#define UI_GROUP(parent, name, ...) \
    pg_ui_add_group(ctx, parent, name, PG_UI_PROPERTIES( __VA_ARGS__ ))
#define UI_ELEM(parent, name, ...) \
    pg_ui_add_elem(ctx, parent, name, PG_UI_PROPERTIES( __VA_ARGS__ ))

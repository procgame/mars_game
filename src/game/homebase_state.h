struct mg_homebase_state;

/********************************/
/*  Homebase info structures    */
/********************************/
typedef int mg_homebase_dome_info_t;
typedef int mg_homebase_upgrade_info_t;
typedef int mg_homebase_bulletin_info_t;

/*  Domes   */
struct mg_homebase_dome_info {
    wchar_t display_name[32];
    char id_name[32];
    int can_build;
    int materials_cost;
    wchar_t func_buttons[3][16];
    void (*func_ui[3])(struct pg_ui_context*, struct mg_homebase_state*, int);
    ubvec4 color;
    float size;
};

/*  Upgrades    */
struct mg_homebase_upgrade_info {
    char dome_id_name[32];
    wchar_t display_name[64];
    char id_name[64];
    char depends_name[4][64];
    int materials_cost;
    mg_homebase_upgrade_info_t depends_id[4];
    mg_homebase_dome_info_t dome_type;
};

/*  Bulletins   */
struct mg_homebase_bulletin_info {
    char dome_id_name[32];
    wchar_t display_name[64];
    char id_name[32];
    int permanent;
    int is_mission;
    int (*should_post)(struct mg_homebase_state*, struct mg_homebase_bulletin_info*);
    mg_homebase_dome_info_t dome_type;
};

/********************************/
/*  In-game data structures     */
/********************************/
struct mg_homebase_bulletin {
    int alloc;
    int active;
    mg_homebase_bulletin_info_t info;
};

PG_MEMPOOL_DECLARE(struct mg_homebase_bulletin, mg_homebase_bulletin);

struct mg_homebase_upgrade {
    int alloc;
    int completed;
    mg_homebase_upgrade_info_t info;
};

PG_MEMPOOL_DECLARE(struct mg_homebase_upgrade, mg_homebase_upgrade);

struct mg_homebase_dome {
    int alloc;
    mg_homebase_dome_info_t type;
    ARR_T(mg_homebase_upgrade_id_t) upgrades;
    ARR_T(mg_homebase_bulletin_id_t) bulletins;
    vec2 pos;
    float angle;
    float size;
    ubvec4 color;
};

PG_MEMPOOL_DECLARE(struct mg_homebase_dome, mg_homebase_dome);

struct mg_homebase_person {
    int alloc;
    wchar_t name[32];
    mg_homebase_bulletin_id_t assigned_mission;
};

PG_MEMPOOL_DECLARE(struct mg_homebase_person, mg_homebase_person);

/********************************/
/*  Full homebase state         */
/********************************/
struct mg_homebase_state {
    /*  UI  */
    struct pg_ui_context* ui_context;
    pg_ui_t ui;
    /*  Memory management   */
    mg_homebase_dome_pool_t dome_pool;
    mg_homebase_upgrade_pool_t up_pool;
    mg_homebase_bulletin_pool_t blt_pool;
    mg_homebase_person_pool_t ppl_pool;
    HTABLE_T(mg_homebase_dome_info_t) dome_names;
    HTABLE_T(mg_homebase_upgrade_info_t) up_names;
    HTABLE_T(mg_homebase_bulletin_info_t) blt_names;
    /*  Domes   */
    ARR_T(mg_homebase_dome_id_t) domes;
    int selected_dome;
    /*  Selected missions   */
    ARR_T(mg_homebase_bulletin_id_t) active_missions;
    int selected_person;
    /*  Personnel   */
    ARR_T(mg_homebase_person_id_t) personnel;
    /*  Rendering   */
    struct pg_viewer worldview;
    struct pg_simple_anim cam_lookat_anim;
    struct pg_simple_anim cam_angle_anim;
    /*  Game state  */
    int tick;
    int population;
    int water;
    int materials;
    int science;
    int should_deploy;
};

/********************************/
/*  Base functions (homebase.c) */
/********************************/
void mg_init_homebase(struct marsgame* mg);
void mg_deinit_homebase(struct mg_homebase_state* h);
void mg_start_homebase(struct marsgame* mg);
void mg_homebase_update(struct marsgame* mg);
void mg_homebase_input(struct marsgame* mg);
void mg_homebase_tick(struct marsgame* mg);
void mg_homebase_draw(struct marsgame* mg, float dt);
void mg_homebase_update_domes(struct mg_homebase_state* h);

/********************************/
/*  Domes (dome.c)              */
/********************************/
int mg_num_dome_types(void);
const struct mg_homebase_dome_info* mg_homebase_dome_info(mg_homebase_dome_info_t type);
mg_homebase_dome_info_t mg_homebase_dome_info_by_name(struct mg_homebase_state* h, char* name);
void mg_homebase_dome_init(struct mg_homebase_dome* dome, mg_homebase_dome_info_t type);
void mg_homebase_dome_deinit(struct mg_homebase_dome* dome);
int mg_homebase_have_dome_type(struct mg_homebase_state* h, mg_homebase_dome_info_t type);

/********************************/
/*  Upgrades (upgrade.c)        */
/********************************/
int mg_num_upgrade_types(void);
const struct mg_homebase_upgrade_info* mg_homebase_upgrade_info(int up);
int mg_homebase_have_upgrade(struct mg_homebase_state* h, int up);
void mg_homebase_get_upgrade(struct mg_homebase_state* h, int up);

/************************************/
/*  Bulletins/missions (bulletin.c) */
/************************************/
int mg_num_bulletin_types(void);
const struct mg_homebase_bulletin_info* mg_homebase_bulletin_info(int blt);
int mg_homebase_have_mission(struct mg_homebase_state* h, mg_homebase_bulletin_info_t blt);
mg_homebase_person_id_t mg_homebase_bulletin_get_assignee(
                            struct mg_homebase_state* h,
                            mg_homebase_bulletin_id_t blt_id);

/************************************/
/*  UI functions (homebase_ui.c)    */
/************************************/
void mg_homebase_build_ui(struct pg_ui_context* ctx, struct mg_homebase_state* h);
void mg_homebase_update_stats_ui(struct mg_homebase_state* h);

/*  UI Dome menu functions  */
void dome_ui_menu(struct pg_ui_context* ctx, struct mg_homebase_state* h, int dome_idx);
void exit_dome_submenu(struct pg_ui_context* ctx, pg_ui_t dome_menu);
int click_dome_submenu(struct pg_ui_context* ctx, pg_ui_t dome_menu,
                       pg_ui_t submenu);
/*  ui/build_new_dome.c     */
void ui_build_dome(struct pg_ui_context* ctx, struct mg_homebase_state* h, int dome_idx);
pg_ui_t ui_build_dome_create(struct pg_ui_context* ctx, struct mg_homebase_state* h);

/*  ui/upgrade_dome.c       */
void ui_upgrade_dome(struct pg_ui_context* ctx, struct mg_homebase_state* h, int dome_idx);
pg_ui_t ui_upgrade_dome_create(struct pg_ui_context* ctx, struct mg_homebase_state* h);

/*  ui/dome_bulletin.c      */
void ui_bulletin_board(struct pg_ui_context* ctx, struct mg_homebase_state* h, int dome_idx);
pg_ui_t ui_bulletin_board_create(struct pg_ui_context* ctx, struct mg_homebase_state* h);

/*  ui/active_missions.c    */
void ui_active_missions(struct pg_ui_context* ctx, struct mg_homebase_state* h, int dome_idx);
pg_ui_t ui_active_missions_create(struct pg_ui_context* ctx, struct mg_homebase_state* h);

/*  ui/recruiting.c */
void ui_recruiting(struct pg_ui_context* ctx, struct mg_homebase_state* h, int dome_idx);
pg_ui_t ui_recruiting_create(struct pg_ui_context* ctx, struct mg_homebase_state* h);

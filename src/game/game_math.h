#define FORCE_INLINE    __attribute__((always_inline)) static inline

FORCE_INLINE vec3 vec3_from_sph(float x, float y)
{
    float s_x = sin(x), s_y = sin(y), c_x = cos(x), c_y = cos(y);
    return vec3( -(s_x * c_y), c_x * c_y, s_y );
}

FORCE_INLINE float sq_dist_point_line(vec3 a, vec3 b, vec3 c)
{
    return vec3_len2(vec3_cross(vec3_sub(b, a), vec3_sub(a, c))) / vec3_dist2(a, b);
}

FORCE_INLINE float raycast_sphere_inverted(vec3 src, vec3 dir, vec3 s, float r)
{
    vec3 m = vec3_sub(src, s);
    float b = vec3_dot(m, dir);
    float c = vec3_dot(m, m) - (r * r);
    if(c > 0.0f && b > 0.0f) return 0.0f;
    float disc = b * b - c;
    if(disc < 0.0f) return 0.0f;
    float t = -b - sqrtf(disc);
    if(t > 0.0f) return 0.0f;
    return -t;
}

FORCE_INLINE float raycast_sphere(vec3 src, vec3 dir, vec3 s, float r)
{
    vec3 m = vec3_sub(src, s);
    float b = vec3_dot(m, dir);
    float c = vec3_dot(m, m) - (r * r);
    if(c > 0.0f && b > 0.0f) return -1.0f;
    float disc = b * b - c;
    if(disc < 0.0f) return -1.0f;
    float t = -b - sqrtf(disc);
    if(t < 0.0f) return 0.0f;
    return t;
}


FORCE_INLINE float raycast_plane(vec3 src, vec3 dir, vec3 p0, vec3 p_norm)
{
    vec3 rp = vec3_sub(src, p0);
    float rn = vec3_dot(dir, p_norm);
    float rpn = vec3_dot(rp, p_norm);
    if(rn == 0.0f) {
        if(rpn == 0.0f) return 0.0f;    /*  Line embedded in plane  */
        else return -1.0f;              /*  No intersection */
    } else {
        float d = rpn / rn;
        if(d < 0.0f) return -1.0f;      /*  Ray points away from plane  */
        else return d;                  /*  Return ray distance */
    }
}

FORCE_INLINE int aabb_intersect(vec3 a_min, vec3 a_max, vec3 b_min, vec3 b_max)
{
    if(a_max.x < b_min.x || a_min.x > b_max.x
    || a_max.y < b_min.y || a_min.y > b_max.y
    || a_max.z < b_min.z || a_min.z > b_max.z) return 0;
    return 1;
}

FORCE_INLINE int factorial(int a)
{
    int n = a, m = a;
    while(--m) n *= m;
    return n;
}

#define POS_LERP2(OBJ, TIME) \
      (OBJ)->pos.x + (OBJ)->vel.x * (TIME), \
      (OBJ)->pos.y + (OBJ)->vel.y * (TIME)
#define POS_LERP3(OBJ, TIME) \
      (OBJ)->pos.x + (OBJ)->vel.x * (TIME), \
      (OBJ)->pos.y + (OBJ)->vel.y * (TIME), \
      (OBJ)->pos.z + (OBJ)->vel.z * (TIME)

#undef FORCE_INLINE


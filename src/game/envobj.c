#include "procgl/procgl.h"
#include "marsgame.h"

#if 0
void mg_envobjs_tick(struct mg_battle_state* b)
{
    mg_entpool_id_t ent_id;
    struct mg_entity* ent;
    int i;
    ARR_FOREACH_REV(b->space.all_envobjs, ent_id, i) {
        ent = mg_entpool_get(ent_id);
        if(!ent) {
            ARR_SWAPSPLICE(b->space.all_envobjs, i, 1);
            continue;
        } else if(ent->dead) {
            ARR_SWAPSPLICE(b->space.all_envobjs, i, 1);
            mg_entpool_free(ent_id);
            continue;
        } else if(ent->last_tick == b->sim_tick) continue;
        ent->last_tick = b->sim_tick;
        /*  Actual envobj tick   */
        if(ent->tick) ent->tick(b, ent);
        ent = mg_entpool_get(ent_id);
    }
}

void mg_envobjs_draw(struct marsgame* mg, float dt)
{
    struct mg_battle_state* b = &mg->battle;
    mg_entpool_id_t ent_id;
    struct mg_entity* ent;
    int i;
    ARR_FOREACH_REV(b->space.all_envobjs, ent_id, i) {
        ent = mg_entpool_get(ent_id);
        if(!ent || ent->dead) continue;
        if(ent->draw) ent->draw(mg, ent, dt);
    }
}
#endif
